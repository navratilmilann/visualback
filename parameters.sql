-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `parameters`;
CREATE TABLE `parameters` (
                              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `type` varchar(255) DEFAULT NULL,
                              `name` varchar(255) DEFAULT NULL,
                              `description` varchar(255) DEFAULT NULL,
                              `slug` varchar(255) DEFAULT NULL,
                              `countable` int(1) DEFAULT 0,
                              `count` int(11) DEFAULT 0,
                              `language_id` int(11) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `parameters` (`id`, `type`, `name`, `description`, `slug`, `countable`, `count`, `language_id`) VALUES
(1,	'technology',	'Laravel',	NULL,	'laravel',	1,	2,	NULL),
(2,	'technology',	'Django',	NULL,	'django',	1,	2,	NULL),
(3,	'technology',	'HTML',	NULL,	'html',	1,	0,	NULL),
(4,	'technology',	'CSS',	NULL,	'css',	1,	0,	NULL),
(5,	'technology',	'Javascript',	NULL,	'javascript',	1,	1,	NULL),
(6,	'site-type',	'Eshop',	NULL,	'eshop',	1,	0,	NULL),
(7,	'site-type',	'Presentation page',	NULL,	'presentation-page',	1,	0,	NULL),
(8,	'site-type',	'Portfolio',	NULL,	'portfolio',	1,	0,	NULL),
(10,	'country',	'Country',	NULL,	'country',	0,	0,	NULL),
(12,	'rating',	'Rating',	NULL,	'rating',	0,	0,	NULL),
(13,	'site-type',	'Application',	NULL,	'application',	1,	0,	NULL),
(14,	'user-details',	'User details',	NULL,	'user-details',	0,	1,	NULL),
(15,	'user-city',	'User city',	NULL,	'user-city',	0,	0,	NULL),
(16,	'user-country',	'User country',	NULL,	'user-country',	0,	0,	NULL),
(17,	'user-permission',	'User permission',	NULL,	'user-permission',	0,	0,	NULL),
(19,	'technology',	'ReactJS',	NULL,	'reactjs',	1,	0,	NULL),
(20,	'user-platforms',	'Platforms',	NULL,	'platforms',	0,	0,	NULL),
(21,	'marketing-tool',	'Facebook',	'You have heard about us through Facebook post or facebook page.',	'facebook',	1,	1,	NULL),
(22,	'marketing-tool',	'People',	'You have heard abous us from friends.',	'people',	1,	8,	NULL),
(23,	'user-position',	'Student',	NULL,	'position',	1,	6,	NULL),
(24,	'source-code',	'Github',	NULL,	'Github',	0,	2,	NULL),
(25,	'user-position',	'Teacher',	NULL,	'position',	1,	5,	NULL),
(26,	'user-position',	'Freelancer',	NULL,	'position',	1,	1,	NULL),
(27,	'user-position',	'Employee',	NULL,	'position',	1,	0,	NULL),
(28,	'user-position',	'Team',	NULL,	'position',	1,	0,	NULL);

-- 2021-07-17 15:14:12