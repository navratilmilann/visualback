<div id="cookie-consent">
	<div class="js-cookie-consent cookie-consent">
	    <span class="cookie-consent__message">
	        {!! trans('cookieConsent::texts.message') !!} <a href="{{ route('cookies') }}">{!! trans('cookieConsent::texts.learn_more') !!}</a>
	    </span>
		<button class="js-cookie-consent-agree cookie-consent__agree button">
			{{ trans('cookieConsent::texts.agree') }}
		</button>
	</div>
</div>
