@extends('layouts.shared')

@section('content')
	<div id="app">
		<contact-index></contact-index>
		@include('partials.validator')
	</div>
@endsection
