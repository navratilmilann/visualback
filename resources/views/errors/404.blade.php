@extends('layouts.shared')

@section('content')
	<div id="app">
		<errors-error
				status-code="{{ !empty($exception) ? $exception->getStatusCode() : 404 }}"
				message="{{ !empty($exception) ? $exception->getMessage() : 'Not Found' }}"
		/>
	</div>
@endsection

