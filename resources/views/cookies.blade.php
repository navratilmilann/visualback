@extends('layouts.shared')

@section('content')
	<div id="app">
		<cookies></cookies>
		@include('partials.validator')
	</div>
@endsection
