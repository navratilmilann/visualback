@extends('layouts.shared')

@section('content')
	<div id="app">
		<profile-detail
				user-profile="{{ $user }}"
				site-likes="{{ $siteLikes }}"
				comment-likes="{{ $commentLikes }}"
		></profile-detail>
		@include('partials.validator')
	</div>
@endsection
