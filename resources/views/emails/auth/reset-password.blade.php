@extends('layouts.mail')

@section('content')

    <!-- BODY  -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0"
           style="min-width:100%;width:100%;background-color: #fff;" bgcolor="#ffffff">
        <tr>
            <td align="center"
                style="padding:15px 20px 0;font-size:24px;line-height:28px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">
                <strong>RESET PASSWORD</strong>
            </td>
        </tr>
        <tr>
            <td align="center"
                style="padding:25px 30px 0;font-size:16px;line-height:18px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Regular',SUWR,Arial,Sans-Serif;color:#505050;">
                We received a request to reset the password associated with
                <a href="mailto:{{ $email }}" target="_blank"
                   style="color:#505050;"><strong
                            style="font-weight:normal;color:#505050;">{{ $email }}</strong>
                </a>
            </td>
        </tr>
        <tr>
            <td class="mob_padding" style="padding:0 20px 20px;">

                <table width="100%" cellspacing="0" cellpadding="0" border="0"
                       style="min-width:100%;width:100%;background-color: #fff;">
                    <tr>
                        <td width="560" style="width:560px;padding:30px 0 10px;" bgcolor="#ffffff"
                            align="center">
                            <table width="240" cellspacing="0" cellpadding="0" border="0"
                                   style="width:240px;" bgcolor="#ffffff">
                                <!-- module 1 begin -->
                                <tr>
                                    <td width="240" cade="CTA_Button" style="width:240px;"
                                        bgcolor="#ffffff" align="center">
                                        <a href="#"
                                           target="_blank"
                                           style="color:#ffffff;text-decoration:none !important;">
                                            <table width="240" border="0" align="center"
                                                   cellspacing="0" cellpadding="0"
                                                   style="width:240px;">
                                                <tr>
                                                    <td width="240" bgcolor="#008a00" align="center"
                                                        style="width:240px;background-color:#500ade;vertical-align:middle;font-size:15px;line-height:20px;font-weight:normal;font-family:'Segoe UI Semibold','Segoe UI',SUWSB,Arial,Sans-serif; font-weight: normal;color:#ffffff;">
                                                        <a href="{{ route('reset-password',['token'=>$token]) }}"
                                                           target="_blank"
                                                           style="color:#ffffff;text-decoration:none;width: 240px;padding: 15px 0 15px 0;display: inline-block;">
                                                            <strong style="font-weight:normal;color:#ffffff;">
                                                                RESET PASSWORD
                                                            </strong>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection


