@extends('layouts.mail')

@section('content')

	<!-- BODY  -->
	<table width="100%" cellspacing="0" cellpadding="0" border="0"
	       style="min-width:100%;width:100%;background-color: #fff;" bgcolor="#ffffff">
		<tr>
			<td align="center"
			    style="padding:15px 20px 0;font-size:24px;line-height:28px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">
				<strong>ADMIN REPORT: {{ strtoupper($data['type']) }}</strong>
			</td>
		</tr>
		<tr>
			<td align="center"
			    style="padding:25px 30px 0;font-size:16px;line-height:18px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Regular',SUWR,Arial,Sans-Serif;color:#505050;">
				One of your {{ $data['type'] }}s was reported for checking.
			</td>
		</tr>
{{--		<tr>--}}
{{--			<td align="center" style="padding:30px 0 10px;">--}}
{{--				<strong style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">Reported--}}
{{--					By User:</strong><br>--}}
{{--				<span style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">--}}
{{--					<a href="{{ get_report_in_admin($data['user_id'],'users') }}"--}}
{{--					   style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-weight:bold;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#500ade;"--}}
{{--					   target="_blank">{{ $data['user_id'] }}</a>--}}
{{--				</span>--}}
{{--				{{ json_encode($data) }}--}}
{{--			</td>--}}
{{--		</tr>--}}
		<tr>
			<td align="center" style="padding:30px 0 20px;">
				<strong style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">{{ ucfirst($data['type']) }}
					URL:</strong><br>
				<span style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">
					<a href="{{ $data['url'] }}"
					   style="font-size:16px;line-height:20px;letter-spacing:-0.01em;font-weight:bold;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#500ade;"
					   target="_blank">{{ $data['id'] }}</a>
				</span>
			</td>
		</tr>
	</table>

@endsection


