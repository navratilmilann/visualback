@extends('layouts.mail')

@section('content')

    <!-- BODY  -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0"
           style="min-width:100%;width:100%;background-color: #fff;" bgcolor="#ffffff">
        <tr>
            <td align="center"
                style="padding:15px 20px 0;font-size:24px;line-height:28px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Semibold',SUWR,Arial,Sans-Serif;color:#505050;">
                <strong>ACCOUNT DELETION</strong>
            </td>
        </tr>
        <tr>
            <td align="center"
                style="padding:25px 30px 55px;font-size:16px;line-height:18px;letter-spacing:-0.01em;font-weight:normal;font-family:'Segoe UI','Segoe UI Regular',SUWR,Arial,Sans-Serif;color:#505050;">
                Dear <strong>{{ $user->name }}</strong>,<br>
                We are glad that you spent an amazing time with us and we wish you much success.
            </td>
        </tr>
    </table>

@endsection


