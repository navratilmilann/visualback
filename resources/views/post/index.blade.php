@extends('layouts.shared')

@section('content')
	<div id="app">
		<post-index
				:parameters="{{ json_encode(['tag'=>request()->tag_slug]) }}"
		></post-index>
		@include('partials.validator')
	</div>
@endsection
