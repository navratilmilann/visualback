@extends('layouts.shared')

@section('content')
	<div id="app">
		<post-detail
				:parameters="{{ json_encode(['id'=>request()->id, 'slug'=>request()->slug]) }}"
		></post-detail>
		@include('partials.validator')
	</div>
@endsection
