<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1">

<!-- Meta -->
@if(!empty($live_preview) && !empty($site))
	<!-- Livepreview -->
	<title>{{ $site->name }} - {{ config('app.name') }}</title>
	<meta property="og:title" content="{{ $site->name }} - {{ config('app.name') }}"/>

	@if(!empty($site->attachment))
		<meta property="og:image" content="{{ asset($site->attachment->path) }}?v=1"/>
	@else
		<meta property="og:image" content="{{ asset('img/social/logo.jpg') }}?v=1"/>
	@endif
@else
	<!-- Others -->
	<title>{{ config('app.name') }} - {{ $seo['title'] }}</title>
	<meta property="og:title" content="{{ $seo['title'] }}"/>
	<meta property="og:image" content="{{ asset('img/social/logo.jpg') }}?v=1"/>
@endif

<meta property="og:url" content="{{ request()->url() }}"/>
<meta property="og:type" content="website"/>
<meta property="og:description" content="{{ $seo['description'] }}"/>
<meta name="description" content="{{ $seo['description'] }}">
<meta property="og:image:width" content="200"/>
<meta property="og:image:height" content="200"/>
<meta property="fb:app_id" content="151847417117215"/>
<!-- General -->
<meta name="author" content="{{ config('app.name') }}">
<meta name="keywords" content="{{ $seo['keywords'] }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="recaptcha-key" content="{{ config('services.recaptcha.key') }}"/>
<meta name="base-url" content="{{ url('/') }}"/>
<meta name="current-url" content="{{ \Request::url() }}">

@if(config('app.debug') || !empty($live_preview))
	<meta name="robots" content="noindex, nofollow"/>
@endif

<!-- Favicons-->
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">
<link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<link rel="canonical" href="{{ request()->url() }}"/>
<!-- Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">
<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/all.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}"/>
<link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/global.css') }}?{{ time() }}" rel="stylesheet">

@if(!empty($live_preview))
	<!-- Livepreview -->
	<link href="{{ asset('css/live-preview.css') }}?{{ time() }}" rel="stylesheet">

	@if(\Route::currentRouteName() === 'site.demoLivepreview')
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/4.3.0/introjs.min.css"
		      integrity="sha512-YZO1kAqr8VPYJMaOgT4ZAIP4OeCuAWoZqgdvVYjeqyfieNWrUTzZrrxpgAdDrS7nV3sAVTKdP6MSKhqaMU5Q4g=="
		      crossorigin="anonymous" referrerpolicy="no-referrer"/>
	@endif

@elseif(!empty($post))
	<!-- Blog -->
	<link href="{{ asset('css/post.css') }}?{{ time() }}" rel="stylesheet">
@elseif(!empty($auth))
	<!-- Auth -->
	<link href="{{ asset('css/auth.css') }}?{{ time() }}" rel="stylesheet">
@else
	<!-- Other -->
	<link href="{{ asset('css/shared.css') }}?{{ time() }}" rel="stylesheet">
@endif

<!-- Recaptcha -->
<script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.key') }}"></script>

@if(config('app.env') == 'production' && !empty(request()->cookie('cookie_consent')))
	<!-- Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-213430689-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());

		gtag('config', 'UA-213430689-1');
	</script>
	<!-- End Google Analytics -->
	<!-- Google Tag Manager -->
	<script>(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-55Z9V6S');
	</script>
	<!-- End Google Tag Manager -->
	<!-- SmartLook -->
	<script>
		window.smartlook || (function (d) {
			var o = smartlook = function () {
				o.api.push(arguments)
			}, h = d.getElementsByTagName('head')[0];
			var c = d.createElement('script');
			o.api = new Array();
			c.async = true;
			c.type = 'text/javascript';
			c.charset = 'utf-8';
			c.src = 'https://rec.smartlook.com/recorder.js';
			h.appendChild(c);
		})(document);
		smartlook('init', 'ea51521a7e355b7f5233f8a9caee78279680d5e8');
	</script>
@endif
