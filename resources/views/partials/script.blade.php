@if(!empty($live_preview))

	@if(\Route::currentRouteName() === 'site.demoLivepreview')
		<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/4.3.0/intro.min.js"
		        integrity="sha512-WYNEDpX7FCz0ejmdUFl444n+v7gDgDFYmxy2YBx99v15UUk3zU5ZWYFBXFCvWYvd+nv/guwUnXmrecK7Ee0Wtg=="
		        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	@endif

	<script src="{{ asset('js/live-preview.js') }}?{{ time() }}"></script>
@elseif(!empty($auth))
	<script src="{{ asset('js/auth.js') }}?{{ time() }}"></script>
@elseif(!empty($post))
	<script src="{{ asset('js/post.js') }}?{{ time() }}"></script>
@else
	@include('cookieConsent::index')
	<script src="{{ asset('js/shared.js') }}?{{ time() }}"></script>
@endif

<script>
	$(document).ready(function () {

		let breakpoint_px;

		if ($('.page').is('#home')) {
			breakpoint_px = $('section:first').offset().top + $('section:first').outerHeight();
		} else {
			breakpoint_px = 100;
		}

		$('body').on('scroll', function (event) {
			let scroll = $('body').scrollTop();

			if (scroll > breakpoint_px) {
				$('header').addClass('sticky')
			} else {
				$('header').removeClass('sticky')
			}
		});

		$('body').on('click', '.js-responsive-button', function (event) {
			event.preventDefault();
			$('body').toggleClass('open-menu');
			$('.js-responsive-menu').fadeToggle(200);
		})
	})
</script>

@yield('scripts')
