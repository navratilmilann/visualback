@if ($errors->any())
	<flash-message
			message="{{ implode('<br>',$errors->all()) }}"
			message-type="error"
	></flash-message>
@endif

@if (\Session::has('error'))
	<flash-message
			message="{{ \Session::pull('error') }}"
			message-type="error"
	></flash-message>
@endif

@if (\Session::has('success'))
	<flash-message
			message="{{ \Session::pull('success') }}"
			message-type="success"
	></flash-message>
@endif

@if (\Session::has('info'))
	<flash-message
			message="{{ \Session::pull('info') }}"
			message-type="warning"
	></flash-message>
@endif

@if (\Session::has('warning'))
	<flash-message
			message="{{ \Session::pull('warning') }}"
			message-type="warning"
	></flash-message>
@endif
