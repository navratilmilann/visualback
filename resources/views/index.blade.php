@extends('layouts.shared')

@section('content')
	<div id="app">
		<index></index>
		@include('partials.validator')
	</div>
@endsection
