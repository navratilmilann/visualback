<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body @if(config('constants.is_beta')) class="beta" @endif>
<div id="app">
	<auth-register
			old-inputs="{{ json_encode(old()) }}"
	></auth-register>
	@include('partials.validator')
</div>

@include('partials.script')
</body>
</html>
