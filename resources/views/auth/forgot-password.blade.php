<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body @if(config('constants.is_beta')) class="beta" @endif>
<div id="app">
	<auth-forgot-password></auth-forgot-password>
	@include('partials.validator')
</div>

@include('partials.script')
</body>
</html>
