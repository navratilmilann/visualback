<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body @if(config('constants.is_beta')) class="beta" @endif>
<div id="app">
	<auth-verification-account
			hash="{{ $hash }}"
			requested-user="{{ $user }}"
	></auth-verification-account>
	@include('partials.validator')
</div>
@include('partials.script')
</body>
</html>
