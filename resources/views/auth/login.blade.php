<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body @if(config('constants.is_beta')) class="beta" @endif>
<div id="app">
	<auth-login
			old-inputs="{{ json_encode(old()) }}"
			user-identifier="{{ session()->get('user-identifier') }}"
			is-gmail="{{ session()->get('is-gmail') }}"
	></auth-login>
	@include('partials.validator')
</div>

@include('partials.script')
</body>
</html>
