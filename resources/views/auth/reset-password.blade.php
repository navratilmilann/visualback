<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body @if(config('constants.is_beta')) class="beta" @endif>
<div id="app">
	<auth-reset-password
			token="{{ $token }}"
	></auth-reset-password>
	@include('partials.validator')
</div>

@include('partials.script')
</body>
</html>
