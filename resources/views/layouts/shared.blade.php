<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body class="@if(config('constants.is_beta')) beta @endif loading">

@include('partials.loader')

@if(config('app.env') == 'production' && !empty(request()->cookie('cookie_consent')))
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-55Z9V6S"
		        height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
@endif
@yield('content')
<!-- Responsive menu -->
@if(Route::currentRouteName() !== 'site.livePreview')
	<div class="js-responsive-menu responsive-menu">
		<div class="custom-container">
			<button class="responsive-button js-responsive-button hidden-sm-up">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</button>
		</div>
		<div class="wrapper">
			<ul>
				@auth
					<li>
						<a href="{{ route('profile.settings',\Auth::user()->identifier) }}">Settings</a>
					</li>
					<li>
						<a href="{{ route('profile.detail',\Auth::user()->identifier) }}">Profile</a>
					</li>
				@endauth
				@guest
					<li>
						<a href="{{ route('login') }}">Log In</a>
					</li>
					<li>
						<a href="{{ route('register') }}">Sign Up</a>
					</li>
				@endguest

			</ul>
		</div>
	</div>
@endif

@include('partials.validator')

@include('partials.script')

@yield('scripts')

</body>
</html>
