<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
	<o:OfficeDocumentSettings>
		<o:PixelsPerInch>96</o:PixelsPerInch>
	</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html;charset=utf-8"/>
	<meta name="format-detection" content="telephone=no"/>
	<meta name="format-detection" content="date=no"/>
	<meta name="format-detection" content="address=no"/>
	<meta name="format-detection" content="email=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		.ExternalClass * {
			line-height: 100%;
		}

		a {
			text-decoration: none !important;
		}

		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}

		b {
			font-weight: normal;
		}

		table {
			border-collapse: collapse;
			margin: 0;
			-ms-text-size-adjust: 100%;
			-webkit-text-size-adjust: 100%;
			mso-table-lspace: 0;
			mso-table-rspace: 0;
		}

		tr {
			font-size: 0px;
			mso-line-height-alt: 0px;
			mso-margin-top-alt: 0px;
			mso-line-height-rule: exactly;
		}

		td {
			margin: 0;
			padding: 0;
			mso-line-height-rule: exactly;
		}

		img {
			margin: 0;
			padding: 0;
			-webkit-user-select: none;
			-ms-interpolation-mode: bicubic;
		}

		input::-moz-focus-inner {
			border: 0;
			outline: 0;
		}

		.-liox-gmailapp-fix {
			display: none;
		}

		.-liox-ol-ignore {
			-lioxkit-outlook: ignore-all-classes;
		}

		.-liox-ff-semibold {
			font-weight: 600 !important;
		}
	</style>
	<style type="text/css">
		@import url(https://asgcdn.azureedge.net/general/fonts/fontface.css);

		@media screen and (max-device-width: 480px),(max-width: 480px) {
			body[yahoofix] {
				padding: 0 !important;
				margin: 0 !important;
			}

			body[yahoofix] .mob_full_width {
				width: 100% !important;
			}

			body[yahoofix] .mob_full_width_image {
				width: 100% !important;
				height: auto;
			}

			body[yahoofix] .mob_padding {
				padding-left: 10px !important;
				padding-right: 10px !important;
			}

			body[yahoofix] .mob_width {
				width: 10px !important;
				max-width: 10px !important;
				min-width: 0 !important;
			}

			body[yahoofix] .mob_width0 {
				width: 0 !important;
				max-width: 0 !important;
				display: none !important;
				min-width: none !important;
			}

			body[yahoofix] .mob_hide {
				display: none !important;
			}

			body[yahoofix] .mob_unhide {
				float: left !important;
				width: auto !important;
				height: 22px !important;
				display: inline-block !important;
				max-height: none !important;
			}

			body[yahoofix] .mob_right {
				float: right !important;
				height: 22px !important;
				width: auto !important;
			}

			body[yahoofix] .mob_fontsize18 {
				font-size: 18px !important;
			}
		}
	</style>
	<title>VisualBack newsletter</title>
</head>

<body yahoofix="yahoofix" style="color:#ffffff;">

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff"
       style="min-width:100%;background-color:#f4f5f9;">
	<tr>
		<td align="center">

			<table class="mob_full_width" width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffd500"
			       style="max-width:600px;width:600px;background-color: #ffd500;margin: 40px 0;border-radius: 10px;overflow: hidden;">
				<tr style="padding:20px 0;background-color:#ffd500;">
					<td class="mob_width0" width="20" height="10" style="min-width:20px;width:20px;height:10px;">
					</td>
					<td height="20" style="height:20px;" align="center">
					</td>
				</tr>
				<tr style="padding:50px 0 20px 0;">
					<td class="mob_width0" width="20" height="10" style="min-width:20px;width:20px;height:10px;">
					</td>
					<td height="70" style="height:110px;background-color: #fff;border-radius:10px 10px 0 0;"
					    align="center">
						<a href="#" target="_blank">
							<img src="{{ asset(config('constants.base_logo_png')) }}"
							     width="180" height="71" alt="" border="0"
							     style="display:block;image-rendering:-webkit-optimize-contrast;"/>
						</a>
					</td>
				</tr>

				<tr style="padding:20px 0;">
					<td class="mob_width0" width="20" height="10" style="min-width:20px;width:20px;height:10px;">
					</td>
					<td align="center" style="background-color: #fff;border-radius: 0 0 10px 10px;overflow: hidden;">
						@yield('content')
					</td>
					<td class="mob_width0" width="20" style="min-width:20px;width:20px;height:10px;">
					</td>
				</tr>


				<!-- FOOTER -->
				<!-- HEADLINE Logo: Social Media -->
				@if(!empty(config('constants.facebook')) || !empty(config('constants.discord')))
					<tr>
						<td class="mob_width0" width="20" style="min-width:20px;width:20px;">

						</td>
						<td class="mob_padding mob_hide" style="padding: 20px 0 20px;background-color: #ffd500;">
							<table align="center" cellspacing="0" cellpadding="0" border="0" width="100%"
							       style="min-width:100%;width:100%;">
								<tr align="center">

									@if(!empty(config('constants.facebook')))
										<td height="23" align="center"
										    style="padding-top:3px;padding-right:14px;display:inline-block;line-height:28px;">
											<a href="{{ config('constants.facebook') }}" target="_blank">
												<img src="{{ url('/img/social/mail/facebook.png') }}?v=0.2"
												     alt="Facebook" width="38" height="38" border="0"
												     style="display:inline-block;background-color: #5009de;border-radius: 50%;padding: 4px;"/>
											</a>
										</td>
									@endif

									@if(!empty(config('constants.discord')))
										<td height="23" align="center"
										    style="padding-top:3px;padding-right:14px;display:inline-block;line-height:28px;">
											<a href="{{ config('constants.discord') }}" target="_blank">
												<img src="{{ url('/img/social/mail/discord.png') }}?v=0.2"
												     alt="Facebook" width="38" height="38" border="0"
												     style="display:inline-block;background-color: #5009de;border-radius: 50%;padding: 4px;"/>
											</a>
										</td>
									@endif

								</tr>
							</table>
						</td>
						<td class="mob_width0" width="20" style="min-width:20px;width:20px;">

						</td>
					</tr>
				@endif
				<tr style="padding:20px 0;">
					<td class="mob_width0" width="20" style="min-width:20px;width:20px;">

					</td>
					<td class="mob_padding0" style="padding:0 0 30px;">
						<table cellspacing="0" cellpadding="0" border="0" width="100%"
						       style="min-width:100%;width:100%;">
							<tr>
								<td align="center" class="mob_padding"
								    style="padding:18px 20px 18px;font-size:14px;line-height:18px;font-weight:normal;font-family:'Segoe UI','Segoe UI Regular',SUWR,Arial,Sans-Serif;color:#505050;">
									Copyright (C) 2021 <a
											href="https://www.visualback.com" target="_blank"
											style="color:#505050;"><strong
												style="font-weight:normal;color:#505050;"><u>VisualBack</u></strong></a>
									All rights reserved.<br><br>
									Our mailing address is: <br> <a
											href="mailto:{{ config('constants.email') }}" target="_blank"
											style="color:#505050;"><strong
												style="font-weight:normal;color:#505050;"><u>{{ config('constants.email') }}</u></strong></a>

									{{--									@if(!empty($isUnsubscribe))--}}
									{{--										<br><br>--}}
									{{--										You can unsubscribe newsletter <a--}}
									{{--												href="{{ route('newsletter.unsubscribe',['identifier'=>$hash]) }}"--}}
									{{--												target="_blank" style="color:#505050;"><strong--}}
									{{--													style="font-weight:normal;color:#505050;font-size:14px;line-height:18px;"><u>here</u></strong></a>--}}
									{{--										.--}}
									{{--									@endif--}}

								</td>
							</tr>
						</table>
					</td>
					<td class="mob_width0" width="20" style="min-width:20px;width:20px;">

					</td>
				</tr>

			</table>
		</td>
	</tr>

</table>
<custom name="opencounter" type="tracking">
</body>
</html>
