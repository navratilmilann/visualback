@extends('layouts.shared')

@section('content')
	<div id="app">
		<site-edit
				form-url="{{ route('site.update',['identifier'=>$site->identifier]) }}"
				site-type="{{ $siteType }}"
				redirect-url="{{ $redirectUrl }}"
				site-technologies="{{ $siteTechnologies }}"
				site-data="{{ $site }}"
				type-options="{{ $typeOptions }}"
				technology-options="{{ $technologyOptions }}"
		></site-edit>
		@include('partials.validator')
	</div>
@endsection

