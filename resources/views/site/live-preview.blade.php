<!DOCTYPE html>
<html lang="en">
@include('partials.head')
<body id="live-preview" class="@if(config('constants.is_beta')) beta @endif loading">
@include('partials.loader')
<div id="app">
	<live-preview
			site-data="{{ $site }}"
			app-url="{{ get_app_url($site->identifier,$site->uri) }}"
	></live-preview>
	@include('partials.validator')
</div>

@include('partials.script')

</body>

