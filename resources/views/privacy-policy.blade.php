@extends('layouts.shared')

@section('content')
	<div id="app">
		<privacy-policy></privacy-policy>
		@include('partials.validator')
	</div>
@endsection
