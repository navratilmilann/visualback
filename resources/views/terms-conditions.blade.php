@extends('layouts.shared')

@section('content')
	<div id="app">
		<terms-conditions></terms-conditions>
		@include('partials.validator')
	</div>
@endsection
