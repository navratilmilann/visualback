@extends('layouts.shared')

@section('content')
	<div id="app">
		<about-us></about-us>
		@include('partials.validator')
	</div>
@endsection
