// https://ourcodeworld.com/articles/read/328/top-10-best-tour-website-guide-javascript-and-jquery-plugins

function startDemoTutorial({commit}, that) {

	let intro = window.introJs(),
		iframe = $('#live-preview-iframe').contents();

	intro.setOptions({
		exitOnOverlayClick: false,
		showBullets: false,
		showProgress: true,
		disableInteraction: true,
		steps: [
			{
				title: 'Quick Tips',															// step 1
				intro: 'A user has recently published his personal portfolio and he\'s looking for constructive front-end feedback.',
			}, {
				title: 'Localized feedback',															// step 3
				element: document.getElementById('live-preview-iframe'),
				intro: 'Someone already left a comment on this site. What does it say?',
				position: 'left'
			}, {
				title: 'Reviews',															// step 4
				element: document.querySelector('aside'),
				intro: 'Here are all the comments already written by someone. Click on it to show details.',
				position: 'right'
			}, {
				title: 'Comment Detail',															// step 5
				element: document.querySelector('aside'),
				intro: '<ul>' +
					'<li>Leave a heart, if you like the comment</li>' +
					'<li>Report it, if you think the content is inappropriate</li>' +
					'<li>Compile user\'s suggested CSS code</li>' +
					'</ul>',
				position: 'right'
			}, {
				title: 'Responsive Web',															// step 6
				element: document.querySelector('.resolution'),
				intro: 'If you wish to see the site on a tablet or a phone view, to make it happen, click on an icon.',
				position: 'top'
			}, {
				title: 'Browse/Comment mode',															// step 7
				element: document.querySelector('.mode'),
				intro: 'Turn on the browse mode to fully browse the site above.',
				position: 'top'
			}, {
				title: 'Localized comment',															// step 7
				element: document.querySelector('#live-preview-iframe'),
				intro: 'To leave a localized comment:<ul>' +
					'<li>Make sure you are in a comment mode</li>' +
					'<li>Find a visual bug and left click to leave a pin</li>' +
					'<li>Write a text comment, CSS styles or add an attachment</li>' +
					'</ul>',
				position: 'top'
			},
			{
				title: 'Challenge!',															// step 8
				intro: 'We have a challenge for you! Choose a place to leave a pin and write a comment. Have fun!',
			}
		]
	}).start();

	intro.onafterchange(async (targetElement) => {

		if (intro._currentStep === 1) {
			iframe.find('.pin[data-count="1"]').addClass('highlight-comment')
		} else if (intro._currentStep === 2) {
			commit('setLivepreviewSettings', {
				open_sidebar: true,
				sidebar_type: 'listing',
			});
			iframe.find('.highlight-comment').removeClass('highlight-comment')
		} else if (intro._currentStep === 3) {
			that.toggleResolution('desktop')
			commit('setLivepreviewSettings', {
				sidebar_type: 'detail',
			});
			$('#listing-sidebar .comment:last-child').trigger('click');
		} else if (intro._currentStep === 4) {
			that.toggleResolution('phone')
		}
	});

	intro.oncomplete(function () {
		iframe.find('.pin[data-count="1"]').removeClass('highlight-comment')
		that.toggleResolution('desktop')
	});
}

export {
	startDemoTutorial
}
