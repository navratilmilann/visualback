import {get_attachment_by_type} from "../includes/helpers";

/**
 * @param commit
 * @param that
 * @param response
 */
function storeReportCallback(commit, that, response) {

	that.removeEntity();

	commit('setFlashMessage', {
		type: response.error ? 'error' : 'success',
		message: response.message
	});

	that.show_modal = false;
}

/**
 * @param commit
 * @param state
 * @param modal
 */
function removeCommentCallback({commit, state}, modal) {

	if (modal.type === 'comment') {

		$($('iframe').contents()).find(`.pin[data-id=${modal.item.id}]`).remove();

		commit('removeComment', modal.item);

		commit('setLivepreviewSettings', {
			sidebar_type: 'listing',
			current_comment: null,
			current_replies: [],
			last_pin_count: state.livepreview_settings.last_pin_count - 1
		});

	} else if (modal.type === 'reply') {

		commit('removeCommentReply', modal.item);

		commit('updateCommentReplies', modal.item);
	}
}

/**
 * @param state
 * @param commit
 * @param response
 */
function addReplyCallback(state, commit, response) {
	if (!response.error) {
		commit('addCommentReply', response.site_comment);

		commit('setLivepreviewSettings', {
			current_comment: state.livepreview_settings.current_comment,
			sidebar_type: 'detail'
		});
	}

	commit('setFlashMessage', {
		type: response.error ? 'error' : 'success',
		message: response.message,
	});
}

/**
 * @param commit
 * @param state
 * @param that
 * @param response
 */
function updateCommentCallback(commit, state, that, response) {
	that.comment.text = that.comment_edit_mode_text;

	that.comment.attachment = that.file_uploader.files.concat(get_attachment_by_type(that.comment.attachment, 'css'));

	that.toggleEditMode();

	commit('setLivepreviewSettings', {
		comment_edit_mode: false,
		comment_edit_mode_text: '',
		current_comment: that.comment
	});

	commit('updateComment', state.livepreview_settings.current_comment);

	commit('setGallery', state.file_uploader.files);

	commit('setFileUploader', {
		files: [],
		updated: false
	});

	commit('setFlashMessage', {
		type: response.error ? 'error' : 'success',
		message: response.message,
	});
}

/**
 * @param payload
 * @param state
 * @param commit
 * @param response
 */
function storeCommentCallback(payload, state, commit, response = {}) {
	if (!response.error) {

		if (!state.auth_user && !state.livepreview_settings.is_demo) {
			commit('toggleUnregisteredUserModal', {show_modal: true});
		}

		commit('addComment', response.site_comment);

		commit('setLivepreviewSettings', {
			sidebar_type: 'listing',
			last_pin_count: state.livepreview_settings.last_pin_count + 1,
		});

		commit('setFileUploader', {
			files: [],
			updated: false
		});

		$($('iframe').contents())
			.find(`.pin[data-count=${payload.data.count}]`)
			.attr('data-id', response.site_comment.id);
	}
	// Vuex
	commit('setFlashMessage', {
		type: !response.error ? 'success' : 'error',
		message: response.message
	});
}

/**
 * Return demo comment entity
 * @param payload
 * @param state
 * @returns {{site_comment, message: string}}
 */
function getDemoCommentData(payload, state) {
	payload.data.replies = [];
	payload.data.attachment = [];
	payload.data.visitor_is_author = true;
	payload.data.id = payload.data.count;
	payload.data.rating = {0: 0, 1: 0};

	if (state.file_uploader.has_files) {
		payload.data.attachment = state.file_uploader.files;
	}

	if (state.css_editor.file) {
		payload.data.attachment.push({type: 'css', url: state.css_editor.file.url, id: state.css_editor.file.id});
	}

	return {
		site_comment: payload.data,
		message: 'Comment was succesfully published.'
	};
}

/**
 * @param that
 * @param commit
 * @param response
 */
function updateReplyCallback(that, commit, response) {
	that.comment.text = that.reply_edit_mode_text;

	that.toggleEditMode();

	commit('updateCommentReply', that.comment);

	commit('setLivepreviewSettings', {
		reply_edit_mode: false
	});

	commit('setFlashMessage', {
		type: response.error ? 'error' : 'success',
		message: response.message,
	});
}

export {
	storeReportCallback,
	updateReplyCallback,
	removeCommentCallback,
	addReplyCallback,
	updateCommentCallback,
	storeCommentCallback,
	getDemoCommentData
}
