// https://laracasts.com/discuss/channels/vue/vue-add-a-service-to-the-app-or-component

let is_checking_pins = false,
	running_timeout;

/**
 *
 * @param resolution
 * @param iframe_window
 * @param comments
 * @param iframe
 */
function reattachPinsToIframe(resolution, iframe_window, comments, iframe) {

	let location_pathname = iframe_window.document.location.pathname;

	for (let i = 0; i < comments.length; i++) {
		if (correctSubpageAndDevice(comments[i], resolution, location_pathname)) {
			setPinPosition(iframe.find(`.pin[data-count=${comments[i].count}]`), comments[i]);
		} else {
			iframe.find(`.pin[data-count=${comments[i].count}]`).remove();
		}
	}

	// checkExisitingPins(iframe_window, comments, resolution);    														//  checking after 3 seconds
}

/**
 *
 * @param comment
 * @returns {*}
 */
function createPin(comment) {
	let pin;

	pin = $(`<div></div>`)
	pin.addClass('pin')
		.addClass('visible');
	pin.attr('data-count', comment.count)
	pin.attr('data-id', comment.id)

	pin = setPinPosition(pin, comment);

	return pin;
}

/**
 *
 * @param comment
 * @param iframe
 */
function appendPin(comment, iframe) {
	let pin = createPin(comment);

	iframe.find(comment.target).parent().append(pin);
}

/**
 *
 * @param pin
 * @param comment
 * @returns {*}
 */
function setPinPosition(pin, comment) {

	let actual_width = window.innerWidth || document.body.clientWidth;

	pin.css('top', comment.y)
		.css('left', comment.relative ? comment.x : (comment.x / comment.width) * actual_width);

	return pin;
}

/**
 *
 * @param iframe
 */
function removePins(iframe) {
	let pins = iframe.find('.pin[data-count]');

	if (pins.length) {
		pins.remove();
	}
}

/**
 *
 * @param pin
 */
function removePinById(pin) {
	if (pin.length) {
		pin.remove();
	}
}

/**
 *
 * @param event
 * @returns {Element}
 */
function getElementOnExpectedPinPosition(event) {

	let element = document.elementFromPoint(event.clientX + 5, event.clientY + 5);

	if (element instanceof HTMLIFrameElement) {
		element = element.contentWindow.document.elementFromPoint(event.clientX + 5, event.clientY + 5);
	}

	return element;
}

/**
 *
 * @param event
 * @param settings
 * @returns {*}
 */
function attachPinToSite(event, settings) {

	let target = $(event.target),
		parent = target.parent(),
		pin = $(`<div></div>`),
		child_coordinate,
		child_coordinates,
		child_coordinate_count,
		i;

	pin.addClass('pin').addClass('temp');
	pin.attr('data-count', settings.last_pin_count);

	while (parent && !parent.is('body') && parent.css('position') !== 'relative' && parent.css('position') !== 'absolute') {
		parent = parent.parent();
	}

	child_coordinates = getChildCoordinatesTypes(event, target, parent);
	child_coordinate_count = child_coordinates.length;

	if (parent === undefined || parent.prop("tagName") === 'BODY') {
		for (i = 0; i < child_coordinate_count; i++) {
			pin.css('left', child_coordinates[i].left)
				.css('top', child_coordinates[i].top)

			if (i === 0) {
				target.parent().append(pin);
			}

			if (getElementOnExpectedPinPosition(event) === pin[0]) {
				child_coordinate = child_coordinates[i];
				break;
			}
		}

		if (i === child_coordinate_count) {
			child_coordinate = child_coordinates[child_coordinate_count - 1];
		}
	} else {
		pin.css('left', child_coordinates[1].left)
			.css('top', child_coordinates[1].top);

		target.parent().append(pin);

		child_coordinate = child_coordinates[1];
	}

	target.removeClass('pause-animation');

	pin.addClass('visible');

	return child_coordinate;
}

/**
 *
 * @param event
 * @param target
 * @param parent
 * @returns {({top, left, relative: number}|{top: number, left: number, relative: number}|{top: number, left: number, relative: number}|{top: (boolean|number|*), left: (boolean|number|*), relative: number}|{top: *, left: *, relative: number})[]}
 */
function getChildCoordinatesTypes(event, target, parent) {
	return [{
		left: event.pageX,
		top: event.pageY,
		relative: 0
	}, {
		left: event.pageX - parent.offset().left,
		top: event.pageY - parent.offset().top,
		relative: 1
	}, {
		left: event.pageX - target.offset().left,
		top: event.pageY - target.offset().top,
		relative: 1
	}, {
		left: event.clientX,
		top: event.clientY,
		relative: 1
	}, {
		left: target.position().left + event.offsetX,
		top: target.position().top + event.offsetY,
		relative: 1
	}, {
		left: event.target.offsetLeft + event.offsetX,
		top: event.target.offsetTop + event.offsetY,
		relative: 1
	}];
}

/**
 *
 * @param comment
 * @param resolution
 * @param location_pathname
 * @returns {boolean}
 */
function correctSubpageAndDevice(comment, resolution, location_pathname) {
	return resolution === comment.device && location_pathname === comment.uri;
}

/**
 *
 * @param resolution
 * @param iframe_window
 * @param comments
 * @param iframe
 */
function attachPinsToIframe(resolution, iframe_window, comments, iframe) {

	let location_pathname = iframe_window.document.location.pathname;

	for (let i = 0; i < comments.length; i++) {
		if (correctSubpageAndDevice(comments[i], resolution, location_pathname)) {
			appendPin(comments[i], iframe);
			setNotFound(comments, 0, i);
		} else {
			removePinById(findPinById(comments[i].id))
			setNotFound(comments, 1, i);
		}
	}

	checkExisitingPins(iframe_window, comments, resolution);    														//  checking after 3 seconds
}

/**
 *
 * @param iframe_window
 * @param comments
 * @param resolution
 */
function checkExisitingPins(iframe_window, comments, resolution) {														// checking, if existing pins

	let location_pathname = iframe_window.document.location.pathname,
		iframe = $($('iframe').contents());

	is_checking_pins = true;

	window.clearTimeout(running_timeout);

	running_timeout = setTimeout(function () {

		for (let i = 0; i < comments.length; i++) {
			if (correctSubpageAndDevice(comments[i], resolution, location_pathname)) {
				if (!iframe.find(`.pin[data-id=${comments[i].id}]`).length) {					// pin is not in iframe
					if (!iframe.find(comments[i].target).length) {								// place for pin is not found
						setNotFound(comments, 1, i);
					} else {																	// place for pin is found, but pin is not found
						appendPin(comments[i], iframe);
						setNotFound(comments, 0, i);
					}
				}
			}
		}

		is_checking_pins = false;

	}, 3000);
}

/**
 *
 * @param comments
 * @param value
 * @param i
 */
function setNotFound(comments, value, i) {

	if (comments[i].not_found === value) return;

	comments[i].not_found = value;

	Vue.set(comments, i, comments[i]);
}

/**
 *
 * @param id
 * @returns {*|Window.jQuery}
 */
function findPinById(id) {
	return $($('iframe').contents()).find(`.pin[data-id=${id}]`);
}

/**
 * Remove potential new comment pin
 */
function removeTemporaryPin() {
	$($('iframe').contents()).find('.pin.temp').remove();
}

export {
	reattachPinsToIframe,
	attachPinsToIframe,
	removePins,
	attachPinToSite,
	findPinById,
	removeTemporaryPin
}
