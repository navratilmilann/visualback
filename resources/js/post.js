import JQuery from 'jquery'
import VTooltip from "v-tooltip";
import VueSocialSharing from 'vue-social-sharing'
import store from './store/post/store';
import Gate from "./policies/Gate";

window.Vue = require('vue');
window.$ = JQuery;

require('./includes/helpers');
require('./includes/filters');

window._token = document.head.querySelector('meta[name="csrf-token"]').content;

window.Vue.prototype.$gate = new Gate();

Vue.component('post-detail', require('./components/post/Detail.vue').default);
Vue.component('post-index', require('./components/post/Index.vue').default);

Vue.use(VueSocialSharing);
Vue.use(VTooltip);

const shared = new Vue({
	el: '#app',
	store,
});
