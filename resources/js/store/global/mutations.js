export default {

	setPageReady: function (state, payload) {
		state.page_ready = payload;
	},

	setUserLoaded: function (state, payload) {
		state.user_loaded = payload;
	},

	showException: function (state, payload) {
		this.commit('setFlashMessage', {
			type: 'error',
			message: payload ? payload : 'Something went wrong. Please try again later.'
		});
	},

	showSuccess: function (state, payload) {
		this.commit('setFlashMessage', {
			type: 'success',
			message: payload
		});
	},

	setFlashMessage: function (state, payload) {

		let that = this;

		state.flash_message = payload;
		state.flash_message.show = true;

		if (!payload.infinite) {

			setTimeout(function () {
				that.commit('flushFlashMessage');
			}, 8000);
		}
	},

	flushFlashMessage: function (state) {
		state.flash_message = {
			message: null,
			type: null,
			show: false
		};
	},

	setAuthUser: function (state, payload) {

		if (payload && !$.isEmptyObject(payload))
			state.auth_user = payload;
		else
			state.auth_user = null;
	},

	setNotificationCount: function (state, payload) {
		state.notification_count = payload;
	},

	setSiteCount: function (state, payload) {
		state.site_count = payload;
	},

	/**
	 * Modals
	 */
	openModal: function (state, payload) {
		state.modal = payload;
		state.modal.show_modal = true;
	},

	closeModal: function (state) {
		state.modal.show_modal = false;
	},

	toggleListModal: function (state, payload) {
		state.list_modal = payload;
	},

	toggleReportModal: function (state, payload) {
		state.report_modal = payload;
	},
}
