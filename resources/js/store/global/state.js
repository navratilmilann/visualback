import constants from "../../includes/constants";

export default {

	constants: constants,
	page_ready: false,
	user_loaded: false,
	flash_message: {
		type: '',
		message: ''
	},
	auth_user: null,
	notification_count: null,
	site_count: null,
	/**
	 * Modals
	 */
	modal: {
		method: 'GET',
		request_options: null,
		title: null,
		content: null,
		description: null,
		item: null,
		type: null,
		show_modal: false,
		callback: null
	},
	list_modal: {
		title: null,
		items: null,
		type: null,
		show_modal: false,
	},
	report_modal: {
		show_modal: false,
		type: null,
		data: null
	},
}
