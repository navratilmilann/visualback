export default {
	getConstants: state => state.constants,
	getPageReady: state => state.page_ready,
	getUserLoaded: state => state.user_loaded,
	getFlashMessage: state => state.flash_message,
	getAuthUser: state => state.auth_user,
	getNotificationCount: state => state.notification_count,
	getSiteCount: state => state.site_count,
	/**
	 * Modals
	 */
	getModal: state => state.modal,
	getListModal: state => state.list_modal,
	getReportModal: state => state.report_modal,
}
