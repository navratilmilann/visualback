import {is_demo, request_options} from "../../includes/helpers";
import routes from "../../includes/routes";
import axios from "axios";
import {
	storeReportCallback,
} from "../../services/StoreService";

export default {
	setAuthUser: async function ({state, commit}) {

		let identifier = '';

		if (is_demo(state)) {
			identifier = state.constants.vb_user_indetifier;
		}

		let options = request_options('GET', routes.get_user(identifier));

		if (!state.auth_user) {

			await axios(options)
				.then((json) => {
					commit('setAuthUser', json.data);

					commit('setUserLoaded', true);
				});
		} else {
			commit('setUserLoaded', true);
		}
	},

	setNotificationCount: async function (store, payload) {

		let options = request_options('GET', routes.get_notification_count(payload));

		if (!store.state.notification_count) {

			await axios(options)
				.then((json) => {
					this.commit('setNotificationCount', json.data);
				});
		}
	},

	setSiteCount: async function (store, payload) {

		let options = request_options('GET', routes.get_site_count(payload));

		if (!store.state.notification_count) {

			await axios(options)
				.then((json) => {
					this.commit('setSiteCount', json.data);
				});
		}
	},

	storeNewsletter: async function (store, payload) {

		let that = this,
			options = request_options('POST', routes.store_newsletter(), payload);

		await axios(options)
			.then((json) => {

				that.commit('setFlashMessage', {
					type: json.data.error ? 'error' : 'success',
					message: json.data.message
				});
			});
	},

	reportEntity: async function ({commit, state}, payload) {

		let that = payload.that;

		if (is_demo(state)) {
			storeReportCallback(commit, that, {message: 'Comment has been reported'})
			return;
		}

		axios(that.modal.data.request_options)
			.then((json) => {
				storeReportCallback(commit, that, json.data)
			});
	},
}
