export default {
	getConstants: state => state.constants,
	getPageReady: state => state.page_ready,
	getUserLoaded: state => state.user_loaded,
	getFlashMessage: state => state.flash_message,
	getLivepreviewSettings: state => state.livepreview_settings,
	getComments: state => state.comments,
	getCurrentComment: state => state.livepreview_settings.current_comment,
	getAuthUser: state => state.auth_user,
	getNotificationCount: state => state.notification_count,
	getSiteCount: state => state.site_count,
	getSites: state => state.sites,
	getFilteredSites: state => state.filtered_sites,
	getFilteringCriteria: state => state.filtering_criteria,
	getIsRegisteringSite: state => state.is_registering_site,
	getPagination: state => state.pagination,
	getProfileSettings: state => state.profile_settings,
	getNotifications: state => state.notifications,
	getFollowers: state => state.followers,
	getFollowings: state => state.followings,
	getUser: state => state.user,
	getMarketingTools: state => state.marketing_tools,
	getUserStatus: state => state.user_status,
	getFileUploader: state => state.file_uploader,
	getGallery: state => state.gallery,
	getCssEditor: state => state.css_editor,

	/**
	 * Modals
	 */
	getModal: state => state.modal,
	getListModal: state => state.list_modal,
	getReportModal: state => state.report_modal,
	getRegisterSiteModal: state => state.register_site_modal,
	getUnregisteredUserModal: state => state.unregistered_user_modal,
	getShareSiteModal: state => state.share_site_modal,
}
