export default {
	setPageReady: function (state, payload) {
		state.page_ready = payload;
	},

	setUserLoaded: function (state, payload) {
		state.user_loaded = payload;
	},

	showException: function (state, payload) {
		this.commit('setFlashMessage', {
			type: 'error',
			message: payload ? payload : 'Something went wrong. Please try again later.'
		});
	},

	showSuccess: function (state, payload) {
		this.commit('setFlashMessage', {
			type: 'success',
			message: payload
		});
	},

	setFlashMessage: function (state, payload) {

		let that = this;

		state.flash_message = payload;
		state.flash_message.show = true;

		if (!payload.infinite) {

			setTimeout(function () {
				that.commit('flushFlashMessage');
			}, 8000);
		}
	},
	flushFlashMessage: function (state) {
		state.flash_message = {
			message: null,
			type: null,
			show: false
		};
	},

	setGallery: function (state, payload) {

		state.gallery = !payload || payload.length < 1 ? [] : payload
	},

	setLivepreviewSettings: function (state, payload) {
		state.livepreview_settings = {
			is_demo: payload.is_demo !== undefined ? payload.is_demo : state.livepreview_settings.is_demo,
			sidebar_type: payload.sidebar_type !== undefined ? payload.sidebar_type : state.livepreview_settings.sidebar_type,
			resolution: payload.resolution !== undefined ? payload.resolution : state.livepreview_settings.resolution,
			uri: payload.uri !== undefined ? payload.uri : state.livepreview_settings.uri,
			open_sidebar: payload.open_sidebar !== undefined ? payload.open_sidebar : state.livepreview_settings.open_sidebar,
			last_pin_count: payload.last_pin_count !== undefined ? payload.last_pin_count : state.livepreview_settings.last_pin_count,
			current_comment: payload.current_comment !== undefined ? payload.current_comment : state.livepreview_settings.current_comment,
			current_replies: payload.current_replies !== undefined ? payload.current_replies : state.livepreview_settings.current_replies,
			comment_edit_mode_text: payload.comment_edit_mode_text !== undefined ? payload.comment_edit_mode_text : state.livepreview_settings.comment_edit_mode_text,
			comment_edit_mode: payload.comment_edit_mode !== undefined ? payload.comment_edit_mode : state.livepreview_settings.comment_edit_mode,
			reply_id_edit_mode: payload.reply_id_edit_mode !== undefined ? payload.reply_id_edit_mode : state.livepreview_settings.reply_id_edit_mode,
			reply_edit_mode_text: payload.reply_edit_mode_text !== undefined ? payload.reply_edit_mode_text : state.livepreview_settings.reply_edit_mode_text,
			reply_edit_mode: payload.reply_edit_mode !== undefined ? payload.reply_edit_mode : state.livepreview_settings.reply_edit_mode,
			actual_pin: payload.actual_pin !== undefined ? payload.actual_pin : state.livepreview_settings.actual_pin,
			current_site: payload.current_site !== undefined ? payload.current_site : state.livepreview_settings.current_site,
			running_code: payload.running_code !== undefined ? payload.running_code : state.livepreview_settings.running_code,
			comment_text: payload.comment_text !== undefined ? payload.comment_text : state.livepreview_settings.comment_text
		};
	},

	clearCommentFields: function (state) {

		$('.input-file').val('');

		this.commit('setFileUploader', {
			files: [],
			updated: false
		});

		this.commit('setLivepreviewSettings', {
			comment_text: '',
			running_code: false
		})

		this.commit('setCssEditor', {
			code: '',
			updated: false,
			file: null
		})
	},

	addCommentReply: function (state, payload) {
		state.livepreview_settings.current_replies.push(payload);
	},
	removeCommentReply: function (state, payload) {
		state.livepreview_settings.current_replies = state.livepreview_settings.current_comment.replies.filter(function (reply) {
			return reply.id !== payload.id;
		});
	},
	updateCommentReply: function (state, payload) {

		let
			current_replies = state.livepreview_settings.current_replies,
			i = 0;

		for (; i < current_replies.length; i++) {

			if (payload.id === current_replies[i].id) {

				current_replies[i] = payload;
				break;
			}
		}

		state.livepreview_settings.current_replies = current_replies;
	},
	updateCommentReplies: function (state, payload) {

		let i = 0;

		for (; i < state.comments.length; i++) {

			if (payload.parent_id === state.comments[i].id) {

				state.comments[i].replies = state.livepreview_settings.current_replies;
				break;
			}
		}
	},


	setComments: function (state, payload) {
		state.comments = payload;
	},
	addComment: function (state, payload) {
		state.comments.unshift(payload);
	},
	removeComment: function (state, payload) {
		state.comments = state.comments.filter(function (comment) {
			return comment.id !== payload.id;
		});
	},
	updateComment: function (state, payload) {

		let i = 0;

		for (; i < state.comments.length; i++) {

			if (payload.id === state.comments[i].id) {
				Vue.set(state.comments, i, payload)
				// state.comments[i] = payload;
				break;
			}
		}
	},

	updateComments: function (state, payload) {
		state.comments = payload;
	},


	setAuthUser: function (state, payload) {

		if (payload && !$.isEmptyObject(payload))
			state.auth_user = payload;
		else
			state.auth_user = null;
	},

	setNotificationCount: function (state, payload) {
		state.notification_count = payload;
	},

	setSiteCount: function (state, payload) {
		state.site_count = payload;
	},

	setUser: function (state, payload) {

		if (payload && !$.isEmptyObject(payload))
			state.user = payload;
		else
			state.user = null;
	},

	mergeSites: function (state, payload) {
		state.sites = state.sites.concat(payload.sites);
	},
	setSites: function (state, payload) {
		state.sites = payload;
	},
	addSite: function (state, payload) {
		state.sites.unshift(payload);
	},
	removeSites: function (state, payload) {
		state.sites = state.sites.filter(function (site) {
			return site.id !== payload.id;
		});

		this.commit('setFilteredSites');
	},
	setFilteredSites: function (state) {

		state.filtered_sites = state.sites;
	},
	setFilterCriteria: function (state, payload) {

		state.filtering_criteria = {
			order_by: payload.order_by ? payload.order_by : state.filtering_criteria.order_by,
			search: payload.search ? payload.search : state.filtering_criteria.search
		}
	},
	setFilterSearch: function (state, payload) {
		state.filtering_criteria.search = payload;
	},
	setFilterOrderBy: function (state, payload) {
		state.filtering_criteria.order_by = payload;
	},
	setIsRegisteringSite: function (state, payload) {
		state.is_registering_site = payload;
	},


	setPagination: function (state, payload) {
		state.pagination = {
			page: payload.page ? payload.page : state.pagination.page,
			start: payload.start ? payload.start : state.pagination.start,
			end: payload.end ? payload.end : state.pagination.end,
			has_more: payload.has_more !== undefined ? payload.has_more : state.pagination.has_more,
			per_page: payload.per_page ? payload.per_page : state.pagination.per_page
		};
	},
	setPaginationPage: function (state, payload) {
		state.pagination.page = payload;
	},

	setProfileSettings: function (state, payload) {
		state.profile_settings = {
			show_profile_settings: payload.show_profile_settings,
			show_notifications: payload.show_notifications,
			show_follows: payload.show_follows,
			show_my_sites: payload.show_my_sites,
		};

		history.pushState("", document.title, payload.uri);
	},
	setFollowers: function (state, payload) {
		state.followers = payload;
	},
	setFollowings: function (state, payload) {
		state.followings = payload;
	},
	setNotifications: function (state, payload) {
		state.notifications = payload;
	},
	deleteNotification: function (state, payload) {
		state.notifications = state.notifications.filter(function (notification) {
			return notification.id !== payload.id;
		});
	},

	setTechnologies: function (state, payload) {
		state.profile_settings.technologies = payload.length ? payload : [];
	},

	setUserStatus: function (state, payload) {
		state.user_status = payload.length ? payload : [];
	},
	setMarketingTools: function (state, payload) {
		state.marketing_tools = payload;
	},

	setFileUploader: function (state, payload) {
		state.file_uploader = {
			has_files: payload.files.length > 0,
			files: payload.files,
			updated: payload.hasOwnProperty('updated') ? payload.updated : state.file_uploader.updated
		};
	},

	setCssEditor: function (state, payload) {
		state.css_editor = {
			element_id: state.css_editor.element_id,
			code: payload.hasOwnProperty('code') ? payload.code : state.css_editor.code,
			updated: payload.hasOwnProperty('updated') ? payload.updated : state.css_editor.updated,
			file: payload.hasOwnProperty('file') ? payload.file : state.css_editor.file
		};
	},

	/**
	 * Modals
	 */
	openModal: function (state, payload) {
		state.modal = payload;
		state.modal.show_modal = true;
	},
	closeModal: function (state) {
		state.modal.show_modal = false;
	},

	toggleRegisterSiteModal: function (state, payload = null) {
		state.register_site_modal.show_modal = !state.register_site_modal.show_modal;
		state.register_site_modal.site = payload ? payload : state.register_site_modal.site;
	},

	toggleListModal: function (state, payload) {
		state.list_modal = payload;
	},

	toggleReportModal: function (state, payload) {
		state.report_modal = payload;
	},

	toggleUnregisteredUserModal: function (state, payload) {
		state.unregistered_user_modal = payload;
	},

	toggleShareSite: function (state, payload) {
		state.share_site_modal = payload;
	},
}
