import constants from "../../includes/constants";

export default {

	constants: constants,

	page_ready: false,

	user_loaded: false,

	flash_message: {
		type: '',
		message: ''
	},

	livepreview_settings: {
		is_demo: false,
		sidebar_type: 'listing',
		open_sidebar: true,
		resolution: 'desktop',
		uri: '/',
		current_site: null,
		current_comment: null,
		current_replies: [],
		comment_edit_mode: false,
		comment_edit_mode_text: '',
		reply_edit_mode: false,
		reply_edit_mode_text: '',
		reply_id_edit_mode: null,
		last_pin_count: 1,
		actual_pin: {
			target: null,
			x: null,
			y: null
		},
		running_code: false,
		comment_text: ''
		// attachment_files: null,
	},

	gallery: [],

	css_editor: {
		element_id: 'visualback_stylling',
		code: '',
		updated: false,
		file: null
	},

	comments: [],

	auth_user: null,

	notification_count: null,

	site_count: null,

	sites: [],
	filtered_sites: [],
	filtering_criteria: {
		order_by: 'desc',
		search: null
	},
	is_registering_site: false,

	pagination: {
		page: 1,
		start: 0,
		end: null,
		has_more: true,
		per_page: 15
	},

	profile_settings: {
		show_profile_settings: true,
		show_notifications: false,
		show_my_sites: false,
		show_follows: false,
		technologies: [],
	},

	notifications: null,

	followers: null,

	followings: null,

	user: null,

	user_status: [],
	marketing_tools: [],

	/**
	 * Modals
	 */
	modal: {
		method: 'GET',
		request_options: null,
		title: null,
		content: null,
		description: null,
		item: null,
		type: null,
		show_modal: false,
		callback: null
	},
	register_site_modal: {
		show_modal: false,
		site: null
	},
	list_modal: {
		title: null,
		items: null,
		type: null,
		show_modal: false,
	},
	unregistered_user_modal: {
		show_modal: false
	},
	report_modal: {
		show_modal: false,
		type: null,
		data: null
	},
	share_site_modal: {
		show_modal: false,
		site: null
	},

	file_uploader: {
		has_files: false,
		files: [],
		updated: false
	}
}
