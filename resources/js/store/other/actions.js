import {is_demo, request_options} from "../../includes/helpers";
import routes from "../../includes/routes";
import axios from "axios";
import {
	addReplyCallback,
	getDemoCommentData,
	storeCommentCallback, storeReportCallback,
	updateCommentCallback, updateReplyCallback
} from "../../services/StoreService";

export default {
	/* Livepreview */
	setComments: async function ({commit, state}, payload) {
		let options = request_options('GET', routes.get_site_comments(payload));

		await axios(options)
			.then((json) => {
				commit('setComments', json.data);
			});
	},

	addCommentReply: function ({commit, state}, payload) {

		if (is_demo(state)) {
			payload.data.count = state.livepreview_settings.current_replies.length + 1;
			let data = getDemoCommentData(payload, state);

			addReplyCallback(state, commit, data);
			return;
		}

		axios(payload)
			.then((json) => {
				addReplyCallback(state, commit, json.data);
			});
	},

	toggleLike: async function ({state}, payload) {

		if (is_demo(state)) return;

		let options = request_options('POST', routes.toggle_like(payload.id, payload.type), payload);

		await axios(options)
			.then((json) => {
			});
	},

	storeComment: async function ({commit, state}, payload) {

		if (is_demo(state)) {
			let data = getDemoCommentData(payload, state);

			storeCommentCallback(payload, state, commit, data)
			return;
		}

		axios(payload)
			.then((json) => {
				storeCommentCallback(payload, state, commit, json.data)
			});
	},

	storeCommentAttachment: async function ({commit}, payload) {

		axios(payload.options)
			.then((json) => {

				commit('setCssEditor', {
					updated: false,
					file: {
						url: json.data.url,
						id: json.data.id
					}
				})
			});
	},

	deleteAttachment: async function ({commit}, payload) {

		let that = payload.that;

		axios(payload.options)
			.then(() => {

				that.attachment = {
					url: null,
					id: null
				}

				// commit('setCssEditor', {
				// 	updated: false,
				// 	file: null
				// })
			});
	},

	updateComment: async function ({commit, state}, payload) {

		let that = payload.that;

		if (is_demo(state)) {
			updateCommentCallback(commit, state, that, {message: 'Succesfully updated comment'});
			return;
		}

		axios(payload.options)
			.then((json) => {
				updateCommentCallback(commit, state, that, json.data);
			});
	},

	updateCommentReply: async function ({commit, state}, payload) {

		let that = payload.that,
			options = request_options('POST', routes.update_site_comment(that.comment.id), payload.options);

		if (is_demo(state)) {
			updateReplyCallback(that, commit, {message: 'Succesfully updated comment'})
			return;
		}

		axios(options)
			.then((json) => {
				updateReplyCallback(that, commit, json.data)
			});
	},

	reportEntity: async function ({commit, state}, payload) {

		let that = payload.that;

		if (is_demo(state)) {
			storeReportCallback(commit, that, {message: 'Comment has been reported'})
			return;
		}

		axios(that.modal.data.request_options)
			.then((json) => {
				storeReportCallback(commit, that, json.data)
			});
	},

	/*
		Shared
	 */
	setAuthUser: async function ({state, commit}) {

		let identifier = '';

		if (is_demo(state)) {
			identifier = state.constants.vb_user_indetifier;
		}

		let options = request_options('GET', routes.get_user(identifier));

		if (!state.auth_user) {

			await axios(options)
				.then((json) => {
					commit('setAuthUser', json.data);

					commit('setUserLoaded', true);
				});
		} else {
			commit('setUserLoaded', true);
		}
	},

	storeSiteScreenshot: async function (state, payload) {

		let site = payload,
			that = this;

		await axios.get(routes.store_screenshot(site.identifier))
			.then(function (json) {

				if (!json.data.error) {

					site.attachment = {
						path: json.data.data
					};

					that.commit('addSite', site);

				} else {

					that.commit('setFlashMessage', {
						message: json.data.message,
						type: 'error'
					});
				}

				that.commit('setIsRegisteringSite', false);
			});
	},

	authorizingSite: async function (state, payload) {

		let options = request_options('POST', routes.authorize_site(payload.identifier), {
			identifier: payload.identifier,
			"private": payload.private
		});

		await axios(options)
			.then((json) => {
				if (!json.data.error) {

					this.commit('setIsRegisteringSite', true);

					this.commit('setFlashMessage', {
						message: json.data.message,
						type: json.data.error ? 'error' : 'success'
					});
				} else {

					this.commit('setFlashMessage', {
						message: json.data.message,
						type: 'error'
					});

					this.commit('setIsRegisteringSite', false);
				}

				// return !json.data.error;
			});
	},

	setSites: async function (state, payload) {

		let options;

		if (payload && payload.user_identifier) {

			options = request_options('GET', routes.get_user_sites(payload.user_identifier));

			await axios(options)
				.then((json) => {
					this.commit('setSites', json.data);
				});

		} else {

			options = request_options('GET', routes.get_sites(payload.limit, payload.offset, payload.query));

			await axios(options)
				.then((json) => {
					this.commit('setSites', json.data);
				});
		}
	},

	loadMoreSites: async function (state, payload) {

		let options = request_options('GET', routes.get_sites(payload.limit, payload.offset));

		await axios(options)
			.then((json) => {
				this.commit('mergeSites', {sites: json.data});

				this.commit('setFilteredSites');

				if (!json.data.length) {		// hide "load more"
					this.commit('setPagination', {has_more: false});
				}
			});
	},

	setNotifications: async function (store) {

		let options = request_options('GET', routes.get_notifications(store.state.auth_user.identifier));

		if (!store.state.notifications) {
			await axios(options)
				.then((json) => {
					this.commit('setNotifications', json.data);
				});
		}
	},

	deleteNotification: async function (store, payload) {

		let options = request_options('GET', routes.delete_notification(payload.id));

		if (store.state.notifications) {
			await axios(options)
				.then((json) => {

					this.commit('deleteNotification', {id: payload.id});
				});
		}
	},

	setTechnologies: async function (store) {

		let options = request_options('GET', routes.get_parameters('technology', 'name'));

		if (!store.state.profile_settings.technologies) {
			await axios(options)
				.then((json) => {
					this.commit('setTechnologies', json.data);
				});
		}
	},

	setNotificationCount: async function (store, payload) {

		let options = request_options('GET', routes.get_notification_count(payload));

		if (!store.state.notification_count) {

			await axios(options)
				.then((json) => {
					this.commit('setNotificationCount', json.data);
				});
		}
	},

	setSiteCount: async function (store, payload) {

		let options = request_options('GET', routes.get_site_count(payload));

		if (!store.state.notification_count) {

			await axios(options)
				.then((json) => {
					this.commit('setSiteCount', json.data);
				});
		}
	},

	setUser: async function (store, payload) {

		let options = request_options('GET', routes.get_user(payload.id));

		if (!store.state.auth_user) {

			await axios(options)
				.then((json) => {
					this.commit('setUser', json.data);
				});
		}
	},

	setMarketingTools: async function (store) {

		let options = request_options('GET', routes.get_parameters('marketing-tool'));

		if (!store.state.marketing_tools.length) {

			await axios(options)
				.then((json) => {
					this.commit('setMarketingTools', json.data);
				});
		}
	},

	setUserStatus: async function (store) {

		let options = request_options('GET', routes.get_parameters('user-status', 'name'));

		if (!store.state.profile_settings.status) {
			await axios(options)
				.then((json) => {
					this.commit('setUserStatus', json.data);
				});
		}
	},

	updateProfileSettings: async function ({state, commit}, payload) {
		let options = request_options('POST', routes.update_auth_user(state.auth_user.identifier), payload.form_data, true)

		await axios(options)
			.then(function (json) {
				if (json.data.error) {
					commit('setFlashMessage', {
						type: 'error',
						message: json.data.message
					});
				} else {
					commit('setFlashMessage', {
						type: 'success',
						message: json.data.message
					});

					payload.file = null;

					if (payload.type === 'account') {
						$('#old-password, #password, #password_confirmation').val('')
					}
				}
			})
			.catch(function (params) {
				let messages = params.response.data.errors;

				if (messages) {
					commit('setFlashMessage', {
						type: 'error',
						message: messages[Object.keys(messages)[0]][0]
					})
				}
			});
	},

	storeNewsletter: async function (store, payload) {

		let that = this,
			options = request_options('POST', routes.store_newsletter(), payload);

		await axios(options)
			.then((json) => {

				that.commit('setFlashMessage', {
					type: json.data.error ? 'error' : 'success',
					message: json.data.message
				});
			});
	},


	// toggleFollow: async function (store, payload) {
	//
	// 	let options = request_options('GET', routes.toggle_follow(payload));
	//
	// 	await axios(options)
	// 		.then((json) => {
	// 		});
	// },

	// setFollowers: async function (store) {
	//
	// 	let options = request_options('GET', routes.get_followers(store.state.auth_user.identifier));
	//
	// 	if (!store.state.followers) {
	// 		await axios(options)
	// 			.then((json) => {
	// 				this.commit('setFollowers', json.data);
	// 			});
	// 	}
	// },
	//
	// setFollowings: async function (store) {
	//
	// 	let options = request_options('GET', routes.get_followings(store.state.auth_user.identifier));
	//
	// 	if (!store.state.followings) {
	// 		await axios(options)
	// 			.then((json) => {
	// 				this.commit('setFollowings', json.data);
	// 			});
	// 	}
	// },
}
