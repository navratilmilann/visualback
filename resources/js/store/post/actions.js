import {request_options} from "../../includes/helpers";
import routes from "../../includes/routes";
import actions from './../global/actions';
import axios from "axios";

let post_actions = {
	setPosts: async function ({commit}, payload) {
		let options = request_options('GET', routes.get_posts(payload));

		await axios(options)
			.then((json) => {
				commit('setPosts', json.data);
			});
	},
	setPost: async function ({commit}, payload) {
		let options = request_options('GET', routes.get_post(payload.slug, payload.id));

		await axios(options)
			.then((json) => {
				commit('setPost', json.data);
			});
	},
	setRelatedPosts: async function ({commit}, payload) {
		let options = request_options('GET', routes.get_related_posts(payload.slug, payload.id, payload.count));

		await axios(options)
			.then((json) => {
				commit('setRelatedPosts', json.data);
			});
	},
	setPopularTags: async function ({commit}) {
		let options = request_options('GET', routes.get_popular_tags());

		await axios(options)
			.then((json) => {
				commit('setPopularTags', json.data);
			});
	}
}

export default {
	...actions, ...post_actions
}
