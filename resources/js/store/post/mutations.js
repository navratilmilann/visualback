import mutations from './../global/mutations';

let post_mutations = {
	setPosts: function (state, payload) {
		state.posts = payload
	},
	setPost: function (state, payload) {
		state.post = payload
	},
	setRelatedPosts: function (state, payload) {
		state.related_posts = payload
	},
	setPopularTags: function (state, payload) {
		state.popular_tags = payload
	},
}

export default {
	...mutations, ...post_mutations
}
