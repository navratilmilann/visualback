import getters from './../global/getters';

let post_getters = {
	getPosts: state => state.posts,
	getRelatedPosts: state => state.related_posts,
	getPost: state => state.post,
	getPopularTags: state => state.popular_tags
}

export default {
	...getters, ...post_getters
}
