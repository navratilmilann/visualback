import state from './../global/state';

let post_state = {
	posts: [],
	related_posts: [],
	post: null,
	popular_tags: [],
}

export default {
	...state, ...post_state
}
