import JQuery from 'jquery'
import store from './store/other/store';
import Gate from './policies/Gate';
import VTooltip from "v-tooltip";

window.Vue = require('vue');
window.$ = JQuery;

require('./includes/helpers');
require('./includes/filters');

document.domain = String(document.head.querySelector('meta[name="base-url"]').content).replace(/(^\w+:|^)\/\//, '');

window.Vue.prototype.$gate = new Gate();
window._token = document.head.querySelector('meta[name="csrf-token"]').content;

Vue.use(VTooltip)

Vue.component('flash-message', require('./components/partials/Layout/FlashMessage.vue').default);
Vue.component('auth-reset-password', require('./components/auth/ResetPassword.vue').default);
Vue.component('auth-forgot-password', require('./components/auth/ForgotPassword.vue').default);
Vue.component('auth-register', require('./components/auth/Register.vue').default);
Vue.component('auth-login', require('./components/auth/Login.vue').default);
Vue.component('auth-verification-account', require('./components/auth/VerificationAccount').default);

const app = new Vue({
	el: '#app',
	store,
});
