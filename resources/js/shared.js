import JQuery from 'jquery'
import store from './store/other/store';
import Gate from './policies/Gate';
import VTooltip from "v-tooltip";

window.Vue = require('vue');
window.$ = JQuery;

require('./includes/helpers');
require('./includes/filters');

document.domain = String(document.head.querySelector('meta[name="base-url"]').content).replace(/(^\w+:|^)\/\//, '');

window.Vue.prototype.$gate = new Gate();

window._token = document.head.querySelector('meta[name="csrf-token"]').content;

Vue.component('cookies', require('./components/Cookies.vue').default);
Vue.component('faq', require('./components/Faq.vue').default);
Vue.component('support-us', require('./components/SupportUs.vue').default);
Vue.component('about-us', require('./components/AboutUs.vue').default);
Vue.component('terms-conditions', require('./components/TermsConditions.vue').default);
Vue.component('privacy-policy', require('./components/PrivacyPolicy.vue').default);
Vue.component('index', require('./components/Index.vue').default);
Vue.component('contact-index', require('./components/contact/Index.vue').default);
Vue.component('site-edit', require('./components/site/Edit.vue').default);
Vue.component('site-index', require('./components/site/Index.vue').default);
Vue.component('profile-detail', require('./components/profile/Detail.vue').default);
Vue.component('profile-settings', require('./components/profile/ProfileSettings.vue').default);
Vue.component('errors-error', require('./components/errors/Error.vue').default);
Vue.component('errors-mobile-livepreview', require('./components/errors/MobileLivepreview.vue').default);
Vue.component('flash-message', require('./components/partials/Layout/FlashMessage.vue').default);

Vue.use(VTooltip);

const shared = new Vue({
	el: '#app',
	store,
});
