/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import JQuery from 'jquery'
import VueI18n from 'vue-i18n'
import store from './store/store';
import {translations as sk_translations} from "./lang/sk/all";
import {translations as en_translations} from "./lang/en/all";

window.Vue = require('vue');
window.$ = JQuery;

require('./bootstrap');
require('./helpers');
require('./filters');

document.domain = 'devchicks.test';
// document.domain = 'devchicks.com';

const i18n = new VueI18n({
	locale: 'en-US',
	messages: {
		'en-US': en_translations,
		'sk-SK': sk_translations,
	}
});

Vue.component('auth-verification-account', require('./components/auth/VerificationAccount').default);
Vue.component('user-site-edit', require('./components/shared/site/Edit.vue').default);
Vue.component('user-site-index', require('./components/shared/site/Index.vue').default);
Vue.component('user-profile-detail', require('./components/shared/profile/Detail.vue').default);
Vue.component('user-profile-settings', require('./components/shared/profile/ProfileSettings.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	el: '#app',
	store,
	i18n
});
