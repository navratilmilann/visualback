import {mapGetters, mapMutations} from "vuex";
import constants from "./constants";
import {get_attachment_by_type, is_empty_string, request_options} from "./helpers";
import routes from "./routes";
import {removeTemporaryPin} from "../services/PinService";

// needs to set data e.g.  old_attributes: ['email']
let modalMixin = {
	data() {
		return {
			show_modal: false,
		}
	},
	watch: {
		modal: function (new_value) {
			this.show_modal = new_value.show_modal;
		}
	},
	methods: {
		closeOnEscape: function (event) {
			if (this.show_modal && event.keyCode === 27) {
				this.show_modal = false;
			}
		},
		closeOnCancel: function () {
			this.show_modal = false;
		},
	},
	created() {
		$(document).on('keydown', this.closeOnEscape)
	}
}

let oldInputMixin = {
	props: ['oldInputs'],
	data() {
		return {
			old: null
		}
	},
	created() {

		let that = this;

		if (that.oldInputs !== '[]') {								// set object by old inputs
			that.old = JSON.parse(this.oldInputs)
		} else {													// create object by empty inputs
			that.old = {};
			that.old_attributes.forEach(function (value) {
				that.old[value] = '';
			});
		}
	}
}

// append show password button to input
let showPasswordMixin = {
	methods: {
		initiateShowPassword: function () {
			this.$nextTick(function () {

				let input = $('#password');

				input.after("<button class='show-password' type='button'>show</button>")

				$(".show-password").click(function () {

					$(this).toggleClass("active");

					if (input.attr("type") === "password") {
						input.attr("type", "text");
					} else {
						input.attr("type", "password");
					}
				});
			})
		}
	}
}

// returns chunks of items
let paginationParentMixin = {
	computed: {
		paginated_items: function () {
			if (this.filtered_items.length) {
				return this.filtered_items.slice(this.pagination.start, this.pagination.end);
			}
			return []
		}
	}
}

let sidebarMixin = {
	methods: {
		...mapMutations(['setCssEditor', 'setLivepreviewSettings', 'setGallery']),
		showCommenting: function () {
			if (this.settings.sidebar_type !== 'commenting') {
				this.clearCommentSidebar();
				this.setLivepreviewSettings({
					open_sidebar: true,
					sidebar_type: 'commenting',
				});
			} else {
				removeTemporaryPin();
				this.stopCSS();                                                                                           // remove all comment sidebar field and remove temporary pin
			}
		},
		showCommentDetail: function (comment) {
			let css_file = get_attachment_by_type(comment.attachment, 'css');

			this.setLivepreviewSettings({
				sidebar_type: 'detail',
				current_comment: comment,
				current_replies: comment.replies,
				comment_edit_mode: false,
				reply_edit_mode: false
			});

			this.setGallery(get_attachment_by_type(comment.attachment, 'attachment'));

			this.setCssEditor({
				file: css_file.length ? css_file[0] : null
			});

			$('iframe').contents().find(`.pin[data-id=${comment.id}]`).addClass('highlight-comment')
		}
	}
}

let cssEditorMixin = {
	computed: {
		...mapGetters({css_editor: 'getCssEditor', settings: 'getLivepreviewSettings'})
	},
	methods: {
		...mapMutations(['setCssEditor', 'setLivepreviewSettings']),
		runCSS: function () {

			let head = $('#live-preview-iframe').contents().find('head'),
				style_element = head.find(`#${this.css_editor.element_id}`),
				css = `<style type="text/css" id="${this.css_editor.element_id}">${this.css_editor.code}</style>`;

			if (style_element.length) {
				style_element.remove();
			}

			if (this.settings.running_code) {
				head.append(css);
			}
		},
		deleteCssFile: function (event) {

			event.preventDefault();
			event.stopImmediatePropagation();

			let style_element = this.head ? this.head.find(`#${this.css_editor.element_id}`) : null,
				that = this,
				options;

			this.addRecaptchaToken(event, async () => {

				options = request_options('POST', routes.delete_attachment(this.site.id, this.css_editor.file.id), {
					identifier: this.site.id,
					attachment_id: this.css_editor.file.id,
					"g-recaptcha": this.recaptcha_token
				});

				await this.deleteAttachment({
					options,
					that
				});

				if (style_element) {
					style_element.remove();
				}

				this.code_changed = true;
			});
		},
	}
}

let commentMixin = {
	computed: {
		comment_author: function () {
			return this.comment.user
				? this.comment.user.nickname
				: (this.comment.nickname ? this.comment.nickname : 'Anonymous')
		},
		comment_author_image_url: function () {
			return this.comment.profile_photo_url
		}
	}
}

let livepreviewMixin = {
	data() {
		return {
			is_checking_pins: false,
			head: null
		}
	},
	computed: {
		...mapGetters({settings: 'getLivepreviewSettings', auth_user: 'getAuthUser'}),
	},
	methods: {
		...mapMutations(['setLivepreviewSettings', 'setFlashMessage', 'showException', 'flushFlashMessage']),
		isEmptyString: is_empty_string,
		stopCSS: function () {
			let style_element = $('#live-preview-iframe').contents().find('head').find(`#visualback_stylling`);

			if (style_element.length) {

				style_element.remove();

				this.setLivepreviewSettings({
					running_code: false
				});

				this.flushFlashMessage();
			}
		},
	},
}

// http://www.blog.tonyswierz.com/javascript/add-and-use-google-recaptcha-in-a-vuejs-laravel-project/
let recaptchaMixin = {
	data() {
		return {
			recaptcha_token: null
		}
	},
	methods: {
		syncSubmitFormWithRecaptcha: function (event) {

			event.preventDefault();

			this.addRecaptchaToken(event, function () {
				$(event.target).submit();
			});
		},
		addRecaptchaToken: function (event, callback = null) {

			let id = $('.page').attr('id'),
				action = id ? id.replace('-', '_') : 'livepreview',
				key = $('meta[name="recaptcha-key"]').attr('content'),
				recaptcha_input = $('input[name="g-recaptcha"]'),
				that = this;

			grecaptcha.ready(function () {
				grecaptcha.execute(key, {action})
					.then(function (token) {

						if (!recaptcha_input.length) {
							// append hidden recaptcha input
							$('<input>').attr({
								type: 'hidden',
								name: 'g-recaptcha',
								value: token,
							}).appendTo('form');

						} else {
							recaptcha_input.val(token)
						}

						that.recaptcha_token = token;

					})
					.then(function () {
						if (callback) {
							callback();
						}
					});
			});
		},
	}
}

export {
	paginationParentMixin,
	recaptchaMixin,
	livepreviewMixin,
	showPasswordMixin,
	oldInputMixin,
	cssEditorMixin,
	sidebarMixin,
	commentMixin,
	modalMixin
};

