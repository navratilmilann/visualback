function copy_on_clipboard(selector, callback) {

	let range,
		selection,
		element = document.querySelector(selector);

	if (document.body.createTextRange) {

		range = document.body.createTextRange();
		range.moveToElementText(element);
		range.select();

	} else if (window.getSelection) {

		selection = window.getSelection();
		range = document.createRange();
		range.selectNodeContents(element);
		selection.removeAllRanges();
		selection.addRange(range);
	}

	try {

		document.execCommand('copy');

		if (callback) {
			callback();
		}

	} catch (err) {
		alert('Please, copy HTML tag manually.');
	}
}

function hide_loading_animation() {
	setTimeout(function () {
		replace_img_by_svg();
		$('body.loading > .pin-loader svg .shp1').animate({
			opacity: 0.05
		}, 350, function () {
			$('body.loading > .pin-loader').remove();
			$('body.loading').removeClass('loading');
		});
	}, 1000);
}

function replace_img_by_svg() {

	let svg_urls = new Set();
	// IMG REPLACE TO SVG ELEMENT
	$('img.svg').each(function () {
		svg_urls.add($(this).attr('src'));
	});

	svg_urls.forEach(function (url) {

		$.get(url, function (data) {

			let $img = $(`img[src$="${url}"]`),
				imgID = $img.attr('id'),
				imgClass = $img.attr('class');

			// Get the SVG tag, ignore the rest
			let $svg = $(data).find('svg');

			// Add replaced image's ID to the new SVG
			if (typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if (typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass + ' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');
	});
}

function url(path) {
	return document.head.querySelector('meta[name="base-url"]').content + path;
}

function scroll_to_element_by_id(selector) {
	document.getElementById(selector).scrollIntoView({behavior: "smooth"});
}

function get_parameter_value_by_type(parameters, type, json_value = false, default_value = null) {

	let entity;

	if (!default_value) {
		if (json_value) {
			default_value = {};
		} else {
			default_value = null;
		}
	}

	if (parameters.length) {

		entity = parameters.filter(function (parameter) {
			return parameter.type === type;
		});

		if (entity.length) {

			if (json_value) {
				return JSON.parse(entity[0]['pivot']['value']);
			} else {
				return entity[0]['pivot']['value'];
			}
		}
	}

	return default_value;

}

function get_parameters_by_type(parameters, type) {

	let entity;

	if (parameters.length) {

		entity = parameters.filter(function (parameter) {
			return parameter.type === type;
		});

		if (entity.length) {

			entity = entity.map(function (value) {
				return {
					type: value['type'],
					name: value['name'],
					slug: value['slug'],
					value: value['pivot']['value']
				}
			});

			return entity;

		}
	}
	return [];
}

function get_attachment_by_type(attachments, type) {

	let entity = null;

	if (attachments.length) {

		entity = attachments.filter(function (attachment) {
			return attachment.type === type;
		});

		if (entity.length) return entity;
	}

	return [];
}

function string_limit(str, limit) {

	if (str.length > limit) str = str.substring(0, limit) + '...';
	return str;
}

function get_path(elem) {
	return get_selector(elem)
		.replace('.on-hover', '')
		.replace('.highlight-element', '')
}

function get_selector(elem) {
	const {
		tagName,
		id,
		className,
		parentNode
	} = elem;

	if (tagName === 'BODY') return 'body';

	let str = tagName.toLowerCase();

	str += (id !== '') ? `#${id}` : '';

	if (className) {
		const classes = className.split(/\s/);

		str += `.${classes[0]}`;
		// for (let i = 0; i < classes.length; i++) {
		// 	str += `.${classes[i]}`;
		// }
	}

	let childIndex = 1;

	for (let e = elem; e.previousElementSibling; e = e.previousElementSibling) {
		childIndex += 1;
	}

	str += `:nth-child(${childIndex})`;

	return `${get_path(parentNode)} > ${str}`;
}

function group_by(xs, key) {
	return xs.reduce(function (rv, x) {
		(rv[x[key]] = rv[x[key]] || []).push(x);
		return rv;
	}, {});
}

function get_browser() {
	let ua = navigator.userAgent, tem,
		M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	if (/trident/i.test(M[1])) {
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE ' + (tem[1] || '');
	}
	if (M[1] === 'Chrome') {
		tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
		if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	}
	M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
	if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
	return M.join(' ');
}

function request_options(method, url, data = null, file = false) {

	let headers;

	if (file) {
		headers = {
			'Access-Control-Allow-Origin': '*',
			'Content-Type': 'multipart/form-data',
			'X-CSRF-TOKEN': window._token,
		}
	} else {
		headers = {
			'X-Requested-With': 'XMLHttpRequest',
			'X-CSRF-TOKEN': window._token,
		}
	}

	return {
		method,
		headers,
		data,
		url,
	};
}

function filter_by_search(items, attribute, search) {
	if (search !== '')
		return items.filter(item => {
			return item[attribute].toLowerCase().includes(search.toLowerCase())
		});

	return items;
}

function get_values_by_attribute(array, attribute) {

	let data = [];

	$.map(array, function (n, i) {
		data.push(n[attribute]);
	});

	return data;
}

function debounce(func, wait = 100) {

	let timeout;

	return function (...args) {
		clearTimeout(timeout);

		timeout = setTimeout(() => {
			func.apply(this, args);
		}, wait);
	};
}

function form_data_to_object(form) {

	let unindexed_array = form.serializeArray();

	let form_data = new FormData();

	$.map(unindexed_array, function (n, i) {
		form_data.append(n['name'], n['value']);
	});

	return form_data;
}

function add_https(url) {
	if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
		url = "https://" + url;
	}
	return url;
}

function is_empty_string(str) {
	return str === null || str.match(/^ *$/) !== null;
}

function remove_item_from_array(array, index) {
	if (index > -1) {
		array.splice(index, 1);
	}

	return array;
}

function strip_html(string) {
	return string.replace(/<\/?[^>]+(>|$)/g, "");
}

function is_demo(state) {
	return state.livepreview_settings && state.livepreview_settings.is_demo;
}

export {
	is_demo,
	strip_html,
	remove_item_from_array,
	is_empty_string,
	add_https,
	get_browser,
	scroll_to_element_by_id,
	get_parameter_value_by_type,
	get_parameters_by_type,
	url,
	string_limit,
	get_path,
	group_by,
	request_options,
	filter_by_search,
	get_attachment_by_type,
	debounce,
	form_data_to_object,
	replace_img_by_svg,
	copy_on_clipboard,
	get_values_by_attribute,
	hide_loading_animation
}
