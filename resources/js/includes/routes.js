import {url} from "./helpers";

export default {
	/*
		API
	 */
	send_verification_email: function (identifier) {
		return url(`/api/send-verification/${identifier}`);
	},
	toggle_like: function (id, type = 'project') {
		return url(`/api/${type}/${id}/like`);
	},
	toggle_follow: function (id) {
		return url(`/api/${id}/follow`);
	},
	get_parameters: function (type, pluck = '') {
		return url(`/api/parameter/${type}/${pluck}`);
	},
	get_user: function (id = '') {
		return url(`/api/user/${id}`);
	},
	update_auth_user: function (user_identifier) {
		return url(`/api/user/${user_identifier}/update`);
	},
	get_sites: function (limit = '', offset = '', query = '') {
		if (offset !== '')
			return url(`/api/project/${limit}/${offset}${query}`);

		return url(`/api/project/${limit}${query}`);
	},
	get_user_sites: function (user_identifier) {
		return url(`/api/user/${user_identifier}/project`);
	},
	store_site: function () {
		return url('/api/project/store');
	},
	report_model: function (id, type) {
		return url(`/api/${type}/${id}/report`);
	},
	set_collaborators: function () {
		return url('/api/project/store/collaborators');
	},
	delete_site: function (site_id, slug) {
		return url(`/api/project/${site_id}/${slug}/delete`);
	},
	update_site_comment: function (comment_id) {
		return url(`/api/project/comment/${comment_id}/update`);
	},
	get_site_comments: function (site_id) {
		return url(`/api/project/${site_id}/comment`);
	},
	store_site_comment: function () {
		return url('/api/project/comment/store');
	},
	delete_site_comment: function (comment_id) {
		return url(`/api/project/comment/${comment_id}/delete`);
	},
	get_attachment: function (site_id, attachment_id) {
		return url(`/api/project/${site_id}/attachment/${attachment_id}`);
	},
	store_attachment: function (site_id) {
		return url(`/api/project/${site_id}/attachment/store`);
	},
	delete_attachment: function (site_id, attachment_id) {
		return url(`/api/project/${site_id}/attachment/${attachment_id}/delete`);
	},
	store_comment_rating: function () {
		return url('/api/project/comment/rating/store');
	},
	store_site_rating: function () {
		return url('/api/project/rating/store');
	},
	store_screenshot: function (site_identifier) {
		return url(`/api/project/${site_identifier}/screenshot/store`);
	},
	get_profile_sites: function () {
		return url(`/api/profile/settings/site`);
	},
	get_technologies: function () {
		return url(`/api/parameter/technology`);
	},
	get_city: function (city) {
		return url(`/api/parameter/cities?city=${city}`);
	},
	get_country: function (country) {
		return url('/api/parameter/countries?name=' + country);
	},
	delete_account: function (user_identifier) {
		return url(`/profile/${user_identifier}/delete`);
	},
	get_notifications: function (user_identifier) {
		return url(`/api/user/${user_identifier}/notifications`);
	},
	get_followers: function (user_identifier) {
		return url(`/api/user/${user_identifier}/followers`);
	},
	get_followings: function (user_identifier) {
		return url(`/api/user/${user_identifier}/followings`);
	},
	get_notification_count: function (user_identifier) {
		return url(`/api/user/${user_identifier}/notifications/count`);
	},
	get_site_count: function (user_identifier) {
		return url(`/api/user/${user_identifier}/site/count`);
	},
	update_notification: function (user_identifier) {
		return url(`/api/user/${user_identifier}/notifications/update`);
	},
	delete_notification: function (id) {
		return url(`/api/user/${id}/notifications/delete`);
	},
	store_newsletter: function () {
		return url('/api/newsletter');
	},
	authorize_site: function (identifier) {
		return url(`/api/project/${identifier}/authorize`);
	},
	get_posts: function (parameters) {
		if (parameters && parameters.tag) {
			return url(`/api/post/tag/${parameters.tag}`);
		}
		return url('/api/post');
	},
	get_related_posts: function (slug, id, count = '') {
		return url(`/api/post/${slug}/${id}/related/${count}`);
	},
	get_post: function (slug, id) {
		return url(`/api/post/${slug}/${id}`);
	},
	get_popular_tags: function () {
		return url(`/api/post/tag/popular`);
	},


	posts_by_tag: function (tag) {
		return url(`/post/tag/${tag}`);
	},
	auth_by_social_network: function (network) {
		return url(`/auth/${network}`);
	},
	login: function () {
		return url(`/login`);
	},
	logout: function () {
		return url(`/logout`);
	},
	forgot_password: function () {
		return url(`/forgot-password`);
	},
	terms_and_conditions: function () {
		return url(`/terms-and-conditions`);
	},
	about_us: function () {
		return url(`/about-us`);
	},
	faq: function () {
		return url(`/faq`);
	},
	posts: function () {
		return url(`/post`);
	},
	post_detail: function (slug, id) {
		return url(`/post/${slug}/${id}`);
	},
	support_us: function () {
		return url(`/support-us`);
	},
	reset_password: function () {
		return url(`/reset-password`);
	},
	register: function () {
		return url(`/register`);
	},
	store_contact: function () {
		return url(`/contact/store`);
	},
	sites: function () {
		return url(`/project`);
	},
	home: function () {
		return url(`/`);
	},
	contact: function () {
		return url(`/contact`);
	},
	settings: function (user_identifier) {
		return url(`/profile/${user_identifier}/settings`);
	},
	notifications: function (user_identifier) {
		return url(`/profile/${user_identifier}/settings/notifications`);
	},
	registered_projects: function (user_identifier) {
		return url(`/profile/${user_identifier}/settings/my-projects`);
	},
	profile: function (id) {
		return url(`/profile/${id}`);
	},
	jobs: function () {
		return url(`/jobs`);
	},
	newsletter: function () {
		return url(`/newsletter`);
	},
	livepreview: function (identifier, slug) {
		return url(`/project/${identifier}/${slug}/live-preview`);
	},
	site_edit: function (identifier) {
		return url(`/project/${identifier}/edit`);
	}
}
