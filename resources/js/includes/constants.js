export default {
	same_color_logo: '/img/logo/logo_sticky.svg',
	base_logo: '/img/logo/logo.svg',
	email: 'visualbackapp@gmail.com',
	facebook: 'https://www.facebook.com/VisualBackApp/',
	buy_me_a_coffee: 'https://www.buymeacoffee.com/VisualBackApp',
	discord: 'https://discord.gg/CQgGss4Bkx',
	// default_profile_url: 'https://eu.ui-avatars.com/api/?name=A&background=500ade&color=fff',
	demo_identifier: 'demo',
	vb_user_indetifier: '6akm0-eua1'
	// instagram: 'devchicks@gmail.com',
}
