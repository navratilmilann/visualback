export default class SitePolicy {
	// static create(user)
	// {
	// 	return user.role === 'editor';
	// }

	// static view(user, site) {
	// 	return true;
	// }

	static delete(user, site) {
		return user && site.is_auth_user_collaborator;
	}

	static update(user, site) {
		return user && site.is_auth_user_collaborator;
	}

	static can_like(user, site) {
		return user && (!site.is_auth_user_collaborator);
	}
}