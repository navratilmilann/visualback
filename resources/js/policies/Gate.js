import CommentPolicy from './CommentPolicy';
import SitePolicy from "./SitePolicy";
import PostPolicy from "./PostPolicy";

export default class Gate {
	constructor() {

		this.policies = {
			comment: CommentPolicy,
			site: SitePolicy,
			post: PostPolicy
		};
	}

	allow(action, type, user = null, model = null) {
		return this.policies[type][action](user, model);
	}

	deny(action, type, model = null) {
		return !this.allow(action, type, model);
	}
}
