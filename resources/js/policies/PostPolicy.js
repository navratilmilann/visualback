export default class PostPolicy {
	static delete(user, post) {
		return user.id === post.user_id;
	}

	static update(user, post) {
		return user && user.id === post.user_id;
	}

	static can_like(user, post) {
		return user && (post.user_id !== user.id);
	}
}
