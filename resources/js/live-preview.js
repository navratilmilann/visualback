import VueSocialSharing from 'vue-social-sharing'
import VTooltip from 'v-tooltip'
import JQuery from 'jquery';
import store from './store/other/store';
import Gate from './policies/Gate';

window.Vue = require('vue');
window.$ = JQuery;

require('./includes/helpers');
require('./includes/filters');

window.Vue.prototype.$gate = new Gate();

window._token = document.head.querySelector('meta[name="csrf-token"]').content;

document.domain = String(document.head.querySelector('meta[name="base-url"]').content).replace(/(^\w+:|^)\/\//, '');

Vue.use(VueSocialSharing);
Vue.use(VTooltip);

Vue.component('live-preview', require('./components/site/LivePreview.vue').default);
Vue.component('errors-error', require('./components/errors/Error.vue').default);


const app = new Vue({
	store,
	el: '#app'
});
