<?php

return [
    'message'    => 'We use cookies to improve your experience and analyze traffic. ',
    'learn_more' => 'Learn more',
    'agree'      => 'Allow cookies',
];
