<?php

return [
    'Delete Account' => 'Vymazať účet',
    'Permanently delete your account.' => 'Natrvalo odstráňte svoj účet.',
    'Once your account is deleted' => 'Po odstránení vášho účtu budú natrvalo odstránené všetky jeho zdroje a údaje. Pred odstránením účtu si prosím stiahnite všetky údaje alebo informácie, ktoré si chcete ponechať.',
    'Are you sure you want to delete your account?' => 'Naozaj chcete odstrániť svoj účet? Po odstránení Vášho účtu budú natrvalo odstránené všetky jeho zdroje a údaje. Zadajte svoje heslo a potvrďte, že chcete natrvalo odstrániť svoj účet.',
    'Password' => 'Heslo',
    'Nevermind' => 'Nevadí!',
    'Browser Session' => 'Prehľadávač',
    'Manage and logout' => 'Spravujte a odhlasujte svoje aktívne relácie v iných prehliadačoch a zariadeniach.',
];
