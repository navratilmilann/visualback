<?php
return [
    'email_exists'          => 'Email už existuje.',
    'sent_contact_email'    => 'Kontaktný email bol odoslaný.',
    'verified_account'      => 'Účet bol úspešne overený.',
    'deleted_attachment'    => 'Príloha bola úspešne odstránená',
    'deleted_site'          => 'Projekt bol úspešne odstránený.',
    'stored_comment_rating' => 'Hodnotenie komentárov bolo úspešne uložené.',
    'stored_site'           => 'Projekt bol úspešne zaregistrovaný',
    'stored_site_comment'   => 'Komentár bol úspešne uložený.',
    'stored_site_rating'    => 'Hodnotenie stránok bolo úspešne uložené.',
    'account_is_verified'   => 'Účet už je verifikovaný'
];
