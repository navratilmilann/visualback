<?php

return [
    'reset' => 'Vaše heslo bolo obnovené!',
    'sent' => 'Odkaz na obnovenie hesla sme Vám poslali e-mailom!',
    'throttled' => 'Prosím, pred ďalším pokusom počkajte.',
    'token' => 'Token na obnovenie hesla je neplatný',
    'user' => 'Nemôžeme nájsť používateľa s touto e-mailovou adresou.',
];
