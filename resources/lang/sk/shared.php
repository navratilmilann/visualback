<?php

return [
    'Get' => 'Získajte',
    'feedback' => 'spätnú väzbu',
    'from another developers and' => 'od skúsených vývojárov a',
    'boost' => 'zlepšite',
    'your UX & UI design' => 'Váš UX & UI dizajn',
    'We Do Help Begginers Design' => 'Pomáhame začínajúcim vývojárom',
    "We're going to help you with your design" => "Pomôžeme Vám s Vaším dizajnom",
    'Explore projects' => 'Zobraziť projekty',
    "Let's go on tour" => "Začíname!",
    'Project in Live-Preview' => 'Projekty v náhľade',
    'Cut through the noise with immediate, visual feedback.' => 'Cut through the noise with immediate, visual feedback. Go from design to build to publish without
					losing your mind in the process. We’ll save you time. You’ll save the day.',
    'Feedbacks' => 'Spätná väzba',

    // Blog
    'We couldnt find any articles' => "Nenašli sme žiadne články :(.",
    'Read more' => 'Prečítať viac',
    'Published' => 'Publikované',
];
