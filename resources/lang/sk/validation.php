<?php

return [
    'accepted' => ':attribute musí byť vyplnený.',
    'active_url' => ':attribute nie je platná URL adresa.',
    'after' => ':attribute musí byť dátum po :date.',
    'after_or_equal' => ':attribute musí byť dátum po alebo rovnako ako :date.',
    'alpha' => 'The :attribute môže obsahovať iba písmená.',
    'alpha_dash' => ':attribute môže obsahovať iba písmená, číslice, pomlčky a podčiarkovníky.',
    'alpha_num' => ':attribute môže obsahovať iba písmená a čísla.',
    'array' => ':attribute musí byť pole.',
    'before' => ':attribute dátum musí byť pred dátumom :date.',
    'before_or_equal' => ':attribute musí byť dátum pred alebo rovný :date.',
    'between' => [
        'numeric' => ':attribute musí mať medzi :min a :max.',
        'file' => ':attribute musí mať medzi :min a :max kilobytov.',
        'string' => ':attribute musí mať medzi :min a :max znakov.',
        'array' => ':attribute musí mať medzi :min a :max položiek.',
    ],
    'boolean' => ':attribute musí byť pravda alebo nepravda.',
    'confirmed' => 'Potvrdenie :attribute sa nezhoduje.',
    'date' => ':attribute nie je platný dátum.',
    'date_equals' => ':attribute musí byť dátum rovný :date.',
    'date_format' => ':attribute nezodpovedá formátu :format.',
    'different' => ':attribute a :other musí byť odlišné.',
    'digits' => ':attribute musí obsahovať :digits číslice.',
    'digits_between' => ':attribute musí byť medzi :min a :max císlami.',
    'dimensions' => ':attribute má nepletné rozmery obrázka.',
    'distinct' => ':attribute pole má duplicitnú hodnotu.',
    'email' => ':attribute musí byť platná e-mailová adresa.',
    'ends_with' => ':attribute musí končiť s nasledujúcimi hodnotami: :values.',
    'exists' => 'Vybratý :attribute je neplatný.',
    'file' => ':attribute musí byť súbor.',
    'filled' => ':attribute musí mať hodnotu.',
    'gt' => [
        'numeric' => ':attribute musí byť väčší než :value.',
        'file' => ':attribute musí byť viac než :value kilobytov.',
        'string' => ':attribute musí byť väčší než :value znakov.',
        'array' => ':attribute musí mať viac než :value položiek.',
    ],
    'gte' => [
        'numeric' => ':attribute musí byť väčší alebo rovný :value.',
        'file' => ':attribute musí byť väčší alebo rovný :value kilobytov.',
        'string' => ':attribute musí byť väčší alebo rovný :value znakov.',
        'array' => ':attribute musí mať:value položiek alebo viac.',
    ],
    'image' => ':attribute musí byť obrázok.',
    'in' => 'Vybraný atribút :attribute je neplatný.',
    'in_array' => ':attribute pole neexistuje v :other.',
    'integer' => ':attribute musí byť celé číslo.',
    'ip' => ':attribute musí byť validná IP adresa.',
    'ipv4' => ':attribute musí byť validná IPv4 adresa.',
    'json' => ':attribute musí byť platný reťazec JSON',
    'lt' => [
        'numeric' => ':attribute musí mať menej než :value.',
        'file' => ':attribute musí mať menej než :value kilobytov.',
        'string' => ':attribute musí mať menej než :value znakov.',
        'array' => ':attribute musí mať menej než :value položiek.',
    ],
    'lte' => [
        'numeric' => ':attribute musí byť mänší alebo rovný :value.',
        'file' => ':attribute musí byť viac než :value kilobytov.',
        'string' => ':attribute musí byť väčší než :value znakov.',
        'array' => ':attribute musí mať viac než :value položiek.',
    ],
    'max' => [
        'numeric' => ':attribute musí byť medzi :min a :max.',
        'file' => ':attribute musí mať medzi :min a :max kilobytov.',
        'string' => ':attribute musí mať medzi :min a :max znakov.',
        'array' => ':attribute musí mať medzi :min a :max položiek.',
    ],
    'mimes' => ':attribute musí byť súbor typu: :values.',
    'mimetypes' => ':attribute musí byť súbor typu: :values.',
    'min' => [
        'numeric' => ':attribute musí mať aspoň :min.',
        'file' => ':attribute musí byť aspoň :min kilobytov.',
        'string' => ':attribute musí mať aspoň :min znakov.',
        'array' => ':attribute musí mať aspoň :min položiek.',
    ],
    'not_in' => 'Atribút :attribute je neplatný.',
    'not_regex' => ':attribute je neplatný.',
    'numeric' => ':attribute musí byť číslo.',
    'password' => 'Heslo je nesprávne.',
    'present' => ':attribute pole musí byť prítomné',
    'regex' => ':attribute je neplatný.',
    'required' => ':Pole attribute je povinné.',
    'required_if' => ' Pole :attribute je povinné ak :other je :value.',
    'required_unless' => 'Pole :attribute is povinné pokiaľ :other je v :values.',
    'required_with' => 'Pole :attribute je povinné ak :values je prítomné.',
    'required_with_all' => 'Pole :attribute je povinné ak :values je prítomná.',
    'required_without' => 'Pole :attribute je povinné ak :values nie je prítomná.',
    'required_without_all' => 'Pole :attribute je povinné ak žiadná z hodnôt: :values nie je prítomná.',
    'same' => ':attribute a :other musia byť rovnaké.',
    'size' => [
        'numeric' => 'Atribút :attribute musí byť veľkosti: :size.',
        'file' => 'Atribút :attribute musí mať :size kilobytov.',
        'string' => 'Atribút :attribute musí mať :size znakov.',
        'array' => 'Atribút :attribute musí obsahovať :size položiek.',
    ],
    'starts_with' => ':attribute musí začať s nasledujúcimi hodnotami: :values.',
    'string' => ':attribute musí byť reťazec.',
    'timezone' => ':attribute musí byť platná časová zóna.',
    'unique' => ':attribute už existuje.',
    'uploaded' => ':attribute sa nepodarilo nahrať.',
    'url' => ':attribute formát URL je neplatný.',
    'uuid' => 'The :attribute musí byť platný UUID.',
];
