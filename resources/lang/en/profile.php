<?php

return [
    'Delete Account' => 'Delete Account',
    'Permanently delete your account.' => 'Permanently delete your account.',
    'Once your account is deleted' => 'Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.',
    'Are you sure you want to delete your account?' => 'Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.',
    'Password' => 'Password',
    'Nevermind' => 'Nevermind',
    'Browser Session' => 'Browser Session',
    'Manage and logout' => 'Manage and logout your active sessions on other browsers and devices.',
    'If necessary, you may logout' => 'If necessary, you may logout of all of your other browser sessions across all of your devices. If you feel your account has been compromised, you should also update your password.',
];
