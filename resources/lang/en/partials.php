<?php
return [
    'Home'                             => 'Home',
    'Projects'                         => 'Projects',
    'Contact'                          => 'Contact',
    'Blog'                             => 'Blog',
    'Contacts'                         => 'Contacts',
    'Profile'                          => 'Profile',
    'Settings'                         => 'Settings',
    'Logout'                           => 'Logout',
    'Sign in'                          => 'Sign in',
    'Register'                         => 'Register',
    'Login'                            => 'Login',
    'Quick Links'                      => 'Quick Links',
    'General Links'                    => 'General Links',
    'About us'                         => 'About us',
    'Post'                             => 'Post',
    'All projects'                     => 'All projects',
    'FAQ'                              => 'FAQ',
    'My account'                       => 'My account',
    'List of jobs'                     => 'List of jobs',
    'Help us grow'                     => 'Help us grow',
    'Buy us a coffee'                  => 'Buy us a coffee',
    'Terms and conditions'             => 'Terms and conditions',
    'Privacy policy'                   => 'Privacy policy',
    'Keep in touch'                    => 'Keep in touch',
    'Created by'                       => 'Created by',
    'Live Preview'                     => 'Live Preview',
    'Jobs'                             => 'Jobs',
    'Sign In via Google'               => 'Sign In via Google',
    'Sign In via with Facebook'        => 'Sign In via with Facebook',
    'Sign In with LinkedIn'            => 'Sign In with LinkedIn',
    'Sign In via GitHub'               => 'Sign In via GitHub',
    'Or'                               => 'Or',
    'Email'                            => 'Email',
    'Password'                         => 'Password',
    'Remember me'                      => 'Remember me',
    'Forgot Password'                  => 'Forgot password?',
    'Dont have an account'             => 'Don’t have an account?',
    'Sign up'                          => 'Sign up',
    'Please confirm login email below' => 'Please confirm login email below',
    'You will receive an email'        => 'You will receive an email containing a link allowing you to reset your password to a new preferred one.',
    'Account'                          => 'Account',
    'Edit Project'                     => 'Edit Project',
    'Profile Settings'                 => 'Profile Settings',
    'Verification account'             => 'Verification account'

];
