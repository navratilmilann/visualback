<?php

return [
    'Get' => 'Get',
    'feedback' => 'feedback',
    'from another developers and' => 'from another developers and',
    'boost' => 'boost',
    'your UX & UI design' => 'your UX & UI design',
    'We Do Help Begginers Design' => 'We Do Help Begginers Design',
    "We're going to help you with your design" => "We're going to help you with your design",
    'Explore projects' => 'Explore projects',
    "Let's go on tour" => "Let's go on tour",
    'Project in Live-Preview' => 'Project in Live-Preview',
    'Cut through the noise with immediate, visual feedback.' => 'Cut through the noise with immediate, visual feedback. Go from design to build to publish without
					losing your mind in the process. We’ll save you time. You’ll save the day.',
    'Feedbacks' => 'Feedback',

    // Blog
    'We couldnt find any articles' => "We couldn't find any articles :(.",
    'Read more' => 'Read more',
    'Published' => 'Published',

];
