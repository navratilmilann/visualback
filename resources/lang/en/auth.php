<?php

return [
    'failed'                                => 'These credentials do not match our records.',
    'throttle'                              => 'Too many login attempts. Please try again in :seconds seconds.',
    'Forgot Your Password?'                 => 'Forgot Your Password?',
    'Never Mind'                            => 'Never Mind',
    'We get it, stuff happens.'             => "We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!.",
    'Send Password Reset Link'              => 'Send Password Reset Link',
    'Already have an acccount?'             => 'Already have an acccount?',
    'Login'                                 => 'Login',
    'Register'                              => 'Register',
    'No an account yet? :('                 => 'No an account yet? :(',
    'Login with GitHub'                     => 'Login with GitHub',
    'Login with Google'                     => 'Login with Google',
    'Login with Facebook'                   => 'Login with Facebook',
    'Login with LinkedIn'                   => 'Login with LinkedIn',
    'Register with GitHub'                  => 'Register with GitHub',
    'Register with Google'                  => 'Register with Google',
    'Register with Facebook'                => 'Register with Facebook',
    'Register with LinkedIn'                => 'Register with LinkedIn',
    'Remember me'                           => 'Remember me',
    'New to VisualBack?'                     => 'New to VisualBack?',
    'Thanks for signing up!'                => "Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gsend you another.",
    'A new verification link has been sent' => 'A new verification link has been sent to the email address you provided during registration.',
    'Resend Verification Email'             => 'Resend Verification Email',
    'Logout'                                => 'Logout',
    'Please confirm access to your account' => 'Please confirm access to your account by entering the authentication code provided by your authenticator application.',
    'emergency recovery codes.'             => 'Please confirm access to your account by entering one of your emergency recovery codes.',
    'Reset Password'                        => 'Reset Password',
    'Use a recovery code'                   => 'Use a recovery code',
    'Use an authentication code'            => 'Use an authentication code',
    'Account is not verified'               => 'Account is not verified. Please verify by clicking on the link in the email to verify your account.'


];
