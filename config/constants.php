<?php
return [
    'is_beta'                   => 1,
    'email'                     => 'visualbackapp@gmail.com',
    'base_logo'                 => '/img/logo/logo_sticky.svg',
    'base_logo_png'             => '/img/logo/logo_sticky.png',
    'facebook'                  => 'https://www.facebook.com/VisualBackApp/',
    'buy_me_a_coffee'           => 'https://www.buymeacoffee.com/VisualBackApp',
    'livepreview_base_url'      => 'http://%s.visualback.test',
    'discord'                   => 'https://discord.gg/CQgGss4Bkx',
    'admin_detail'              => 'http://admin.visualback.test/',
    'default_post_illustration' => '/storage/post/default.svg'
//    'instagram' => ''
];
