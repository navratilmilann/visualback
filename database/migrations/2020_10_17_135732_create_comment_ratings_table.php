<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentRatingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if(Schema::hasTable('comment_ratings')) return;

        Schema::create('comment_ratings', function (Blueprint $table) {
            $table->id();
            $table->integer('site_id');
            $table->integer('comment_id');
            $table->integer('addressee_id');
            $table->integer('evaluater_id');
            $table->integer('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('comment_ratings');
    }
}
