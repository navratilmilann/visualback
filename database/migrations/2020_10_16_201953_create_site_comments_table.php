<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteCommentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (Schema::hasTable('site_comments')) return;

        Schema::create('site_comments', function (Blueprint $table) {
            $table->id();
            $table->integer('site_id');
            $table->integer('user_id')->nullable();
            $table->string('nickname', 255)->nullable();
            $table->integer('parent_id')->nullable();
            $table->text('text')->nullable();
            $table->string('uri', 255)->nullable();
            $table->string('device', 30)->nullable();
            $table->string('rating', 255)->default(json_encode(['0' => 0, '1' => 0, 'count' => 0]));
            $table->text('target')->nullable();
            $table->string('ip', 40)->nullable();
            $table->string('session_id', 50)->nullable();
            $table->double('x', 14, 8)->nullable();
            $table->double('y', 14, 8)->nullable();
            $table->boolean('relative')->default(0);
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->string('browser', 20)->nullable();
            $table->integer('count')->nullable();
            $table->boolean('banned')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('site_comments');
    }
}
