<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->text('nickname')->nullable();
            $table->text('name')->nullable();
            $table->text('surname')->nullable();
            $table->text('email')->nullable();
//            $table->string('email', 255)->unique();     // ALTER TABLE `users` DROP INDEX `users_email_unique`;
            $table->text('hash_email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->text('profile_photo_path')->nullable();
            $table->string('identifier', 30)->nullable();
            $table->string('oauth_id', 100)->nullable();
            $table->string('oauth_service', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
