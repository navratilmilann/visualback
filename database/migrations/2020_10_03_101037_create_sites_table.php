<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSitesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sites', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable();
            $table->string('slug', 255)->nullable();
            $table->text('url')->nullable();
            $table->text('uri')->nullable();
            $table->string('base_url', 255)->nullable();
            $table->text('description')->nullable();
//            $table->string('rating', 255)->default(json_encode([["5" => 0, "4" => 0, '3' => 0, '2' => 0, '1' => 0], ["count" => 0, "sum" => 0, "average" => 0]]));
            $table->string('rating', 255)->default(json_encode(["0" => 0, "1" => 0, "count" => 0]));
            $table->boolean('verified')->default(0);
            $table->boolean('active')->default(0);
            $table->boolean('private')->default(0);
            $table->boolean('banned')->default(0);
            $table->integer('views')->default(0);
            $table->integer('first_open')->default(0);
            $table->string('identifier', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sites');
    }
}
