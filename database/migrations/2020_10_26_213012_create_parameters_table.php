<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('parameters')) return;

        Schema::create('parameters', function (Blueprint $table) {
            $table->id();
            $table->string('type',255)->nullable();
            $table->string('name',255)->nullable();
            $table->string('description',255)->nullable();
            $table->string('slug',255)->nullable();
            $table->integer('count')->default(0)->nullable();
            $table->integer('countable')->default(0)->nullable();
            $table->integer('active')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameters');
    }
}
