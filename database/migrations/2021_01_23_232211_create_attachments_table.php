<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttachmentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if(Schema::hasTable('attachments')) return;

        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('attachmentable_id')->nullable();
            $table->string('attachmentable_type', 100);
            $table->string('type', 30);
            $table->text('path')->nullable();
            $table->string('filename', 255)->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('attachments');
    }
}
