<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersParametersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if(Schema::hasTable('users_parameters')) return;

        Schema::create('users_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('parameter_id');
            $table->text('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users_parameters');
    }
}
