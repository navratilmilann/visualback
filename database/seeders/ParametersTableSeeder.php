<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parameters')->insert([
            'id' => 1,
            'type' => 'technology',
            'name' => 'Laravel',
            'description' => NULL,
            'slug' => 'laravel',
            'count' => 0,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 2,
            'type' => 'technology',
            'name' => 'Django',
            'description' => NULL,
            'slug' => 'django',
            'count' => 0,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 3,
            'type' => 'technology',
            'name' => 'HTML',
            'description' => NULL,
            'slug' => 'html',
            'count' => 3,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 4,
            'type' => 'technology',
            'name' => 'CSS',
            'description' => NULL,
            'slug' => 'css',
            'count' => 3,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 5,
            'type' => 'technology',
            'name' => 'javascript',
            'description' => NULL,
            'slug' => 'javascript',
            'count' => 4,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 6,
            'type' => 'type',
            'name' => 'Eshop',
            'description' => NULL,
            'slug' => 'eshop',
            'count' => -3,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 7,
            'type' => 'type',
            'name' => 'Presentation page',
            'description' => NULL,
            'slug' => 'presentation-page',
            'count' => 4,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 8,
            'type' => 'type',
            'name' => 'Portfolio',
            'description' => NULL,
            'slug' => 'portfolio',
            'count' => 0,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 10,
            'type' => 'country',
            'name' => 'Country',
            'description' => NULL,
            'slug' => 'country',
            'count' => 0,
            'language_id' => NULL
        ]);
        DB::table('parameters')->insert([
            'id' => 12,
            'type' => 'rating',
            'name' => 'Rating',
            'description' => NULL,
            'slug' => 'rating',
            'count' => 0,
            'language_id' => NULL
        ]);

    }
}
