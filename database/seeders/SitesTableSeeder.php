<?php

namespace Database\Seeders;

use App\Services\UniqueIdentifier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SitesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        for ($i = 0; $i < 10000; $i++) {

            $identifier = (new UniqueIdentifier())->getUniqueIdentifier('site');

            $id = DB::table('sites')->insertGetId([
                'name'       => 'startitup.sk',
                'slug'       => 'startitupsk-filozofia-kaizen-kazdy-den-sa-stan-o-3-lepsim-lepsim-clovekom-tu-je-vsetko-co-musis-spravit',
                'url'        => 'https://www.startitup.sk/filozofia-kaizen-kazdy-den-sa-stan-o-3-lepsim-lepsim-clovekom-tu-je-vsetko-co-musis-spravit/?fbclid=IwAR2H02SjbEom4C7_jDbGesvU3fa5Nl0P0UnSWY4zhG-V3FKdvKskw-yKY5Y',
                'uri'        => '/filozofia-kaizen-kazdy-den-sa-stan-o-3-lepsim-lepsim-clovekom-tu-je-vsetko-co-musis-spravit/?fbclid=IwAR2H02SjbEom4C7_jDbGesvU3fa5Nl0P0UnSWY4zhG-V3FKdvKskw-yKY5Y',
                'base_url'   => 'https://www.startitup.sk',
                'rating'     => '{"0":0,"1":0,"count":0}',
                'active'     => 1,
                'verified'   => 1,
                'views'      => 1,
                'identifier' => $identifier
            ]);

            DB::table('users_sites')->insert([
                'user_id'  => 26,
                'site_id'  => $id,
                'accepted' => 1
            ]);

            DB::table('attachments')->insert([
                'attachmentable_id'   => $id,
                'attachmentable_type' => 'App\Models\Site',
                'path'                => '/storage/screenshot/4.jpg',
                'type'                => 'screenshot',
                'active'              => 1
            ]);
        }
    }
}
