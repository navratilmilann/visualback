<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteComment extends Model {
    use HasFactory;

    protected $appends  = [
        'formatted_created_at', 'auth_user_liked', 'auth_user_reported', 'visitor_is_author', 'profile_photo_url'
    ];

    protected $fillable = [
        'site_id', 'text', 'parent_id', 'user_id', 'rating', 'uri', 'device', 'target', 'x', 'y', 'count',
        'browser', 'width', 'height', 'nickname', 'ip', 'banned', 'relative', 'session_id'
    ];

    protected $casts    = [
        'rating' => 'array',
    ];

    /*
     * Accessors & Mutators
     */
    public function getProfilePhotoUrlAttribute() {
        $nickname = $this->nickname . $this->session_id;
        $gravatar = "https://gravatar.com/avatar/" . md5(strtolower(trim($nickname . '@gmail.com'))) . "?s=400&d=robohash&r=r";

        return $this->user
            ? $this->user->profile_photo_url
            : $gravatar;
    }

    public function getAuthUserLikedAttribute() {
        if (\Auth::check()) {
            return \Auth::user()->hasLiked($this);
        }

        return false;
    }

    public function getFormattedCreatedAtAttribute() {

        if (Carbon::parse($this->attributes['created_at'])->isToday()) {
            return date('H:i', strtotime($this->attributes['created_at']));
        }

        return date('j.n.Y H:i', strtotime($this->attributes['created_at']));
    }

    public function getRatingAttribute($value) {
        return json_decode($value);
    }

    public function getVisitorIsAuthorAttribute() {
        if (!empty($this->attributes['user_id']) && $this->attributes['user_id'] == \Auth::id()) {
            return true;
        } elseif (!empty($this->attributes['session_id']) && $this->attributes['session_id'] == session()->getId()) {
            return true;
        }

        return false;
    }

    public function getAuthUserReportedAttribute() {

        if (\Auth::check())
            return $this->reports()->where('user_id', \Auth::id())->count() > 0;

        return false;
    }

    /*
     * Relations
     */
    public function reports() {
        return $this->hasMany('App\Models\SiteCommentReport', 'comment_id');
    }

    public function replies() {
        return $this->hasMany('App\Models\SiteComment', 'parent_id', 'id');
    }

    public function comment_rating() {
        return $this->hasMany('App\Models\CommentRating', 'comment_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function attachment() {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function site() {
        return $this->belongsTo(Site::class);
    }

    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }

    /**
     * Scopes
     */

}
