<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteCommentReport extends Model {
    use HasFactory;

    protected $fillable = [
        'user_id', 'comment_id', 'author_email_message', 'message', 'sent_email_at', 'archive'
    ];
}
