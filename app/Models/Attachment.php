<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model {
    use HasFactory;

    protected $fillable = [
        'path', 'active', 'type', 'attachmentable_type', 'attachmentable_id'
    ];

    protected $appends  = ['url'];

    /*
     * Accessors & Mutators
     */
    public function getUrlAttribute() {
        return url($this->attributes['path']);
    }

    /*
     * Relations
     */
    public function comment() {
        return $this->belongsTo('App\Models\SiteComment');
    }

    public function attachmentable() {
        return $this->morphTo();
    }
}
