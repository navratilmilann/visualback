<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {
    use HasFactory;

    protected $fillable = [
        'user_id', 'notification_type_id', 'value', 'seen'
    ];

    protected $appends  = [
        'formatted_created_at'
    ];

    /**
     * @return false|string
     */
    public function getFormattedCreatedAtAttribute() {

        if (Carbon::parse($this->attributes['created_at'])->isToday()) {
            return "Today " . date('H:i', strtotime($this->attributes['created_at']));
        }

        return date('j.n.Y H:i', strtotime($this->attributes['created_at']));
    }

    /**
     * Relations
     */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification_type() {
        return $this->belongsTo(NotificationType::class, 'notification_type_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
