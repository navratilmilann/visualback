<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parameter extends Model {
    use HasFactory;

    protected $fillable   = [
        'type', 'name', 'slug', 'description', 'count', 'countable'
    ];

    public    $timestamps = false;

    /*
     * Scope
     */
    public function scopeCountable($query) {
        return $query->where('countable', 1);
    }

    /*
     * Relations
     */
    public function sites() {
        return $this
            ->belongsToMany('App\Models\Site', 'sites_parameters', 'parameter_id', 'site_id')
            ->withPivot('value');
    }

    public function posts() {
        return $this->belongsToMany(Post::class, 'posts_parameters', 'parameter_id', 'post_id');
    }

    public function users() {
        return $this
            ->belongsToMany('App\Models\User', 'users_parameters', 'parameter_id', 'user_id')
            ->withPivot('value');
    }
}
