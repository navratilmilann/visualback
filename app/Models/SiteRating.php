<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteRating extends Model {
    use HasFactory;

    protected $fillable = ['user_id', 'site_id', 'type', 'value', 'text'];

    /**
     * Relations
     */
    public function user() {
        return $this->belongsTo('App\Models\Site', 'user_id', 'id');
    }
}
