<?php

namespace App\Models;

use App\Notifications\VerifyEmailNotification;
use App\Traits\Encryptable;
use App\Traits\Likes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail {

    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use Encryptable;
    use Likes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'surname', 'name', 'oauth_service', 'oauth_id', 'profile_photo_path',
        'hash_email', 'nickname', 'email_verified_at', 'identifier'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
//        'id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends     = [
        'profile_photo_url', 'detail_url', 'has_password',
//        'is_auth_user_follower',
    ];

    protected $encryptable = [
        'email',
        'name',
        'surname',
//        'nickname'
    ];

    /**
     * Get the URL to the user's profile photo.
     *
     * @return string
     */
    public function getProfilePhotoUrlAttribute() {
        if (preg_match("~^(?:f|ht)tps?://~i", $this->profile_photo_path)) {
            return $this->profile_photo_path;
        }

        $nickname = !empty($this->attributes['nickname']) ? $this->attributes['nickname'] : null;
        // "https://eu.ui-avatars.com/api/?name={$nickname}&background=500ade&color=fff"
        $gravatar = "https://gravatar.com/avatar/" . md5(strtolower(trim($nickname . '@gmail.com'))) . "?s=400&d=robohash&r=r";

        if (!empty($this->oauth_service) && $this->oauth_service == 'google') {
            return !empty($this->profile_photo_path)
                ? url('storage/' . $this->profile_photo_path)
                : $gravatar;
        }

        return !empty($this->profile_photo_path)
            ? url('storage/' . $this->profile_photo_path)
            : $gravatar;
    }

    /*
     * Relations
     */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites() {
        return $this->belongsToMany('App\Models\Site', 'users_sites');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\SiteComment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parameters() {
        return $this
            ->belongsToMany('App\Models\Parameter', 'users_parameters', 'user_id', 'parameter_id')
            ->where('active', 1)
            ->withPivot('value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications() {
        return $this->hasMany(Notification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followers() {
        return $this->hasMany(Follow::class, 'target_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followings() {
        return $this->hasMany(Follow::class, 'user_id');
    }

    /*
     * Accessors & Mutators
     */
    /**
     * @return string
     */
    public function getDetailUrlAttribute() {
        return route('profile.detail', ['identifier' => $this->attributes['identifier']]);
    }

    public function getHasPasswordAttribute() {
        return !empty($this->attributes['password']);
    }

    public function sendEmailVerificationNotification() {
        $this->notify(new VerifyEmailNotification());
    }

    public function getFollowersAttribute() {
        return $this->followers()->with('follower:id,identifier,nickname,profile_photo_path')->get();
    }

    public function getFollowingsAttribute() {
        return $this->followings()->with('following:id,identifier,nickname,profile_photo_path')->get();
    }

    /**
     * @return mixed
     */
//    public function getIsAuthUserFollowingAttribute() {
//
//        if (\Auth::check())
//            return (bool)$this->followings()->where('target_id', $this->id)->first(['id']);
//
//        return false;
//    }

    /**
     * @return bool
     */
    public function getIsAuthUserFollowerAttribute() {

        if (\Auth::check())
            return (bool)$this->followers()->where('user_id', \Auth::id())->first(['id']);

        return false;
    }

}
