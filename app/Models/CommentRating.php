<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentRating extends Model {
    use HasFactory;

    public    $timestamps = false;

    protected $fillable   = [
        'comment_id', 'addressee_id', 'evaluater_id', 'value', 'site_id'
    ];

    /*
     * Accessors & Mutators
     */
    /**
     * Relations
     */
    public function comment() {
        return $this->belongsTo('App\Models\SiteComment', 'id', 'comment_id');
    }
}
