<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Follow extends Model {
    use HasFactory;

    protected $fillable = ['target_id', 'user_id'];

    /*
     * Relations
     */
    public function follower() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function following() {
        return $this->belongsTo(User::class, 'target_id');
    }
}
