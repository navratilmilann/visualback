<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    use HasFactory;

    protected $fillable = [
        'title', 'text', 'user_id', 'slug', 'attributes', 'rating', 'excerpt'
    ];

    protected $appends  = [
        'illustration_url', 'auth_user_liked', 'formatted_created_at'
    ];

    protected $casts    = [
        'attributes' => 'array',
        'rating'     => 'array',
    ];

    /*
     * Mutator
     */
    public function setAttributesAttribute($attribute_array) {
        $this->attributes['attributes'] = json_encode($attribute_array);
    }

    /*
     * Accessor
     */
    public function getFormattedCreatedAtAttribute() {
        if (Carbon::parse($this->attributes['created_at'])->isToday()) {
            return 'Today ' . date('H:i', strtotime($this->attributes['created_at']));
        }

        return date('j.n.Y H:i', strtotime($this->attributes['created_at']));
    }

    public function getIllustrationUrlAttribute() {
        $attachment = $this->attachment()->first();

        return !empty($attachment)
            ? url($attachment->path)
            : url(config('constants.default_post_illustration'));
    }

    public function getAuthUserLikedAttribute() {
        if (\Auth::check()) {
            return \Auth::user()->hasLiked($this);
        }

        return false;
    }

    /*
     * Relations
     */
    public function tags() {
        return $this->belongsToMany(Parameter::class, 'posts_parameters')
            ->where('type', 'post-tag');
    }

    public function comments() {
        return $this->hasMany(PostComment::class);
    }

    public function attachment() {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
}
