<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model {

    use HasFactory;

    protected $fillable = [
        'banned', 'name', 'url', 'uri', 'base_url', 'slug', 'description', 'verified', 'active', 'base_url', 'views', 'identifier', 'rating', 'first_open', 'private'
    ];

    protected $appends  = [
        'live_preview_url', 'is_auth_user_collaborator', 'prototype_user',
        'author_excerpt', 'auth_user_liked', 'auth_user_reported'
    ];

    protected $casts    = [
        'rating' => 'array',
    ];

    private function getAuthors() {
        return $this->users()->select('users.id', 'nickname', 'users.identifier','profile_photo_path')->get();
    }

    /*
     * Accessors & Mutators
     */

    /**
     * @return string
     */
    public function getLivePreviewUrlAttribute() {
        return route('site.livePreview', ['identifier' => $this->attributes['identifier'], 'slug' => $this->attributes['slug']]);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getRatingAttribute($value) {
        return json_decode($value);
    }

    /**
     * @return bool
     */
    public function getIsAuthUserCollaboratorAttribute() {
        $authUser = \Auth::user();

        if ($authUser) {
            return $this->getAuthors()->where('id', $authUser->id)->count() > 0;
        }

        return false;
    }

//    /**
//     * @return mixed
//     */
//    public function getParameterIdiesAttribute() {
//        return $this->parameters()->pluck('parameters.id');
//    }

    /**
     * @return mixed|null
     */
    public function getPrototypeUserAttribute() {
        return $this->getAuthors()->first();
    }

    /**
     * @return false
     */
    public function getAuthUserLikedAttribute() {

        if (\Auth::check()) {
            return \Auth::user()->hasLiked($this);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getAuthUserReportedAttribute() {

        if (\Auth::check()) {
            return $this->reports()->where('user_id', \Auth::id())->count() > 0;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getAuthorExcerptAttribute() {

        $users       = $this->getAuthors();
        $authorCount = $users->count();
        $othersCount = $authorCount - 1;

        if ($authorCount == 2) {
            return "{$users->first()->nickname} and {$othersCount} other";
        } elseif ($authorCount > 1) {
            return "{$users->first()->nickname} and {$othersCount} others";
        } elseif ($authorCount > 0) {
            return "{$users->first()->nickname}";
        }
    }

    /**
     * Relations
     */
    public function reports() {
        return $this->hasMany('App\Models\SiteReport');
    }

    public function rating() {
        return $this->hasMany('App\Models\SiteRating');
    }

    public function parameters() {
        return $this
            ->belongsToMany('App\Models\Parameter', 'sites_parameters', 'site_id', 'parameter_id')
            ->withPivot('value');
    }

    public function technologies() {
        return $this->parameters()->where('type', 'technology');
    }

    public function attachment() {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }

    public function comments() {
        return $this->hasMany(SiteComment::class);
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'users_sites')
            ->wherePivot('accepted', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }

    /*
     * Scope
     */
    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query) {
        return $query->where('active', 1)->where('banned', 0)->where('verified', 1);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopePublic($query) {
        return $query->active()->where('private', 0);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeListing($query) {
        return $query->active()->select('sites.id', 'name', 'rating', 'slug', 'views', 'identifier', 'banned', 'private')
            ->with([
                'users'        => function ($query) {
                    $query->select('users.id', 'nickname', 'profile_photo_path', 'identifier')
                        ->whereNotNull('email_verified_at');
                },
                'attachment'   => function ($query) {
                    $query->select('attachments.id', 'path', 'attachmentable_id', 'attachmentable_type');
                },
                'technologies' => function ($query) {
                    $query->select('slug', 'name', 'type');
                },
            ])
            ->withCount('comments');
    }

    /*
     * Methods
     */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function auth_user_sites() {
        return $this->users()->wherePivot('accepted', 1);
    }
}
