<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiteReport extends Model {
    use HasFactory;

    protected $fillable = [
        'user_id', 'site_id', 'author_email_message', 'message', 'sent_email_at', 'archive'
    ];

    public function getCreatedAtAttribute($value) {
        return date('j.n.Y H:i', strtotime($value));
    }

    public function reporter() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function site() {
        return $this->belongsTo(Site::class, 'site_id');
    }
}
