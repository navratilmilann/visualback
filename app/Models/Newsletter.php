<?php

namespace App\Models;

use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model {

    use HasFactory;
    use Encryptable;

    protected $fillable    = [
        'email', 'hash_email', 'user_id'
    ];

    protected $encryptable = [
        'email',
        'name',
        'surname',
        'nickname'
    ];
}
