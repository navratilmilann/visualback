<?php

namespace App\Jobs;

use App\Mail\ReportedModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Throwable;

class NewReportEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable;

    private $type, $id, $user_id;

    /**
     * NewReportEmail constructor.
     * @param $type
     * @param $id
     * @param $user_id
     */
    public function __construct($type, $id, $user_id) {
        $this->type    = $type;
        $this->id      = $id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        Mail::to(config('constants.email'))->send(new ReportedModel($this->type, $this->id, $this->user_id));
    }

    public function failed(Throwable $exception) {
        Log::error('JobFailed. ' . json_encode(['exception' => $exception->getMessage()]));
    }
}
