<?php

namespace App\Jobs;

use App\Mail\Site\NewComment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendNewCommentEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable;

    private $comment, $user;

    /**
     * SendNewCommentEmail constructor.
     * @param $comment
     * @param $user
     */
    public function __construct($comment, $user) {
        $this->comment = $comment;
        $this->user    = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        if (config('app.env') == 'production') {
            Mail::to($this->user->email)->send(new NewComment($this->comment, $this->user));
        }
    }
}
