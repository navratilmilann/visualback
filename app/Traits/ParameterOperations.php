<?php

namespace App\Traits;

use App\Models\Parameter;
use Illuminate\Support\Collection;

trait ParameterOperations {
    /*
     * Syncing parameters with model
     * and syncign parameters and setting value in relation table
     */

    /**
     * @param array $data
     * @param array $types
     * @return false|string
     */
    private function getJsonableAttributes(array $data, array $types) {

        $json = [];

        foreach ($types as $type) {
            if (!empty($data[$type])) {
                $json[$type] = $data[$type];
            }
        }

        return json_encode($json);
    }

    /**
     * @param array $data
     * @param string $attribute
     * @return mixed
     */
    private function getParametersIdByAttribute(array $data, string $attribute = 'name') {
        return Parameter::select('id')
            ->whereIn($attribute, $data)
            ->pluck('id');
    }

    /**
     * @param $model
     * @param array $types
     */
    private function detachParametersByType($model, array $types): void {
        // delete old parameters relations with user
        $model->parameters()->detach(
            $model->parameters()->whereIn('type', $types)->pluck('parameter_id')
        );
    }

    /**
     * @param $model
     * @param $data
     * @param array $types
     */
    private function updateParameterByType($model, $data, array $types): void {

        self::decrementParameters($model, $types);
        self::detachParametersByType($model, $types);

        self::attachParameters($model, $data);
        self::incrementParameters($model);
    }

    /**
     * @param $model
     * @param array $data
     */
    private function updateParameterValueByIds($model, array $data): void {

        $attach_data = [];

        foreach ($data as $key => $item) {
            $attach_data[$key] = [
                'value' => $item
            ];
        }

        self::detachParametersByType($model, array_keys($data));

        $model->parameters()->syncWithoutDetaching($attach_data);
    }

    /**
     * @param $model
     */
    private function incrementParameters($model): void {
        $model->parameters()->increment('count');
    }

    /**
     * @param $model
     * @param $types
     */
    private function decrementParameters($model, $types): void {
        $model->parameters()->whereIn('type', $types)->decrement('count');
    }

    /**
     * @param $model
     * @param $data
     */
    private function attachParameters($model, $data): void {
        $model->parameters()->attach($data);
    }
}
