<?php

namespace App\Traits;

use App\Models\Like;

trait Likes {

    /**
     * @return mixed
     */
    public function likes() {
        return $this->hasMany(Like::class);
    }

    /**
     * @param  $likeable
     * @return $this
     */
    public function addLike($likeable): self {

        if ($this->hasLiked($likeable)) {
            return $this;
        }

        (new Like)
            ->user()->associate($this)
            ->likeable()->associate($likeable)
            ->save();

        return $this;
    }

    /**
     * @param $likeable
     * @return $this
     */
    public function removeLike($likeable): self {

        if (!$this->hasLiked($likeable)) {
            return $this;
        }

        $likeable->likes()->whereHas('user', fn($q) => $q->whereId($this->id))->delete();

        return $this;
    }

    /**
     * @param $likeable
     * @return bool
     */
    public function hasLiked($likeable): bool {
        if (!$likeable->exists) {
            return false;
        }

        return $likeable->likes()->whereHas('user', function ($query) {
            return $query->whereId($this->id);
        })->exists();
    }

    /**
     * @param $likeable
     * @return \App\Models\User|Likes
     */
    public function toggleLike($likeable) {
        if ($this->hasLiked($likeable)) {
            return $this->removeLike($likeable);
        } else {
            return $this->addLike($likeable);
        }
    }
}