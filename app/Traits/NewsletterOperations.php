<?php

namespace App\Traits;

use App\Models\Newsletter;

trait NewsletterOperations {
    /**
     * @param $email
     * @param $user_id
     */
    public function storeNewsletter($email, $user_id) {
        if (!$this->userActivatedNewsletter($email)) {
            Newsletter::create([
                'email'      => $email,
                'user_id'    => $user_id,
                'hash_email' => hash_email($email),
            ]);
        }
    }

    /**
     * @param $email
     * @param $user_id
     */
    public function deleteNewsletter($email, $user_id) {
        if ($this->userActivatedNewsletter($email)) {
            Newsletter::where('user_id', $user_id)->delete();
        }
    }

    /**
     * @param $email
     * @return mixed
     */
    public function userActivatedNewsletter($email) {
        return Newsletter::where('hash_email', hash_email($email))->count();
    }
}
