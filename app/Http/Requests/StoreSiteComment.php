<?php

namespace App\Http\Requests;

use App\Rules\Recaptcha;
use Illuminate\Foundation\Http\FormRequest;

class StoreSiteComment extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'site_id'         => 'required|integer',
            'text'            => 'required|string',
            'uri'             => 'required|string',
            'device'          => 'required|string',
            'attachment_id'   => 'integer|nullable',
            'target'          => 'string|nullable',
            'x'               => 'numeric|nullable',
            'y'               => 'numeric|nullable',
            'count'           => 'integer|nullable',
            'g-recaptcha'     => ['required', new Recaptcha]
        ];
    }
}
