<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Services\UpdateNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request) {

        $updateNotification = new UpdateNotification($request->all());
        $notificationResult = $updateNotification->do();

        return $notificationResult;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request) {

        Notification::where('id', $request->id)->delete();

        return response()->json([
            'message' => __('flash-messages.deleted_notification'),
            'error'   => 0
        ]);
    }

    /**
     * @param string $userId
     * @param Request $request
     * @return JsonResponse
     */
    public function notifications(string $userId, Request $request): JsonResponse {

        $notifications = Notification::where('user_id', \Auth::id())
            ->with('notification_type')
            ->get();

        return response()->json($notifications);
    }

    /**
     * @param string $userId
     * @param Request $request
     * @return JsonResponse
     */
    public function notificationCount(string $userId, Request $request): JsonResponse {

        $notificationCount = Notification::where('user_id', \Auth::id())->count();

        return response()->json($notificationCount);
    }
}
