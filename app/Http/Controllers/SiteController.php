<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteSite as DeleteSiteRequest;
use App\Http\Requests\StoreSite as StoreSiteRequest;
use App\Http\Requests\ToggleLike;
use App\Models\Parameter;
use App\Models\Site;
use App\Services\Site\AuthorizeSite;
use App\Services\Site\DeleteSite;
use App\Services\Site\SetSiteCollaborators;
use App\Services\Site\StoreSite;
use App\Services\Site\StoreSiteRating;
use App\Services\Site\UpdateSite;
use App\Services\ToggleModelLike;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateSite as UpdateSiteRequest;

class SiteController extends Controller {

    private const DEMO_IDENTIFIER = 'demo';

    /**
     * @return View
     */
    public function index(): View {
        return view('site.index');
    }

    /**
     * @param string $identifier
     * @param Request $request
     * @return View
     */
    public function edit(string $identifier, Request $request): View {
        $site              = Site::with('parameters')->where('identifier', $identifier)->first();
        $siteTechnologies  = $site->parameters->where('type', 'technology')->pluck('name');
        $siteType          = $site->parameters->where('type', 'site-type')->pluck('name');
        $typeOptions       = Parameter::where('type', 'site-type')->pluck('name');
        $technologyOptions = Parameter::where('type', 'technology')->orderBy('name', 'ASC')->pluck('name');
        $redirectUrl       = url()->previous();

        if (empty($site)) {
            abort(404);
        }

        return view('site.edit', compact(
            'site', 'typeOptions', 'technologyOptions', 'siteTechnologies', 'siteType', 'redirectUrl'
        ));
    }

    /**
     * @param UpdateSiteRequest $request
     * @return RedirectResponse
     */
    public function update(UpdateSiteRequest $request): RedirectResponse {
        $updateSite = new UpdateSite($request->except('_token'));
        $updateSite->do();

        return redirect()->to($request->redirect_url);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View
     */
    public function demoLivepreview() {
        $site = Site::select('id', 'uri', 'name', 'identifier', 'slug', 'created_at', 'rating', 'first_open')
            ->where('identifier', self::DEMO_IDENTIFIER)->first();

        $site->increment('views');

        return view('site.live-preview', compact(
            'site'
        ));
    }

    /**
     * @param string $identifier
     * @param string $slug
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View|RedirectResponse
     */
    public function livePreview(string $identifier, string $slug, Request $request) {
        $site = Site::select('id', 'uri', 'name', 'identifier', 'slug', 'created_at', 'rating', 'first_open')
            ->with(
                'users:id,name,identifier',
                'users.parameters:type',
                'parameters:name,type',
                'attachment:attachmentable_id,attachmentable_type,path'
            )->where('identifier', $identifier)->first();

        if (empty($site)) {
            return redirect()->to('/project/not-found');
        }

        if ($site->is_auth_user_collaborator && !$site->first_open) {
            $site->update([
                'first_open' => 1
            ]);
            $site->first_open = 0;
        }

        $site->increment('views');

        return view('site.live-preview', compact(
            'site'
        ));
    }

    /**
     * @param Request $request
     * @return array|string[]
     */
    private function getOrderBy(Request $request): array {

        $orderBy = [
            'column' => 'created_at',
            'sort'   => 'desc'
        ];

        if (!empty($request->order_by)) {
            if ($request->order_by == 'popular') {
                $orderBy = [
                    'column' => 'views',
                    'sort'   => 'desc'
                ];
            } else {
                $orderBy = [
                    'column' => 'created_at',
                    'sort'   => $request->order_by
                ];
            }
        }

        return $orderBy;
    }

    /**
     * @param int $id
     * @param string $slug
     * @return JsonResponse
     */
    public function getSite(int $id, string $slug): JsonResponse {

        $site = Site::select('sites.id', 'user_id', 'name', 'created_at', 'rating', 'slug', 'identifier')
            ->where('slug', $slug)
            ->with('user:id,name,profile_photo_path')
            ->first();

        return response()->json($site);
    }

    /**
     * @param Request $request
     * @param int $limit
     * @param int $offset
     * @return JsonResponse
     */
    public function getSites(Request $request, $limit = 15, $offset = 0): JsonResponse {

        $orderBy = self::getOrderBy($request);

        $sites = Site::listing()->active()->public();

        if ($request->search) {
            $sites = $sites->where('name', 'LIKE', '%' . $request->search . '%');
        }

        $sites = $sites->orderBy($orderBy['column'], $orderBy['sort'])
            ->skip($offset)
            ->take($limit)
            ->get();

        return response()->json($sites);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function storeSiteRating(Request $request): JsonResponse {

        $storeSiteRating = new StoreSiteRating($request->all());
        $siteResults     = $storeSiteRating->do();

        return $siteResults;
    }

    /**
     * @param DeleteSiteRequest $request
     * @return JsonResponse
     */
    public function deleteSite(DeleteSiteRequest $request): JsonResponse {

        $deleteSite  = new DeleteSite(['identifier' => $request->identifier]);
        $siteResults = $deleteSite->do();

        return $siteResults;
    }

    /**
     * @param StoreSiteRequest $request
     * @return JsonResponse
     */
    public function storeSite(StoreSiteRequest $request): JsonResponse {

        $storeSite = new StoreSite($request->all());
        $site      = $storeSite->do();

        return $site;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setCollaborators(Request $request): JsonResponse {

        $setSiteMembers = new SetSiteCollaborators($request->all());
        $siteMembers    = $setSiteMembers->do();

        return $siteMembers;
    }

    /**
     * @param ToggleLike $request
     * @return JsonResponse
     */
    public function toggleLike(ToggleLike $request) {

        $toggleModelLike = new ToggleModelLike(['id' => $request->id, 'type' => $request->segment(2)]);
        $like            = $toggleModelLike->do();

        return $like;
    }

    /**
     * @param $identifier
     * @param Request $request
     * @return JsonResponse
     */
    public function authorizeSite($identifier, Request $request) {

        $authorizeSite         = new AuthorizeSite($request->identifier, $request->private);
        $authorizeSiteResponse = $authorizeSite->do();

        return $authorizeSiteResponse;
    }
}
