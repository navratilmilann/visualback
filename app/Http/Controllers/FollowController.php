<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\ToggleFollow;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FollowController extends Controller {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggleFollow(Request $request): JsonResponse {

        $toggleFollow = new ToggleFollow($request);
        $response     = $toggleFollow->do();

        return $response;
    }

    /**
     * @param $identifier
     * @return JsonResponse
     */
    public function followers($identifier) {

        $followers = User::where('identifier', $identifier)->first()->followers;

        return response()->json($followers);
    }

    /**
     * @param $identifier
     * @return JsonResponse
     */
    public function followings($identifier) {

        $followings = User::where('identifier', $identifier)->first()->followings;

        return response()->json($followings);
    }
}
