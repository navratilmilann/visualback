<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Parameter;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ParameterController extends Controller {

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getTechnologies(Request $request): JsonResponse {
        return response()->json(Parameter::select('name')->where('type', 'technology')->pluck('name'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getParameter(Request $request): JsonResponse {

        if ($request->pluck)
            return response()->json(Parameter::select('name')->where('type', $request->type)->pluck($request->pluck));

        return response()->json(Parameter::select('name', 'id', 'description')->where('type', $request->type)->get());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getCountries(Request $request): JsonResponse {
        return response()->json(Country::select('name')->where('name', 'LIKE', '%' . $request->name . '%')->pluck('name'));
    }

    /**
     * https://github.com/shivammathur/countrycity/blob/master/example/index.html
     * @param Request $request
     * @return JsonResponse
     */
    public function getCities(Request $request): JsonResponse {

        $user    = \Auth::user();
        $country = $user->parameters()->where('type', 'user-country')->first()->pivot->value;
        $cities  = [];

        if ($country) {

            $countryCode = Country::select('code')->where('name', $country)->first()->code;

            $cities = file_get_contents("http://api.geonames.org/searchJSON?username=ksuhiyp&country={$countryCode}&name_startsWith={$request->city}");
            $cities = json_decode($cities);
            $cities = $cities->geonames;
        }

        return response()->json(
            $cities
        );
    }
}
