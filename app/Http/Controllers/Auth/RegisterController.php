<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterUser;
use App\Http\Requests\Auth\VerificationAccount;
use App\Models\Newsletter;
use App\Models\User;
use App\Services\Profile\UpdateSettings;
use App\Services\StoreNewsletter;
use App\Services\UniqueIdentifier;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;

class RegisterController extends Controller {

    /**
     * @param User $user
     * @param Request $request
     */
    private function storeNewsletter(User $user, Request $request) {

        if (empty($request->newsletter)) {

            $newsletter_email = Newsletter::where('hash_email', hash_email($user->email))->first();

            if (!empty($newsletter_email)) {
                $newsletter_email->update(['user_id' => $user->id]);
            } else {
                $storeNewsletter = new StoreNewsletter($request->all(), $user);
                $storeNewsletter->do();
            }
        }
    }

    /**
     * @param Request $request
     * @return View
     */
    public function verificationAccount(Request $request): View {

        $hash = $request->hash;
        $user = User::select('id', 'name', 'nickname', 'email', 'password', 'identifier')->where('id', $request->id)->first();

        return view('auth.verification-account', compact(
            'hash', 'user'
        ));
    }

    /**
     * @param VerificationAccount $request
     * @return false|RedirectResponse
     */
    public function verifiedAccount(VerificationAccount $request) {

        $user = User::where('identifier', $request->identifier)->first();

        if (!hash_equals((string)$request->hash, sha1($user->email))) {
            Session::put('error', __('flash-messages.failed_verification'));

            return redirect()->to('/login');
        }

        (new UpdateSettings($request->all(), $user))->do();

        // todo: account succesfuly verified - email
        $user->update(['email_verified_at' => now()]);

        Session::put('success', __('flash-messages.verified_account'));

        \Auth::login($user);

        return redirect()->to('/project');
    }

    /**
     * @return View
     */
    public function showRegisterForm(): View {
        return view('auth.register');
    }

    /**
     * @param RegisterUser $request
     * @return \Illuminate\Contracts\Foundation\Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterUser $request) {

        $existingUser = User::select('email')->where('hash_email', hash_email($request->email))->first();

        if ($existingUser) {

            $service = explode('@', $request->email)[1];
            $email   = $existingUser->email[0] . '****@' . $service;

            Session::put('error', "The user with email {$email} has already been registered.");

            return redirect()->back();

        } else {

            $explodedEmail = explode('@', $request->email);
            $nickname      = $explodedEmail[0];

            $newUser = User::create([
                'name'       => $nickname,
                'nickname'   => $nickname,
                'email'      => $request->email,
                'hash_email' => hash_email($request->email),
                'password'   => bcrypt($request->password),
                'identifier' => (new UniqueIdentifier())->getUniqueIdentifier('user')
            ]);

            self::storeNewsletter($newUser, $request);

            Session::put('warning', __('flash-messages.thanks_for_sign_up'));

            event(new Registered($newUser));
        }

        return redirect('/login')
            ->with([
                'user-identifier' => $newUser->identifier,
                'is-gmail'        => $explodedEmail[1] === 'gmail.com'
            ]);
    }

    /**
     * @param string $identifier
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendVerificationEmail(string $identifier): JsonResponse {

        $user = User::where('identifier', $identifier)->first();

        event(new Registered($user));

        return response()->json([
            'type'    => 'success',
            'message' => 'Verification email already sent'
        ]);
    }
}
