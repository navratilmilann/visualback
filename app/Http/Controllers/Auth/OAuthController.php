<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UniqueIdentifier;
use Illuminate\Http\RedirectResponse;
use App\Models\User;
use Mockery\Exception;
use Laravel\Socialite\Facades\Socialite;

class OAuthController extends Controller {

    /**
     * Create a new controller instance
     * @return RedirectResponse
     */
    public function redirectToGithub(): \Symfony\Component\HttpFoundation\RedirectResponse {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Create a new controller instance.
     * @return RedirectResponse
     */
    public function handleGithubCallback(): RedirectResponse {

        try {

            $user         = Socialite::driver('github')->stateless()->user();
            $existingUser = User::where('hash_email', hash_email($user->email))->first();

            if ($existingUser) {

                \Auth::login($existingUser);

            } else {

                $explodedEmail = explode('@', $user->email);
                $nickname      = $explodedEmail[0];

                $newUser = User::create([
                    'nickname'           => $nickname,
                    'name'               => $user->nickname,
                    'email'              => $user->email,
                    'hash_email'         => hash_email($user->email),
                    'profile_photo_path' => $user->avatar,
                    'oauth_id'           => $user->id,
                    'oauth_service'      => 'github',
                    'identifier'         => (new UniqueIdentifier())->getUniqueIdentifier('user'),
                    'email_verified_at'  => now()
                ]);

                \Auth::login($newUser, true);
            }

            return redirect('/project');

        } catch (Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToGoogle(): \Symfony\Component\HttpFoundation\RedirectResponse {
        return Socialite::driver('google')->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleGoogleCallback() {

        try {

            $user         = Socialite::driver('google')->stateless()->user();
            $existingUser = User::where('hash_email', hash_email($user->email))->first();

            if ($existingUser) {

                \Auth::login($existingUser, true);

            } else {

                $explodedEmail = explode('@', $user->email);
                $nickname      = $explodedEmail[0];

                $newUser = User::create([
                    'nickname'           => $nickname,
                    'name'               => $user->name,
                    'email'              => $user->email,
                    'hash_email'         => hash_email($user->email),
                    'profile_photo_path' => $user->avatar_original,
                    'oauth_id'           => $user->id,
                    'oauth_service'      => 'google',
                    'identifier'         => (new UniqueIdentifier())->getUniqueIdentifier('user'),
                    'email_verified_at'  => now()
                ]);

                \Auth::login($newUser);
            }

            return redirect()->to('/project');

        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * @return mixed
     */
    public function redirectToLinkedin() {
        return Socialite::driver('linkedin')->scopes(['r_liteprofile', 'r_emailaddress'])->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleLinkedinCallback() {

        try {

            $user         = Socialite::driver('linkedin')->stateless()->user();
            $existingUser = User::where('hash_email', hash_email($user->email))->first();

            if ($existingUser) {

                \Auth::login($existingUser, true);

            } else {

                $explodedEmail = explode('@', $user->email);
                $nickname      = $explodedEmail[0];

                $newUser = User::create([
                    'nickname'          => $nickname,
                    'name'              => $user->name,
                    'email'             => $user->email,
                    'hash_email'        => hash_email($user->email),
//                    'profile_photo_path' => $user->avatar_original,
                    'oauth_id'          => $user->id,
                    'oauth_service'     => 'linkedin',
                    'identifier'        => (new UniqueIdentifier())->getUniqueIdentifier('user'),
                    'email_verified_at' => now()
                ]);

                \Auth::login($newUser);
            }

            return redirect()->to('/project');

        } catch (\Exception $e) {
            return redirect('/login');
        }
    }

    /**
     * Facebook
     */
    /**
     * @return mixed
     */
    public function redirectToFacebook() {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handleFacebookCallback() {

        try {

            $user         = Socialite::driver('facebook')->stateless()->user();
            $existingUser = User::where('hash_email', hash_email($user->email))->first();

            if ($existingUser) {

                \Auth::login($existingUser, true);

            } else {

                $explodedEmail = explode('@', $user->email);
                $nickname      = $explodedEmail[0];

                $newUser = User::create([
                    'nickname'          => $nickname,
                    'name'              => $user->name,
                    'email'             => $user->email,
                    'hash_email'        => hash_email($user->email),
//                    'profile_photo_path' => $user->avatar_original,
                    'oauth_id'          => $user->id,
                    'oauth_service'     => 'facebook',
                    'identifier'        => (new UniqueIdentifier())->getUniqueIdentifier('user'),
                    'email_verified_at' => now()
                ]);

                \Auth::login($newUser);
            }

            return redirect()->to('/project');

        } catch (\Exception $e) {
            return redirect('/login');
        }
    }
}
