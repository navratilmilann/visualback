<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\Recaptcha;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller {

    use AuthenticatesUsers;

    /**
     * @throws ValidationException
     */
    private function sendFailedNotVerifiedAccount() {
        throw ValidationException::withMessages([
            $this->username() => [__('flash-messages.thanks_for_sign_up')],
        ]);
    }

    /**
     * @return string
     */
    private function redirectPath(): string {
        return '/project';
    }

    /**
     * comparision of hashed email and password
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request): array {
        return ['hash_email' => hash_email($request->{$this->username()}), 'password' => ($request->password)];
    }

    protected function validateLogin(Request $request) {
        $request->validate([
            $this->username() => 'required|string',
            'password'        => 'required|string',
            'g-recaptcha'     => ['required', new Recaptcha]
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request) {

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            // account is not verified
            if (empty($request->user()->email_verified_at)) {

//                $this->guard()->logout();
                Session::put('info', __('flash-messages.thanks_for_sign_up'));
//                return $this->sendFailedNotVerifiedAccount($request);
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout(Request $request) {

        \Auth::logout();

        return redirect('/login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function forgotPassword(Request $request) {

        $request->validate([
            'email'       => 'required|email',
            'g-recaptcha' => ['required', new Recaptcha]
        ]);

        $email = $request->email;
        $token = Str::random(64);
        $user  = User::where('hash_email', hash_email($request->email))->first();

        if (!empty($user)) {

            DB::table('password_resets')->insert(
                ['email' => Crypt::encrypt($email), 'token' => $token, 'created_at' => Carbon::now()]
            );

            Mail::send('emails.auth.reset-password', ['token' => $token, 'email' => $email], function ($message) use ($email) {
                $message->to($email);
                $message->subject('Reset Password Notification');
            });

            return redirect()->route('home')->with('info', 'We have e-mailed your password reset link!');
        }

        return redirect()->route('home')->with('error', "User with this email does not exist!");
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showForgotPassword() {
        return view('auth.forgot-password');
    }

    /**
     * @param $token
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showResetPassword($token) {
        return view('auth.reset-password', compact('token'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function resetPassword(Request $request) {

        $request->validate([
            'password'              => 'required|string|min:8|confirmed',
            'password_confirmation' => 'required',
            'token'                 => 'required',
        ]);

        $resetPassword = DB::table('password_resets')
            ->where(['token' => $request->token])
            ->first();

        if (!$resetPassword) {
            return back()->withInput()->with('error', 'Invalid token!');
        }

        $hashEmail = hash_email(Crypt::decrypt($resetPassword->email));

        User::where('hash_email', $hashEmail)->update([
            'password' => Hash::make($request->password)
        ]);

        DB::table('password_resets')->where(['token' => $request->token])->delete();

        return redirect('/login')->with('success', 'Your password has been changed!');
    }
}
