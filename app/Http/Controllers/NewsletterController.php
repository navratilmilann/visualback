<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsletter as StoreNewsletterRequest;
use App\Services\StoreNewsletter;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NewsletterController extends Controller {

    /**
     * @param StoreNewsletterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNewsletter(StoreNewsletterRequest $request): JsonResponse {

        $storeNewsletter         = new StoreNewsletter($request->all());
        $storeNewsletterResponse = $storeNewsletter->do();

        return $storeNewsletterResponse;
    }
}
