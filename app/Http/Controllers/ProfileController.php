<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateSettings as UpdateSettingsRequest;
use App\Models\Site;
use App\Models\User;
use App\Services\Profile\UpdateSettings;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller {

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function detail(Request $request): View {
        $siteLikes    = 0;
        $commentLikes = 0;
        $user         = User::select('id', 'name', 'profile_photo_path', 'oauth_service', 'nickname', 'identifier')
            ->with(
                'sites:id,slug,rating,name,description,created_at,active,identifier',
                'parameters:type,name,slug'
            )
            ->withCount('comments')
            ->where('identifier', $request->identifier)
            ->first();

        if (empty($user)) {
            abort(404);
        }

        if (!empty($user->sites)) {
            $siteLikes = $user->sites->sum(function ($site) {
                return $site->rating->{1};
            });
        }

        if (!empty($user->comments)) {
            $commentLikes = $user->comments->where('parent_id', null)->sum(function ($comment) {
                return $comment->rating->{1};
            });
        }

        return view('profile.detail', compact(
            'user', 'siteLikes', 'commentLikes'
        ));
    }

    /**
     * @param string $identifier
     * @param Request $request
     * @return View
     */
    public function settings(string $identifier, Request $request): View {
        return view('profile.settings');
    }


    /**
     * @param string $identifier
     * @return RedirectResponse
     */
    public function delete(string $identifier): RedirectResponse {

        $user = User::where('identifier', $identifier)->first();

        $user->delete();

        return redirect()->route('login');
    }

    /**
     * @param $identifier
     * @param Request $request
     * @return JsonResponse
     */
    public function userProjects($identifier, Request $request): JsonResponse {

        $userId  = optional(User::select('id')->where('identifier', $identifier)->first())->id;
        $siteIds = DB::table('users_sites')
            ->select('site_id')
            ->where('user_id', $userId)
            ->where('accepted', '=', 1)
            ->get()
            ->pluck('site_id');
        $sites   = Site::select('sites.id', 'name', 'rating', 'slug', 'views', 'identifier', 'banned', 'private')
            ->whereIn('sites.id', $siteIds)
            ->where('active', 1)
            ->where('verified', 1);

        if (!\Auth::check() || \Auth::user()->identifier !== $identifier) {                                             // for others show only public projects
            $sites = $sites->where('private', 0);
        }

        $sites = $sites
            ->with([
                'users'        => function ($query) {
                    $query->select('users.id', 'name', 'nickname', 'profile_photo_path', 'identifier');
                },
                'attachment'   => function ($query) {
                    $query->select('path', 'attachmentable_id', 'attachmentable_type');
                },
                'technologies' => function ($query) {
                    $query->select('slug', 'name');
                },
            ])
            ->withCount('comments')
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json($sites);
    }

    /**
     * @param string|null $identifier
     * @return JsonResponse
     */
    public function user(string $identifier = null): JsonResponse {

        $user = [];

        if ($identifier) {

            $user = User::select('id', 'profile_photo_path', 'nickname', 'email', 'identifier', 'oauth_service')
                ->with('parameters',)
                ->where('identifier', $identifier ?? \Auth::user()->identifier)
                ->first();

            $user->setAppends(['profile_photo_url', 'detail_url']);

        } else if (\Auth::check()) {

            $user = User::select('id', 'name', 'profile_photo_path', 'nickname', 'surname', 'email', 'identifier', 'oauth_service')
                ->with('parameters:name,type,id',)
                ->where('identifier', \Auth::user()->identifier)
                ->first();

            $user->setAppends(['profile_photo_url', 'detail_url']);
        }

        return response()->json($user);
    }

    /**
     * @param UpdateSettingsRequest $request
     * @return JsonResponse
     */
    public function update(UpdateSettingsRequest $request): JsonResponse {

        $updateSettings = new UpdateSettings($request->all());
        $userResults    = $updateSettings->do();

        return $userResults;
    }

    /**
     * @param string $identifier
     * @return JsonResponse
     */
    public function siteCount(string $identifier): JsonResponse {
        $user      = User::select('id')->where('identifier', $identifier)->first();
        $siteCount = DB::table('users_sites')
            ->select('site_id')
            ->where('user_id', $user->id)
            ->where('accepted', '=', 1)
            ->count();

        return response()->json($siteCount);
    }
}
