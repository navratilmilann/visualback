<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Contact\StoreContact;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StoreContact as StoreContactRequest;

class ContactController extends Controller {

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(): View {
        return view('contact.index');
    }

    /**
     * @param StoreContactRequest $request
     * @return JsonResponse
     */
    public function store(StoreContactRequest $request): JsonResponse {

        $storeContact = new StoreContact($request->all());
        $contact      = $storeContact->do();

        return $contact;
    }
}
