<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class PostController extends Controller {
    /**
     * @return View
     */
    public function index(): View {
        return view('post.index');
    }

    /**
     * @param $slug
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function detail($slug, $id) {
        $post = Post::select('slug', 'id', 'attributes')
            ->where('slug', $slug)
            ->where('id', $id)->first();

        if (empty($post)) {
            return redirect()->to('/post/not-found');
        }

        $post->attributes = ['views' => $post->attributes['views'] + 1];
        $post->save();

        return view('post.detail');
    }

    /**
     * @param null $tag_slug
     * @return JsonResponse
     */
    public function getPosts($tag_slug = null): JsonResponse {
        if ($tag_slug) {
            $posts = Post::select('user_id', 'excerpt', 'text', 'title', 'slug', 'rating', 'attributes', 'id', 'created_at')
                ->whereHas('tags', function ($query) use ($tag_slug) {
                    $query->where('slug', $tag_slug);
                });
        } else {
            $posts = Post::select('user_id', 'excerpt', 'text', 'title', 'slug', 'rating', 'attributes', 'id', 'created_at');
        }

        $posts = $posts
            ->with('user:id,nickname,identifier,profile_photo_path', 'tags:description,name,slug')
            ->withCount('comments')
            ->get();

        return response()->json($posts);
    }

    /**
     * @param string $slug
     * @param int $id
     * @return JsonResponse
     */
    public function getPost(string $slug, int $id): JsonResponse {
        $post = Post::select('user_id', 'text', 'title', 'slug', 'rating', 'attributes', 'id', 'created_at')
            ->where('slug', $slug)
            ->where('id', $id)
            ->with('user:id,nickname,identifier,profile_photo_path')
            ->with('tags:description,name,slug')
            ->first();

        return response()->json($post);
    }

    /**
     * @param string $slug
     * @param int $id
     * @param int $count
     * @return JsonResponse
     */
    public function getRelatedPosts(string $slug, int $id, int $count = 1): JsonResponse {
        $posts = Post::select('user_id', 'excerpt', 'text', 'title', 'slug', 'rating', 'attributes', 'id', 'created_at')
            ->with('tags:description,name,slug')
            ->withCount('comments')
            ->orderBy('created_at', 'DESC')
            ->where('id', '<>', $id)
            ->skip(0)
            ->take($count)
            ->get();

        return response()->json($posts);
    }

    /**
     * @return JsonResponse
     */
    public function getPopularTags() {
        $tags = Parameter::whereIn('id', [48, 49, 50, 51])
            ->get();

        return response()->json($tags);
    }
}
