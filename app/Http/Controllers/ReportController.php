<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReportEntity;
use App\Services\ReportModel;
use Illuminate\Http\Request;

class ReportController extends Controller {

    /**
     * @param $type
     * @param $id
     * @param ReportEntity $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportModel($type, $id, ReportEntity $request) {

        $reportModel = new ReportModel($id, $type, $request->message, $request->user_id);

        $reportResponse = $reportModel->do();

        return $reportResponse;
    }
}
