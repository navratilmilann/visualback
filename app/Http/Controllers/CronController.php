<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Services\Cron\DeleteUnregisteredSites;
use App\Services\Cron\DeleteUnusedAttachments;
use Illuminate\Http\JsonResponse;

class CronController extends Controller {

    /**
     * @return JsonResponse
     */
    public function deleteUnusedAttachments(): JsonResponse {

        $deleteUnusedAttachments = new DeleteUnusedAttachments();

        $response = $deleteUnusedAttachments->do();

        return $response;
    }

    /**
     * @return JsonResponse
     */
    public function deleteUnregisteredSites() {

        $deleteUnregisteredSites = new DeleteUnregisteredSites();

        $response = $deleteUnregisteredSites->do();

        return $response;
    }
}
