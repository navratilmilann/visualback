<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\SiteComment;
use App\Services\Site\DeleteAttachment;
use App\Services\Site\DeleteSiteComment;
use App\Http\Requests\DeleteSiteComment as DeleteSiteCommentRequest;
use App\Services\Site\StoreSiteComment;
use App\Http\Requests\StoreCommentRating as StoreCommentRatingRequest;
use App\Services\Site\StoreCommentRating;
use App\Services\Site\UpdateSiteComment;
use App\Http\Requests\UpdateSiteComment as UpdateSiteCommentRequest;
use App\Services\Site\StoreAttachment;
use App\Http\Requests\StoreAttachment as StoreAttachmentRequest;
use App\Services\Site\StoreScreenshot;
use App\Http\Requests\StoreSiteComment as StoreSiteCommentRequest;
use App\Http\Requests\DeleteAttachment as DeleteAttachmentRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends Controller {

    /**
     * @return array
     */
    private function getSiteCommentQuery(): array {

        $user = auth()->user();

        $data = [
            'attachment'   => function ($query) {
                $query->select('id', 'path', 'attachmentable_id', 'attachmentable_type', 'type');
            },
            'replies',
            'replies.user' => function ($query) {
                $query->select('id', 'name', 'nickname', 'profile_photo_path', 'identifier');
            },
            'user'         => function ($query) {
                $query->select('id', 'name', 'nickname', 'profile_photo_path', 'identifier');
            },
        ];;

        if ($user) {
            $authData = [
                'comment_rating'         => function ($query) use ($user) {
                    $query->select('comment_id', 'value')->where('evaluater_id', $user->id);
                },
                'replies.comment_rating' => function ($query) use ($user) {
                    $query->select('comment_id', 'value')->where('evaluater_id', $user->id);
                },
            ];
            $data     = array_merge($data, $authData);
        }

        return $data;

    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getComments(int $id): JsonResponse {

        $data = SiteComment::where('site_id', $id)
            ->select('id', 'text', 'user_id', 'created_at', 'rating', 'uri', 'device', 'target', 'x', 'y', 'count', 'nickname', 'banned', 'width', 'relative', 'session_id')
            ->with(self::getSiteCommentQuery())
            ->where('parent_id', null)
            ->orderBy('count', 'DESC')
            ->get();

        return response()->json($data);
    }


    /**
     * @param StoreCommentRatingRequest $request
     * @return JsonResponse
     */
    public function storeCommentRating(StoreCommentRatingRequest $request): JsonResponse {

        $storeCommentRating = new StoreCommentRating($request->all());
        $commentResults     = $storeCommentRating->do();

        return $commentResults;
    }

    /**
     * @param StoreSiteCommentRequest $request
     * @return JsonResponse
     */
    public function storeSiteComment(StoreSiteCommentRequest $request): JsonResponse {

        $storeSiteComment   = new StoreSiteComment($request->all());
        $siteCommentResults = $storeSiteComment->do();

        return $siteCommentResults;
    }

    /**
     * @param int $id
     * @param DeleteSiteCommentRequest $request
     * @return JsonResponse
     */
    public function deleteSiteComment(int $id, DeleteSiteCommentRequest $request): JsonResponse {

        $deleteSiteComment = new DeleteSiteComment($id);
        $commentResults    = $deleteSiteComment->do();

        return $commentResults;
    }

    /**
     * @param int $id
     * @param UpdateSiteCommentRequest $request
     * @return JsonResponse
     */
    public function updateSiteComment(int $id, UpdateSiteCommentRequest $request): JsonResponse {

        $updateComment  = new UpdateSiteComment($request->all());
        $commentResults = $updateComment->do();

        return $commentResults;
    }

    /**
     * @param int $id
     * @param StoreAttachmentRequest $request
     * @return JsonResponse
     */
    public function storeAttachment(int $id, StoreAttachmentRequest $request): JsonResponse {

        $storeAttachment = new StoreAttachment($request->all());
        $attachment      = $storeAttachment->do();

        return $attachment;
    }

    /**
     * @param string $identifier
     * @param int $attachmentId
     * @param DeleteAttachmentRequest $request
     * @return JsonResponse
     */
    public function deleteAttachment(string $identifier, int $attachmentId, DeleteAttachmentRequest $request): JsonResponse {

        $deleteAttachment = new DeleteAttachment($attachmentId);
        $attachment       = $deleteAttachment->do();

        return $attachment;
    }

    /**
     * @param int $id
     * @param int $attachmentId
     * @param Request $request
     * @return JsonResponse
     */
    public function getAttachment(int $id, int $attachmentId, Request $request): JsonResponse {

        $attachment = Attachment::find($attachmentId);
        $file       = null;

        if ($attachment) {

            $path = public_path("/{$attachment->path}");
            $file = file_get_contents($path);
        }

        return response()->json([
            'file' => $file
        ]);
    }

    /**
     * @param string $siteIdentifier
     * @return JsonResponse
     * @throws \Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot
     */
    public function storeScreenshot(string $siteIdentifier): JsonResponse {

        $storeScreenshot = new StoreScreenshot($siteIdentifier);
        $screenshot      = $storeScreenshot->do();

        return $screenshot;
    }
}
