<?php

function hash_email($email) {
    return hash('sha256', $email);
}

/**
 * @param $filePath
 * @return mixed
 */
function make_directory_if_not_exist($filePath): string {

    if (!file_exists(dirname($filePath)))
        mkdir(dirname($filePath), 0777, true);

    return $filePath;
}

/**
 * @param $id
 * @param $uri
 * @return string
 */
function get_app_url($id, $uri) {
    return sprintf(config('constants.livepreview_base_url') . $uri, $id);
}

/**
 * @param $id
 * @param $type
 * @return string
 */
function get_report_in_admin($id, $type) {

    if ($type == 'comment') {
        $type = 'site-comments';
    } elseif ($type == 'project') {
        $type = 'sites';
    }

    return config('constants.admin_detail') . $type . '/' . $id;
}

/**
 * @return mixed
 */
function get_user_ip() {
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR']    = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}
