<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAccountOwner {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        abort_if($request->user_id && \Auth::user()->identifier != $request->user_id, 403);

        return $next($request);
    }
}
