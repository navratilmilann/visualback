<?php

namespace App\Http\Middleware;

use App\Models\Site;
use Closure;
use Illuminate\Http\Request;

class IsSiteAuthor {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $site = Site::where('identifier', $request->identifier)->first();

//        abort_if($site->user_id != \Auth::user()->id, 403);

        abort_if($site->users('id', \Auth::user()->id)->count() < 1, 403);

        return $next($request);
    }
}
