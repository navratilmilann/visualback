<?php

namespace App\Http\Middleware;

use App\Models\SiteComment;
use Closure;
use Illuminate\Http\Request;

class IsCommentAuthor {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $comment = SiteComment::findOrFail($request->id);

        abort_if(!$comment->visitor_is_author, 403);

        return $next($request);
    }
}
