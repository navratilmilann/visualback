<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IsUserVerified {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {

        $column = $request->id ? 'id' : 'identifier';

        $user = User::select('email_verified_at')->where($column, $request->{$column})->first();

        if (empty($user) || !empty($user->email_verified_at)) {

            Session::put('info', __('flash-messages.account_is_verified'));

            return redirect()->route('home');
        }

        return $next($request);
    }
}
