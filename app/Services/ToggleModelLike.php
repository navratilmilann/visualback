<?php

namespace App\Services;

use App\Models\Site;
use App\Models\SiteComment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class ToggleModelLike {

    private $data, $entity;

    /**
     * UpdateNotification constructor.
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * @return false|string
     */
    private function getProjectRating() {

        $rating = $this->entity->rating;

        if (\Auth::user()->hasLiked($this->entity)) {
            $rating->{1}   = $rating->{1} + 1;
            $rating->count = $rating->count + 1;
        } else {
            $rating->{1}   = $rating->{1} - 1;
            $rating->count = $rating->count - 1;
        }

        return $rating;
    }

    /**
     * @return mixed
     */
    private function getCommentRating() {

        $rating = $this->entity->rating;

        if (\Auth::user()->hasLiked($this->entity)) {
            $rating->{1}   = $rating->{1} + 1;
            $rating->count = $rating->count + 1;
        } else {
            $rating->{1}   = ($rating->{1} - 1) >= 0 ? $rating->{1} - 1 : 0;
            $rating->count = ($rating->count - 1) >= 0 ? $rating->count - 1 : 0;
        }

        return $rating;
    }

    private function setModelEntity(): void {

        if ($this->data['type'] == 'project') {
            $this->entity = Site::where('id', $this->data['id'])->first();
        } elseif ($this->data['type'] == 'comment') {
            $this->entity = SiteComment::where('id', $this->data['id'])->first();
        }
    }

    private function updateSummary(): void {

        if ($this->data['type'] == 'project') {

            $rating = self::getProjectRating();

            $this->entity->rating = ($rating);
            $this->entity->save();

        } elseif ($this->data['type'] == 'comment') {

            $rating = self::getCommentRating();

            $this->entity->rating = ($rating);

            $this->entity->save();
        }
    }

    private function toggleLike(): void {

        if ($this->entity) {
            \Auth::user()->toggleLike($this->entity);
        }
    }

    public function do(): JsonResponse {

        try {

            self::setModelEntity();

            self::toggleLike();

            self::updateSummary();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.toggle_like')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
