<?php

namespace App\Services;

use App\Models\Newsletter;
use App\Models\User;
use App\Traits\NewsletterOperations;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class StoreNewsletter {

    use NewsletterOperations;

    private $data, $user;

    /**
     * StoreNewsletter constructor.
     * @param array $data
     * @param User|null $user
     */
    public function __construct(array $data, User $user = null) {
        $this->data = $data;
        $this->user = $user;
    }

    private function emailIsUnique() {
        $activated_newsletter = $this->userActivatedNewsletter($this->data['email']);

        if (!empty($activated_newsletter)) {
            throw new Exception(__('flash-messages.existing_newsletter_email'));
        }
    }

    public function do(): JsonResponse {

        try {

            self::emailIsUnique();

            $this->storeNewsletter($this->data['email'], optional($this->user)->id);

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.stored_newsletter')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
