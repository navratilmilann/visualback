<?php

namespace App\Services;

use App\Models\Notification;
use Mockery\Exception;

class StoreNotification {

    private const SITE_INVITATION = 'site-invitation';
    private const USER_FOLLOW     = 'user-follow';
    private const NEW_SITE_FOLLOW = 'new-site-follow';

    private $type, $attributes, $value, $type_id;

    /**
     * UpdateNotification constructor.
     * @param string $type
     * @param array $attributes
     */
    public function __construct(string $type, array $attributes) {
        $this->type       = $type;
        $this->attributes = $attributes;
    }

    private function setSiteCollaboratorsValue() {

        $author = $this->attributes['site']->users()->first();

        $this->value = json_encode([
            'site' => [
                'name'       => $this->attributes['site']->name,
                'identifier' => $this->attributes['site']->identifier,
                'slug'       => $this->attributes['site']->slug
            ],      // who registered site
            'user' => [
                'identifier'        => $author->identifier,
                'nickname'          => $author->nickname,
                'profile_photo_url' => $author->profile_photo_url
            ],
        ]);
    }

    private function setUserFollow() {

        $this->value = json_encode([
            'user' => [     // follower name
                'identifier' => $this->attributes['user']->identifier,
                'nickname'   => $this->attributes['user']->nickname,
            ],
        ]);
    }

    private function setNewSiteFollow() {

        $this->value = json_encode([
            'site' => [
                'name'       => $this->attributes['site']->name,
                'identifier' => $this->attributes['site']->identifier,
                'slug'       => $this->attributes['site']->slug
            ],
            'user' => [     // who registered site
                'identifier' => $this->attributes['author']->identifier,
                'nickname'   => $this->attributes['author']->nickname
            ],
        ]);
    }

    private function setNotification() {

        if ($this->type === self::SITE_INVITATION) {

            $this->type_id = 1;

            self::setSiteCollaboratorsValue();

        } elseif ($this->type === self::USER_FOLLOW) {

            $this->type_id = 2;

            self::setUserFollow();

        } elseif ($this->type === self::NEW_SITE_FOLLOW) {

            $this->type_id = 3;

            self::setNewSiteFollow();
        }
    }

    private function store() {

        $data = [];

        self::setNotification();

        if (!empty($this->type_id)) {

            foreach ($this->attributes['users'] as $user) {
                $data[] = [
                    'user_id'              => $user->id,
                    'value'                => $this->value,
                    'notification_type_id' => $this->type_id,
                    'created_at'           => now()
                ];
            }

            Notification::insert($data);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse|null
     */
    private function getResponse() {

        if ($this->type === self::SITE_INVITATION) {
            return response()->json([
                'error'   => 0,
                'message' => 'You successfully accepted the collaboration on the project.'
            ]);
        }

        return null;
    }

    /**
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function do() {

        try {

            self::store();

            return self::getResponse();

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
