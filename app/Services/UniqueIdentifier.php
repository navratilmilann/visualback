<?php

namespace App\Services;

use App\Models\Site;
use App\Models\User;

class UniqueIdentifier {

    public const TYPE_SITE    = 'site';
    public const TYPE_COMMENT = 'comment';
    public const TYPE_USER    = 'user';

    private function anyExist(string $modelType, string $randomString) {

        if ($modelType == UniqueIdentifier::TYPE_SITE)
            return Site::where('identifier', $randomString)->exists();
        else if ($modelType == UniqueIdentifier::TYPE_USER)
            return User::where('identifier', $randomString)->exists();

        return null;
    }

    private function getLength(string $modelType) {
        if ($modelType == UniqueIdentifier::TYPE_SITE)
            return 8;
        else if ($modelType == UniqueIdentifier::TYPE_USER)
            return 10;
    }

    public function getUniqueIdentifier(string $modelType): string {

        $characters       = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString     = '';
        $length           = self::getLength($modelType);
        // hundred attempts for finding unique identifier
        for ($attempt = 1; $attempt <= 1000; $attempt++) {

            for ($i = 0; $i < $length; $i++) {
                if ($i != ((int)$length / 2)) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                } else {
                    $randomString .= '-';
                }
            }
            // check, if identifier is unique
            if (!self::anyExist($modelType, $randomString))
                break;

            $attempt++;

        }

        return $randomString;
    }
}