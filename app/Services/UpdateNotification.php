<?php

namespace App\Services;

use App\Models\Notification;
use App\Models\Site;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class UpdateNotification {

    private $data;

    /**
     * UpdateNotification constructor.
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * @param $site
     */
    private function checkIfSiteExist($site) {
        if (empty($site)) {
            Notification::where('id', $this->data['notification_id'])->delete();

            throw new Exception(__('flash-messages.project_already_removed'));
        }
    }

    private function updateNotification() {

        if ($this->data['type'] === 'site-invitation') {

            $site = Site::where('identifier', $this->data['site_identifier'])->first();

            self::checkIfSiteExist($site);

            if ($this->data['status'] === 'accepted') {
                \Auth::user()->sites()->updateExistingPivot($site, ['accepted' => 1], false);
            }

            Notification::where('id', $this->data['notification_id'])->delete();
        }
    }

    public function do(): JsonResponse {

        try {

            self::updateNotification();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.updated_notification')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
