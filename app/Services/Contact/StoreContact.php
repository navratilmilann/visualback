<?php

namespace App\Services\Contact;

use App\Mail\ContactForm;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;

class StoreContact {

    private $data;

    /**
     * Submit constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     *
     */
    private function send(): void {

        Mail::to($this->data['email'])->send(new ContactForm($this->data));

        Mail::to(config('constants.email'))->send(new ContactForm($this->data));
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            self::send();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.sent_contact_email')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
