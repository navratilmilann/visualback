<?php

namespace App\Services\Site;

use App\Models\Site;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class AuthorizeSite {

    private $identifier, $private, $site, $dom;

    private const META_NAME = 'visualback-id';

    /**
     * AuthorizeSite constructor.
     * @param string $identifier
     * @param bool $private
     */
    public function __construct(string $identifier, bool $private) {
        $this->identifier = $identifier;
        $this->private    = $private;
        $this->dom        = new \DOMDocument('1.0', 'utf-8');
    }

    private function getSite(): void {
        $this->site = Site::where('identifier', $this->identifier)->first();
    }

    /**
     * @return mixed
     */
    private function findMetatag() {

        $context = stream_context_create([
            'ssl' => [
                'verify_peer'      => false,
                'verify_peer_name' => false,
            ],
        ]);

        $html = file_get_contents($this->site->url, false, $context);

        $dom = new \DOMDocument;
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();

        foreach ($dom->getElementsByTagName('meta') as $tag) {
            if ($tag->getAttribute('name') == AuthorizeSite::META_NAME) {
                return $tag->getAttribute('content');
            }
        }

        throw new Exception('Project identifier not found.');
    }

    private function verifySite(string $metaTagIdentifier) {

        if ($metaTagIdentifier != $this->site->identifier) {
            throw new Exception('Project identifier mismatched.');
        }
    }

    private function activateSite() {

        $this->site->update([
            'private'  => $this->private,
            'verified' => 1,
            'active'   => 1
        ]);
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            self::getSite();


            $metaTagIdentifier = self::findMetatag();

            self::verifySite($metaTagIdentifier);

            self::activateSite();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.authorized_site')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
