<?php

namespace App\Services\Site;

use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class StoreAttachment {

    private $data, $attachment_path, $path, $entity_id;

    /**
     * StoreAttachment constructor.
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data            = $data;
        $this->attachment_path = public_path('/storage/');
        $this->entity_id       = $data['entity_id'] ?? null;
    }

    /**
     * @param int $length
     * @return string
     */
    private function generateRandomString(int $length = 10): string {

        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @return string
     */
    private function getFilename(): string {
        return self::generateRandomString(20) . '.css';
    }

    /**
     * @param $path
     */
    private function makeDirectoryIfNotExist(string $path) {

        if (!file_exists(dirname($path)))
            mkdir(dirname($path), 0777, true);
    }

    /**
     * @param $file
     */
    private function storeFileOnDisk($file): void {

        $path = $file->store('attachment', 'public');

        $this->path = '/storage/' . $path;
    }

    private function storeCSSOnDisk(): void {

        $this->filename = self::getFilename();
        $path           = $this->attachment_path . $this->data['type'] . '/' . $this->filename;

        self::makeDirectoryIfNotExist($path);

//        $this->data['data'] = htmlspecialchars($this->data['data'], ENT_QUOTES, 'UTF-8');
        $this->data['data'] = strip_tags($this->data['data']);

        file_put_contents($path, $this->data['data']);

        $this->path = '/storage/' . $this->data['type'] . '/' . $this->filename;
    }

    /**
     * @return Attachment
     */
    private function storeFileToDB(): Attachment {

        return Attachment::create([
            'path'                => $this->path,
            'type'                => $this->data['type'],
            'attachmentable_type' => 'App\Models\SiteComment',
            'attachmentable_id'   => $this->entity_id
        ]);
    }

    /**
     * @return Attachment
     */
    private function storeCSS(): Attachment {

        self::storeCSSOnDisk();

        return self::storeFileToDB();
    }

    /**
     * @return array
     */
    private function storeFiles(): array {

        if (is_array($this->data['data'])) {

            foreach ($this->data['data'] as $file) {

                self::storeFileOnDisk($file);

                $file = self::storeFileToDB();

                $file_urls[] = [
                    'url'  => $file->url,
                    'id'   => $file->id,
                    'type' => $this->data['type']
                ];
            }

        } else {

            self::storeFileOnDisk($this->data['data']);

            $file = self::storeFileToDB();

            $file_urls[] = [
                'url'  => $file->url,
                'id'   => $file->id,
                'type' => $this->data['type']
            ];
        }

        return $file_urls;
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            if ($this->data['type'] === 'css') {

                $css = self::storeCSS();

                return response()->json([
                    'error' => 0,
                    'id'    => $css->id,
                    'url'   => $css->url
                ]);

            } else {

                $data = self::storeFiles();

                return response()->json([
                    'error' => 0,
                    'data'  => $data
                ]);
            }

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
