<?php

namespace App\Services\Site;

use App\Models\Site;
use App\Models\SiteRating;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class StoreSiteRating {

    private $data;

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * @return array
     */
    private function getSiteData(): array {

        $site                     = Site::select('id', 'rating')
            ->where('id', $this->data['site_id'])
            ->first();
        $sentRating               = $this->data['rating'];
        $rating                   = $site->rating;
        $rating[0]->{$sentRating} = $rating[0]->{$sentRating} + 1;
        $rating[1]->count         = $rating[1]->count + 1;
        $rating[1]->sum           = $rating[1]->sum + $sentRating;
        $rating[1]->average       = round($rating[1]->sum / $rating[1]->count, 1);

        return [
            'rating' => $rating
        ];
    }

    /**
     * @return array
     */
    private function getSiteRatingData(): array {

        $type = $this->data['type'] ?? 'total';
        $text = $this->data['text'] ?? null;

        return [
            'site_id' => $this->data['site_id'],
            'user_id' => $this->data['user_id'],
            'type'    => $type,
            'value'   => $this->data['rating'],
            'text'    => $text
        ];
    }

    private function updateSite(): string {

        $data = self::getSiteData();

        Site::where('id', $this->data['site_id'])
            ->update($data);

        return $data['rating'];
    }

    /**
     * @return mixed
     */
    private function storeSiteRating(): SiteRating {
        return SiteRating::create(self::getSiteRatingData());
    }

    public function do(): JsonResponse {

        try {

            self::storeSiteRating();

            $rating = self::updateSite();

            return response()->json([
                'error'   => 0,
                'rating'  => $rating,
                'message' => __('flash-messages.stored_site_rating')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
