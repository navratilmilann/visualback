<?php

namespace App\Services\Site;

use App\Models\Site;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;

class StoreScreenshot {

    private $identifier, $site;

    /**
     * StoreScreenshot constructor.
     * @param string $siteIdentifier
     */
    public function __construct(string $siteIdentifier) {
        $this->identifier = $siteIdentifier;
        $this->site       = self::getSite();
    }

    /**
     * @return Site
     */
    private function getSite(): Site {
        return Site::where('identifier', $this->identifier)->where('verified', 1)->first();
    }

    /**
     * @throws \Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot
     */
    private function storeImage(): void {

        Browsershot::url($this->site->base_url)
            ->ignoreHttpsErrors()
            ->setScreenshotType('jpeg', 95)
            ->optimize()
            ->fit(Manipulations::FIT_CONTAIN, 350, 350)
            ->save(public_path('/storage/screenshot/' . $this->site->identifier . '.jpg'));
    }

    /**
     * @param string $path
     */
    private function storeAttachment(string $path): void {

        $this->site->attachment()->create([
            'path'   => $path,
            'active' => 1,
            'type'   => 'screenshot'
        ])->save();
    }

    /**
     * @return string
     * @throws \Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot
     */
    private function storeScreenshot(): string {

        $path = '/storage/screenshot/' . $this->site->identifier . '.jpg';

        self::storeImage();

        self::storeAttachment($path);

        return $path;
    }

    /**
     * @return JsonResponse
     * @throws \Spatie\Browsershot\Exceptions\CouldNotTakeBrowsershot
     */
    public function do(): JsonResponse {

        try {

            $path = self::storeScreenshot();

            return response()->json([
                'data'  => $path,
                'error' => 0,
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
