<?php

namespace App\Services\Site;

use App\Models\Site;
use App\Models\SiteComment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class UpdateSiteComment {

    private $data;

    /**
     * UpdateSiteComment constructor.
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    private function updateComment(): void {

        $comment = SiteComment::find($this->data['id']);

        $comment->update([
            'text' => strip_tags($this->data['text'])
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function do(): JsonResponse {

        try {

            self::updateComment();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.succesfully_updated_comment')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
