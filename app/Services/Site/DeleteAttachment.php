<?php

namespace App\Services\Site;

use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class DeleteAttachment {

    private $attachment_id;

    /**
     * DeleteAttachment constructor.
     * @param int $data
     */
    public function __construct(int $data) {
        $this->attachment_id = $data;
    }

    /**
     * @return Attachment
     */
    private function getAttachment(): Attachment {
        return Attachment::where('id', $this->attachment_id)->first();
    }

    private function deleteAttachment(): void {

        $attachment = self::getAttachment();

        $attachment->delete();
    }

    public function do(): JsonResponse {

        try {

            self::deleteAttachment();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.deleted_attachment')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
