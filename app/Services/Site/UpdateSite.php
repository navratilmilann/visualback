<?php

namespace App\Services\Site;

use App\Models\Site;
use App\Traits\ParameterOperations;
use Mockery\Exception;

class UpdateSite {

    use ParameterOperations;

    private $data, $site;

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * @return false|string[]
     */
    private function getParameterNames() {

        $parameters     = $this->data['parameters'];
        $parameterNames = explode(',', $parameters['technology']);

//        $parameterNames[] = $parameters['site-type'];

        return $parameterNames;
    }

    /**
     * Update site in sites
     */
    private function updateSite() {

        $this->site = Site::select('id', 'name')->with('parameters')->where('id', $this->data['id'])->first();

        $insertData['name']    = !empty($this->data['name']) ? $this->data['name'] : $this->site->name;
        $insertData['private'] = !empty($this->data['private']);
        $insertData['slug']    = \Str::slug($insertData['name'], '-');
        $insertData['user_id'] = \Auth::check() ? \Auth::user()->id : null;
        $insertData['active']  = \Auth::check() ? 1 : 0;

        return $this->site->update($insertData);
    }

    /**
     * Connect project with parameters
     */
    private function storeParameters(): void {

        $parameterIds = $this->getParametersIdByAttribute(self::getParameterNames());

//        dd($this->site->parameters()->whereIn('type', ['technology', 'site-type'])->decrement('count'));
        $this->updateParameterByType($this->site, $parameterIds, ['technology', 'site-type']);

//        $this->updateParameterValueByIds($this->site, [24 => $this->data['parameters']['source-code']]);
    }

    /**
     * Public function for communicating with controller
     */
    public function do() {

        try {

            self::updateSite();

            self::storeParameters();

            return $this->site;

        } catch (Exception $exception) {

            session()->put('error', $exception->getMessage());

            return redirect()
                ->back()
                ->send();
        }
    }

}
