<?php

namespace App\Services\Site;

use App\Models\Site;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class DeleteSite {

    private $data;

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    private function deleteSite(): void {

        $site = Site::where('identifier', $this->data['identifier'])->first();

        $site->users()->detach();

        $site->parameters()->detach();

        $site->delete();
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            self::deleteSite();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.deleted_site')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
