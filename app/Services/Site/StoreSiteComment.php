<?php

namespace App\Services\Site;

use App\Jobs\SendNewCommentEmail;
use App\Models\Attachment;
use App\Models\SiteComment;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class StoreSiteComment {

    private $data;

    private const UNREGISTERED_USER_COMMENTS = 3;       // allowed count of comments in hour

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data = $data;
    }

    private function checkCountOfCommentsInHour() {

        if (empty($this->data['user_identifier'])) {

            $commentsInHour = SiteComment::where('ip', $this->data['ip'])
                ->where('created_at', '>=', Carbon::now()->subHour())
                ->count();

            if ($commentsInHour >= self::UNREGISTERED_USER_COMMENTS) {
                throw new Exception('Please, register account for more than 3 comments per hour.');
            }
        }
    }

    private function setUserId() {
        if (!empty($this->data['user_identifier'])) {
            $this->data['user_id'] = \Auth::id();
        } else {
            $this->data['session_id'] = session()->getId();
        }
    }

    private function cleanText() {
        $this->data['text'] = strip_tags($this->data['text']);
    }

    /**
     * @return mixed
     */
    private function storeSiteComment(): SiteComment {

        self::setUserId();

        self::cleanText();

        $siteComment = SiteComment::create($this->data);

        return SiteComment::select('id', 'created_at', 'nickname', 'user_id', 'parent_id', 'text', 'rating', 'uri', 'device', 'target', 'count', 'x', 'y', 'relative', 'width', 'session_id')
            ->where('id', $siteComment->id)
            ->with(
                'comment_rating:comment_id,value',
                'replies:id,created_at,user_id,parent_id,text,rating,uri,device,count'
            )->first();
    }

    /**
     * @param SiteComment $siteComment
     */
    private function activateCSS(SiteComment $siteComment): void {

        if (!empty($this->data['attachments']) && count($this->data['attachments']) > 0) {

            $attachmentIdies = $this->data['attachments'];

            Attachment::whereIn('id', $attachmentIdies)->update([
                'active'            => 1,
                'attachmentable_id' => $siteComment->id
            ]);
        }
    }

    /**
     * @param $comment
     */
    private function sendNotification($comment) {

        if (empty($this->data['parent_id'])) {
            $comment = SiteComment::select('user_id', 'id', 'site_id')
                ->where('id', $comment->id)
                ->with('user:id,nickname,identifier', 'site:id,name,slug,identifier', 'site.users:email,nickname')
                ->first();

            foreach ($comment->site->users as $user) {                                                                  // send notifications about new comment
                dispatch(new SendNewCommentEmail($comment, $user));
            }
        }
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            $this->data['ip'] = get_user_ip();

            self::checkCountOfCommentsInHour(); // if not registered

            $siteComment = self::storeSiteComment();

            self::activateCSS($siteComment);

            self::sendNotification($siteComment);

            $siteComment->load('attachment', 'user:id,identifier,nickname,profile_photo_path');

            return response()->json([
                'error'        => 0,
                'site_comment' => $siteComment,
                'message'      => __('flash-messages.stored_site_comment')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
