<?php

namespace App\Services\Site;

use App\Events\NoticeSiteCollaborators;
use App\Models\Site;
use App\Models\User;
use App\Services\StoreNotification;
use App\Services\UniqueIdentifier;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class SetSiteCollaborators {

    private const SITE_INVITATION = 'site-invitation';

    private $data, $users, $site;

    /**
     * SetSiteMembers constructor.
     * @param array $data
     */
    public function __construct(array $data) {
        $this->data = $data;
        $this->site = self::getSite();
    }

    /**
     * @param $site
     * @return false|string
     */
    private function getNotification($site) {
        return json_encode([
            'site' => [
                'name' => $site->name,
                'id'   => $site->id,
                'slug' => $site->slug
            ],
            'user' => [
                'id'       => $site->users->first()->id,
                'nickname' => $site->users->first()->nickname
            ],
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    private function getSite() {
        return Site::with('users:id,nickname')->where('id', $this->data['site_id'])->first();
    }

    private function checkIfEmailsIsEmpty() {

        if (empty($this->data['emails'])) {
            throw new Exception(__('flash-messages.Email input is empty'));
        }
    }

    /**
     * @return array
     */
    private function clearEmail() {

        if (($key = array_search(\Auth::user()->email, $this->data['emails'])) !== false) {
            unset($this->data['emails'][$key]);
        }

        return array_unique($this->data['emails']);
    }

    private function storeUsersByEmails(): void {

        if (!empty($this->data['emails'])) {

            $emails = self::clearEmail();

            foreach ($emails as $email) {

                $hashedEmail = hash_email($email);

                $user = User::firstOrCreate([
                    'hash_email' => $hashedEmail
                ], [
                    'hash_email' => $hashedEmail,
                    'email'      => $email,
                    'nickname'   => explode('@', $email)[0],
                    'identifier' => (new UniqueIdentifier())->getUniqueIdentifier('user')
                ]);

                $this->users[] = $user;
            }
        }
    }

    private function sendCollaboratorMail(): void {

        event(new NoticeSiteCollaborators($this->users, $this->data['site_id']));
    }

    private function setNotifications() {

        $attributes        = [
            'users' => $this->users,
            'site'  => $this->site
        ];
        $storeNotification = new StoreNotification(self::SITE_INVITATION, $attributes);
        $storeNotification->do();
    }

    public function do(): JsonResponse {

        try {

//            self::checkIfEmailsIsEmpty();

            self::storeUsersByEmails();

            self::setNotifications();

            self::sendCollaboratorMail();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.set_site_members')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
