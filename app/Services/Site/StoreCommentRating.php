<?php

namespace App\Services\Site;

use App\Models\CommentRating;
use App\Models\SiteComment;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class StoreCommentRating {

    private $data, $site_comment, $previous_comment_rating;

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data                    = $data;
        $this->site_comment            = self::getSiteComment();
        $this->previous_comment_rating = self::getPreviousCommentRating();
    }

    /**
     * @return mixed
     */
    private function getSiteComment() {
        return SiteComment::select('id', 'rating', 'site_id')
            ->where('id', $this->data['comment_id']);
    }

    /**
     * @return mixed
     */
    private function getUserWithRating() {
        return User::where('id', $this->data['addressee_id'])
            ->with(['parameters' => function ($query) {
                $query->where('parameters.id', 12);
            }])->first();
    }

    /**
     * @return mixed
     */
    private function getPreviousCommentRating() {
        return CommentRating::where('evaluater_id', $this->data['evaluater_id'])
            ->where('comment_id', $this->data['comment_id'])->get();
    }

    /**
     * @param $rating
     * @return mixed
     */
    private function removePreviousCommentRating($rating) {

        // delete previous comment rating and update ratings in site_comment
        if ($this->previous_comment_rating->count() > 0) {

            $previousValue            = $this->previous_comment_rating->first()->value;
            $rating->{$previousValue} = $rating->{$previousValue} - 1;                    // subtract previous count of value

            $this->previous_comment_rating->first()->delete();                             // delete previous comment rating of user
        }

        return $rating;
    }

    /**
     * @return array
     */
    private function getCommentRatingData() {

        $rating           = $this->site_comment->first()->rating;
        $value            = $this->data['value'];
        $rating->{$value} = $rating->{$value} + 1;

        $rating = self::removePreviousCommentRating($rating);

        return json_encode($rating);
    }

    /**
     * @return mixed
     */
    private function updateSiteComment() {

        $rating = self::getCommentRatingData();

        $this->site_comment->update(['rating' => $rating]);

        return $rating;
    }

    /**
     * Calculate sum, average and count as summary data about user rating
     * data about likes and dislikes from comments and replies
     * @param $userRating
     * @param $value
     * @return mixed
     */
    private function getAggregationRatingData($userRating, $value): array {

        $sum = $userRating[1]->sum;

        // update rating, if exists previous comment rating
        if ($this->previous_comment_rating->count() > 0) {

            $previousValue                   = $this->previous_comment_rating->first()->value;
            $userRating[0]->{$previousValue} = $userRating[0]->{$previousValue} - 1;

            $userRating[1]->sum = $previousValue                           // if previous value is 1, then need to subtract 1
                ? $sum - 1
                : $sum + 1;

        } else {
            // increment count
            $userRating[1]->count++;
            $userRating[1]->sum = $sum + $value;
        }

        $userRating[1]->average = round($userRating[1]->sum / $userRating[1]->count, 1);

        return $userRating;
    }

    /**
     * @param $userRating
     * @return false|string
     */
    private function getAddresseeRatingData($userRating) {

        $value                   = $this->data['value'];
        $userRating[0]->{$value} = $userRating[0]->{$value} + 1;

        return json_encode(self::getAggregationRatingData($userRating, $value));
    }

    private function updateUserParameter(): void {

        $user   = self::getUserWithRating();
        $rating = json_decode($user->parameters->first()->pivot->value);

        $user->parameters()->updateExistingPivot([12], [
            'value' => self::getAddresseeRatingData($rating)
        ]);
    }

    /**
     * @return mixed
     */
    private function storeCommentRating(): CommentRating {
        return CommentRating::create([
            'comment_id'   => $this->data['comment_id'],
            'addressee_id' => $this->data['addressee_id'],
            'evaluater_id' => $this->data['evaluater_id'],
            'value'        => $this->data['value'],
            'site_id'      => $this->site_comment->first()->site_id
        ]);
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            self::updateUserParameter();

            self::storeCommentRating();

            $commentRating = self::updateSiteComment();

            return response()->json([
                'commentRating' => $commentRating,
                'message'       => __('flash-messages.stored_comment_rating')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
