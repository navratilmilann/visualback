<?php

namespace App\Services\Site;

use App\Models\SiteComment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class DeleteSiteComment {

    private $comment;

    /**
     * DeleteSiteComment constructor.
     * @param int $commentId
     */
    public function __construct(int $commentId) {
        $this->comment = self::getComment($commentId);
    }

    /**
     * @param int $commentId
     * @return SiteComment
     */
    private function getComment(int $commentId): SiteComment {
        return SiteComment::find($commentId);
    }

    private function deleteComment(): void {
        $this->comment->delete();
    }

    /**
     * @return JsonResponse
     */
    public function do(): JsonResponse {

        try {

            self::deleteComment();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.succesfully_deleted_comment')
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
