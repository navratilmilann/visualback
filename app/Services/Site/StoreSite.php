<?php

namespace App\Services\Site;

use App\Models\Attachment;
use App\Models\Parameter;
use App\Models\Site;
use App\Services\StoreNotification;
use App\Services\UniqueIdentifier;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

/**
 * Class StoreSite
 * @package App\Services\Site
 *
 * Class temporarly creates store site to database
 * Before its activation user has to put metatag into website
 * and website needs to be successfully verified
 */
class StoreSite {

    private const NEW_SITE_FOLLOW = 'new-site-follow';

    private $data, $parsed_url, $base_url, $auth_user, $blacklist, $uri;

    /**
     * Register constructor.
     * @param $data
     */
    public function __construct(array $data) {
        $this->data      = $data;
        $this->auth_user = \Auth::user();
//        $this->blacklist = include dirname(__FILE__) . "/../../../../../blacklist.php";
    }

    /**
     * @param string $url
     * @return bool
     */
    private function urlNotExist(string $url) {

        $ch      = curl_init();
        $options = [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_AUTOREFERER    => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT        => 120,
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_SSL_VERIFYPEER => false
        ];
        curl_setopt_array($ch, $options);
        curl_exec($ch);

        $statusCode  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $redirectUrl = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
        $url         = $redirectUrl ? $redirectUrl : $url;

        $this->parsed_url = self::parsedUrl($url);
        $this->base_url   = self::getBaseUrl();
        $this->uri        = self::getUri();

        return $statusCode == 0 || strpos($statusCode, '404') !== false;
    }

    /**
     * @return false|string[]
     */
    private function getParameterNames(): array {

        $parameters       = $this->data['parameters'];
        $parameterNames   = explode(',', $parameters['technology']);
        $parameterNames[] = $parameters['type'];

        return $parameterNames;
    }

    /**
     * @param string $url
     * @return array
     */
    private function parsedUrl(string $url): array {

        $url = trim($url, '/');                                                   // in case scheme relative URI is passed, e.g., //www.google.com/

        if (!preg_match('#^http(s)?://#', $url)) {                                  // If scheme not included, prepend it
            $url = 'http://' . $url;
        }

        $this->data['url'] = $url;

        return parse_url($url);
    }

    /**
     * @return string
     */
    private function getDomain(): string {
        return preg_replace('/^www\./', '', $this->parsed_url['host']);
    }

    private function checkRegisteredSite() {
        // ping input url
        // in nested method set $this->data['url'] on url from request - possible redirected url
        if (self::urlNotExist($this->data['url'])) {
            throw new Exception('Registered project does not exist.');
        }
        // on blacklist
//        if (in_array($this->base_url, $this->blacklist)) {
//            throw new Exception('Project is not allowed to register.');
//        }

        // todo: get only neccessary columns
        // todo: registration "storeSiteScreenshot" bullshits
        $existingSite = Site::where('base_url', $this->base_url)->where('uri', $this->uri)->first();

        if (!empty($existingSite)) {

            if (!$existingSite->verified) {
                return $existingSite;
            } else {
                throw new Exception('Registered project already exists.');
            }
        }

        return null;
    }

    /**
     * @return string
     */
    private function getName(): string {

        if (!empty($this->data['name'])) {
            return $this->data['name'];
        } else {
            return self::getDomain();
        }
    }

    /**
     * @return string
     */
    private function getBaseUrl(): string {

//        if (strpos($this->parsed_url['host'], '.github.io')) {
//            $uri = explode('/', $this->parsed_url['path']);
//
//            return $this->parsed_url['scheme'] . '://' . $this->parsed_url['host'] . "/" . $uri[1];
//        }

        return self::removeLastSlash($this->parsed_url['scheme'] . '://' . $this->parsed_url['host']);
    }

    /**
     * @param $url
     * @return string
     */
    private function removeLastSlash(string $url): string {
        return rtrim($url, "/");
    }

    /**
     * @return string
     */
    private function getUri(): string {
        return (!empty($this->parsed_url['path']) ? $this->parsed_url['path'] : '/') . (!empty($this->parsed_url['query']) ? '?' . $this->parsed_url['query'] : '');
    }

    /**
     * @return string
     */
    private function getSlug(): string {

        $host = $this->parsed_url['host'];
        $path = $this->parsed_url['path'] ?? '';

        $url = str_replace('/', '-', $host . $path);
        $url = preg_replace('/^www\./', '', $url);

        return \Str::slug($url, '-');
    }

    /**
     * Store site in sites
     * @return Site
     */
    private function storeSite(): Site {

        $name = self::getName();

        $this->data['name']       = $name;
        $this->data['uri']        = $this->uri;
        $this->data['slug']       = self::getSlug();
        $this->data['base_url']   = $this->base_url;
        $this->data['active']     = 0;
        $this->data['verified']   = 0;
        $this->data['identifier'] = (new UniqueIdentifier())->getUniqueIdentifier('site');

        $site = Site::create($this->data);

        $site->users()->attach([$this->auth_user->id => ['accepted' => 1]]);

        return Site::find($site->id);
    }

    /**
     * @param $site
     */
    private function storeParameters(Site $site): void {

        if (!empty($this->data['parameters'])) {

            $idQuery = Parameter::whereIn('name', self::getParameterNames());

            $site->parameters()->attach($idQuery->pluck('id'));

            $idQuery->increment('count');
        }
    }

    /**
     * @param Site $site
     */
    private function sendNotification(Site $site): void {

        $attributes = [
            'users'  => $this->auth_user->followers()->select('user_id AS id')->get(['id']),
            'site'   => $site,
            'author' => $this->auth_user
        ];

        $storeNotification = new StoreNotification(self::NEW_SITE_FOLLOW, $attributes);
        $storeNotification->do();
    }

    /**
     * Public function for communicating with controller
     */
    public function do(): JsonResponse {

        try {

            $notVerifiedSite = self::checkRegisteredSite();

            if (empty($notVerifiedSite)) {

                $notVerifiedSite = self::storeSite();

                self::storeParameters($notVerifiedSite);

//            self::sendNotification($site);
            }

            $notVerifiedSite->load('users:id,name,profile_photo_path,identifier', 'technologies', 'comments');

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.stored_site'),
                'site'    => $notVerifiedSite,
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
