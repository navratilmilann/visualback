<?php

namespace App\Services;

use App\Jobs\NewReportEmail;
use App\Models\SiteCommentReport;
use App\Models\SiteReport;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class ReportModel {

    private $type, $id, $message, $user_id;

    private const
        PROJECT_TYPE = 'project',
        COMMENT_TYPE = 'comment';

    /**
     * ReportModel constructor.
     * @param int $id
     * @param string $type
     * @param string $message
     * @param int $user_id
     */
    public function __construct(int $id, string $type, string $message, int $user_id) {
        $this->type    = $type;
        $this->id      = $id;
        $this->message = $message;
        $this->user_id = $user_id;
    }

    private function storeReport(): void {

        if ($this->type === self::PROJECT_TYPE) {
            SiteReport::create([
                'user_id' => $this->user_id,
                'site_id' => $this->id,
                'message' => $this->message
            ]);
        } elseif ($this->type === self::COMMENT_TYPE) {
            SiteCommentReport::create([
                'user_id'    => $this->user_id,
                'comment_id' => $this->id,
                'message'    => $this->message
            ]);
        }
    }

    private function sendEmail(): void {
        dispatch(new NewReportEmail($this->type, $this->id, $this->user_id));
    }

    public function do(): JsonResponse {

        try {

            $type = $this->type;

            self::storeReport();

            self::sendEmail();

            return response()->json([
                'error'   => 0,
                'message' => __("flash-messages.{$type}_reported")
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
