<?php

namespace App\Services\Cron;

use App\Models\Attachment;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

/**
 * Delete attachment which are not activated after 3 days
 * Class DeleteUnusedAttachments
 * @package App\Services\Cron
 */
class DeleteUnusedAttachments {

    private function remove(): void {

        $attachments = Attachment::select('active', 'path')
            ->where('created_at', '<=', now()->subDays(3)->setTime(0, 0, 0)->toDateTimeString())
            ->where('active', 0);

        foreach ($attachments->get() as $attachment) {

            $path = public_path($attachment->path);

            if (file_exists($path)) {
                unlink($path);
            }
        }

        // delete them from DB
        $attachments->delete();
    }

    public function do(): JsonResponse {

        try {

            self::remove();

            return response()->json([
                'error'   => 0,
                'message' => 'Successful cron operation'
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
