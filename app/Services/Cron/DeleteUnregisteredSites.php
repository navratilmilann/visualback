<?php

namespace App\Services\Cron;

use App\Models\Attachment;
use App\Models\Site;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

/**
 * Delete unregistered sites 7 days old
 * Class DeleteUnregisteredSites
 * @package App\Services\Cron
 */
class DeleteUnregisteredSites {

    private function remove(): void {

        Site::select('id')
            ->where('verified', 0)
            ->where('created_at', '<=', now()->subDays(7)->setTime(0, 0, 0)->toDateTimeString())
            ->delete();
    }

    public function do(): JsonResponse {

        try {

            self::remove();

            return response()->json([
                'error'   => 0,
                'message' => 'Successful cron operation'
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
