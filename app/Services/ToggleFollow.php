<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Mockery\Exception;

class ToggleFollow {

    private const USER_FOLLOW = 'user-follow';

    private $request, $target_user, $auth_user;

    /**
     * ToggleFollow constructor.
     * @param Request $request
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    private function getTargetUser(): User {
        return User::select('id', 'identifier', 'nickname')->where('identifier', $this->request->id)->first();
    }

    /**
     * @return array
     */
    private function getNotificationAttributes(): array {
        return [
            'users'  => [$this->target_user],
            'user'   => $this->auth_user,
            'target' => $this->target_user
        ];
    }

    private function setData(): void {
        $this->target_user = self::getTargetUser();
        $this->auth_user   = \Auth::user();
    }

    private function setFollow(): void {
        // follow
        $attributes        = self::getNotificationAttributes();
        $storeNotification = new StoreNotification(self::USER_FOLLOW, $attributes);
        $storeNotification->do();

        // Create a new follow instance for the authenticated user
        $this->target_user->followers()->create([
            'user_id' => $this->auth_user->id
        ]);
    }

    private function setUnfollow(): void {
        // unfollow
        $this->target_user->followers()->where('user_id', $this->auth_user->id)->delete();
    }

    private function toggle(): void {

        if (!empty($this->target_user)) {

            if (!$this->target_user->is_auth_user_follower) {

                self::setFollow();

            } else {

                self::setUnfollow();
            }
        }
    }

    /**
     * @return JsonResponse
     */
    private function getResponse(): JsonResponse {

        if ($this->target_user->is_auth_user_follower) {

            // follow
            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.follow_user')
            ]);

        } else {

            // unfollow
            return response()->json([
                'error'   => 1,
                'message' => __('flash-messages.unfollow_user')
            ]);

        }
    }

    public function do(): JsonResponse {

        try {

            self::setData();

            self::toggle();

            return self::getResponse();

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }

    }
}
