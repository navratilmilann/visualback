<?php

namespace App\Services\Profile;

use App\Models\Newsletter;
use App\Models\User;
use App\Traits\NewsletterOperations;
use App\Traits\ParameterOperations;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Mockery\Exception;

class UpdateSettings {

    use ParameterOperations,
        NewsletterOperations;

    private $data, $user;

    /**
     * UpdateSettings constructor.
     * @param array $data
     * @param User|null $user
     */
    public function __construct(array $data, User $user = null) {
        $this->data = $data;
        $this->user = $user ?? \Auth::user();
    }

    /**
     * @param array $data
     */
    private function updateUser(array $data): void {
        $this->user->update($data);
    }

    /**
     * @param $validator
     */
    private function handleException($validator): void {
        if ($validator->fails()) {
            throw new Exception($validator->errors()->first());
        }
    }

    private function validateNickname(): void {
        $validator = Validator::make($this->data, [
            'nickname' => 'unique:users|min:4|max:25',
        ]);

        self::handleException($validator);
    }

    private function validatePassword(): void {
        $validator = Validator::make($this->data, [
            'password' => [
                'required',
                'confirmed',
                Password::min(8)
                    ->mixedCase()
                    ->symbols()
            ],
        ]);

        self::handleException($validator);
    }

    /**
     * @return mixed
     */
    private function storeProfilePicture() {

        $file = $this->data['profile_picture'];

        if (!empty($file)) {
            return $file->store('profile-photo', 'public');
        }

        return $this->user->profile_photo_path;
    }

    private function verificationAccount(): void {

        // work position
//        if (!empty($this->data['user-status'])) {
//
//            $parameterId = $this->getParametersIdByAttribute([$this->data['user-status']]);
//
//            $this->updateParameterByType($this->user, $parameterId, ['user-status']);
//        }

        // how user met our project
        if (!empty($this->data['marketing-tool'])) {

            $marketingTools = explode(',', $this->data['marketing-tool']);

            $this->updateParameterByType($this->user, $marketingTools, ['marketing-tool']);
        }
    }

    /**
     * Validation and update
     */
    private function updateAccount(): void {

        if ($this->data['type'] == 'account') {

            $newPassword          = $this->data['password'] ?? null;
            $oldPassword          = $this->data['old_password'] ?? null;
            $confirmationPassword = $this->data['password_confirmation'] ?? null;

            if ($this->data['nickname'] !== $this->user->nickname) {                                // updating nickname
                self::validateNickname();

                $data['nickname'] = $this->data['nickname'];
            }

            if (!empty($confirmationPassword) || !empty($newPassword) || !empty($oldPassword)) {     // if filled out one of password fields

                if (empty($oldPassword)) {
                    throw new Exception(__('flash-messages.fill_in_password_fields'));
                }

                if (!Hash::check($oldPassword, $this->user->password)) {
                    throw new Exception(__('flash-messages.incorrect_old_password'));
                }

                if (empty($confirmationPassword) || empty($newPassword)) {
                    throw new Exception(__('flash-messages.fill_in_password_fields'));
                }

                self::validatePassword();

                $data['password'] = Hash::make($newPassword);
            }

            self::verificationAccount();

            if (!empty($data)) {
                self::updateUser($data);
            }
        }
    }

    private function updateUserDetails(): void {

        if ($this->data['type'] == 'user_detail') {

//            $city      = !empty($this->data['user-city']) ? $this->data['user-city'] : null;
//            $country   = !empty($this->data['user-country']) ? $this->data['user-country'] : null;
//            $platforms = $this->getJsonableAttributes($this->data, ['github', 'linkedin']);
//            $parameterIds = $this->getParametersIdByAttribute([$this->data['user-status']]);

            $data = [
                'name'               => $this->data['name'],
                'surname'            => $this->data['surname'],
                'profile_photo_path' => self::storeProfilePicture()
            ];

            if (!empty($data)) {
                self::updateUser($data);
            }

//            $this->updateParameterByType($this->user, $parameterIds, ['user-status']);

//            $this->updateParameterValueByIds($this->user, [15 => $city, 16 => $country, 20 => $platforms]);
        }
    }

    private function updateProfession(): void {

        if ($this->data['type'] == 'profession') {

            $parameterId    = $this->getParametersIdByAttribute(json_decode($this->data['technology']));
            $workExperience = self::getJsonableAttributes($this->data, ['work_experiences']);

            $this->updateParameterByType($this->user, $parameterId, ['technology']);

            $this->updateParameterValueByIds($this->user, [14 => $workExperience]);
        }
    }

    private function updatePermission(): void {

        if ($this->data['type'] == 'permissions') {
            // store or delete newsletter
            if (!empty($this->data['newsletter'])) {
                $this->storeNewsletter($this->user->email, $this->user->id);
            } else {
                $this->deleteNewsletter($this->user->email, $this->user->id);
            }

            $permissions = self::getJsonableAttributes($this->data, ['newsletter']);
            $this->updateParameterValueByIds($this->user, [17 => $permissions]);
        }
    }

    public function do(): JsonResponse {

        try {

            self::updateAccount();

            self::updateUserDetails();

//            self::updateProfession();

            self::updatePermission();

            return response()->json([
                'error'   => 0,
                'message' => __('flash-messages.updated_user_settings'),
            ]);

        } catch (Exception $exception) {

            return response()->json([
                'error'   => 1,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
