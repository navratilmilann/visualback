<?php

namespace App\Listeners;

use App\Events\NoticeSiteCollaborators;
use App\Mail\Site\InvitationSite;
use App\Models\Site;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendCollaboratorNotification {
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }


    private function getSite(int $siteId) {
        return Site::where('id', $siteId)->first();
    }

    /**
     * Handle the event.
     *
     * @param NoticeSiteCollaborators $event
     * @return void
     */
    public function handle(NoticeSiteCollaborators $event) {

        $site = self::getSite($event->siteId);

        foreach ($event->users as $user) {

            $user->sites()->attach($site->id);

            Mail::to($user->email)->send(new InvitationSite($user, $site));
        }

    }
}
