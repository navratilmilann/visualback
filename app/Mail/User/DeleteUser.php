<?php

namespace App\Mail\User;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DeleteUser extends Mailable {
    use Queueable, SerializesModels;

    private $user;

    /**
     * DeleteUser constructor.
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
//            ->from([
//                'address' => env('MAIL_FROM'), 'name' => env('APP_NAME')
//            ])
            ->markdown('emails.user.deletion', [
                'user' => $this->user
            ]);
    }
}
