<?php

namespace App\Mail\Site;

use App\Models\SiteComment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewComment extends Mailable {
    use Queueable, SerializesModels;

    private $comment, $user;

    /**
     * NewCommented constructor.
     * @param SiteComment $comment
     * @param $user
     */
    public function __construct(SiteComment $comment, $user) {
        $this->comment = $comment;
        $this->user    = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this
            ->to($this->user->email)
            ->markdown('emails.site.new-comment')
            ->with('user', $this->user)
            ->with('comment', $this->comment);
    }
}
