<?php

namespace App\Mail\Site;

use App\Models\Site;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvitationSite extends Mailable {
    use Queueable, SerializesModels;

    private $user, $site;

    /**
     * InvitationSite constructor.
     * @param User $user
     * @param Site $site
     */
    public function __construct(User $user, Site $site) {
        $this->user = $user;
        $this->site = $site;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        if (!$this->user->hasVerifiedEmail())
            $this->user->verification_link = route('verification.verify', ['id' => $this->user->id, 'hash' => sha1($this->user->email)]);

        return $this
            ->to($this->user->email)
            ->markdown('emails.site.invitation')
            ->with('user', $this->user)
            ->with('site', $this->site);
    }
}
