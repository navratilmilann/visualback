<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportedModel extends Mailable {
    use Queueable, SerializesModels;

    public $data;

    /**
     * ReportedModel constructor.
     * @param string $type
     * @param int $id
     * @param $user_id
     */
    public function __construct(string $type, int $id, $user_id) {
        $this->data = [
            'type'    => $type,
            'id'      => $id,
            'user_id' => $user_id
        ];
    }

    private function getAdditionalData() {
        $this->data['url'] = get_report_in_admin($this->data['id'], $this->data['type']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        self::getAdditionalData();

        return $this
            ->subject('ADMIN IMPORTANT - REPORT')
            ->with('data', $this->data)
            ->markdown('emails.reported-model');
    }
}
