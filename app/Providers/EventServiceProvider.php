<?php

namespace App\Providers;

use App\Events\NoticeSiteCollaborators;
use App\Listeners\SendCollaboratorNotification;
use App\Models\Attachment;
use App\Models\Site;
use App\Models\SiteComment;
use App\Models\User;
use App\Observers\AttachmentObserver;
use App\Observers\SiteCommentObserver;
use App\Observers\SiteObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NoticeSiteCollaborators::class => [
            SendCollaboratorNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot() {
        User::observe(UserObserver::class);
        Site::observe(SiteObserver::class);
        SiteComment::observe(SiteCommentObserver::class);
        Attachment::observe(AttachmentObserver::class);
    }
}
