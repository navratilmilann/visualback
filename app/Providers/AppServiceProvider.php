<?php

namespace App\Providers;

use App\Models\Seo;
use Illuminate\Queue\Events\JobFailed;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * @return bool[]|null
     */
    private function getSharedVariable() {

        $current_route = \Route::currentRouteName();
        $auth_routes   = [
            'forgot-password', 'login', 'register', 'reset-password', 'verification.verify'
        ];

        if ($current_route === 'site.livePreview' || $current_route === 'site.demoLivepreview') {
            return ['live_preview' => true];
        } elseif (in_array($current_route, $auth_routes)) {
            return ['auth' => true];
        } elseif (strpos($current_route, 'post.') !== false) {
            return ['post' => true];
        }

        return null;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {

        view()->composer(['partials.*'], function ($view) {

            $shared_variable = $this->getSharedVariable();

            if ($shared_variable) {
                $view->with(array_key_first($shared_variable), $shared_variable);
            }
        });

        view()->composer(['*', 'auth.*', 'errors.*', 'errors::*'], function ($view) {
            $seo = Seo::where('route_name', 'default')->first();

            $view->with('seo', $seo->attributes);
        });

        Schema::defaultStringLength(191);
    }
}
