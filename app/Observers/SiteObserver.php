<?php

namespace App\Observers;

use App\Models\Site;
use App\Traits\ParameterOperations;

class SiteObserver {

    use ParameterOperations;

    /**
     * Handle the Site "created" event.
     *
     * @param \App\Models\Site $site
     * @return void
     */
    public function created(Site $site) {
        //
    }

    /**
     * Handle the Site "updated" event.
     *
     * @param \App\Models\Site $site
     * @return void
     */
    public function updated(Site $site) {
        //
    }

    /**
     * Handle the Site "deleted" event.
     *
     * @param \App\Models\Site $site
     * @return void
     */
    public function deleted(Site $site) {
        //
    }

    /**
     * @param Site $site
     */
    public function deleting(Site $site) {

        $parameterTypes = $site->parameters()->pluck('type')->toArray();

        // parameters
        $this->decrementParameters($site, $parameterTypes);

        $site->parameters()->detach();

        // comments and replies
        $site->comments()->each(function ($comment) {
            $comment->delete();
        });
        // attachments
        $site->attachment()->each(function ($attachment) {
            $attachment->delete();
        });
        // ratings
        $site->rating()->delete();

        $site->reports()->delete();
    }

    /**
     * Handle the Site "restored" event.
     *
     * @param \App\Models\Site $site
     * @return void
     */
    public function restored(Site $site) {
        //
    }

    /**
     * Handle the Site "force deleted" event.
     *
     * @param \App\Models\Site $site
     * @return void
     */
    public function forceDeleted(Site $site) {
        //
    }
}
