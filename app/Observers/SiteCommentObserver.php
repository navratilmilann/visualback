<?php

namespace App\Observers;

use App\Models\SiteComment;

class SiteCommentObserver {
    /**
     * Handle the SiteComment "created" event.
     *
     * @param \App\Models\SiteComment $siteComment
     * @return void
     */
    public function created(SiteComment $siteComment) {

        if (empty($siteComment->user_id)) {
            $siteComment->nickname = 'anonymous';           // todo: generate unique nickname
            $siteComment->save();
        }
    }

    /**
     * Handle the SiteComment "updated" event.
     *
     * @param \App\Models\SiteComment $siteComment
     * @return void
     */
    public function updated(SiteComment $siteComment) {
        //
    }

    /**
     * Handle the SiteComment "deleted" event.
     *
     * @param \App\Models\SiteComment $siteComment
     * @return void
     */
    public function deleted(SiteComment $siteComment) {
        //
    }

    /**
     * @param SiteComment $siteComment
     */
    public function deleting(SiteComment $siteComment) {

        $siteComment->comment_rating()->delete();

//        $siteComment->attachment()->delete();
        $siteComment->attachment()->each(function ($attachment) {
            $attachment->delete();
        });

        $siteComment->replies()->delete();

        $siteComment->reports()->delete();
//        if ($siteComment->replies()->count()) {
//
//        }
    }

    /**
     * Handle the SiteComment "restored" event.
     *
     * @param \App\Models\SiteComment $siteComment
     * @return void
     */
    public function restored(SiteComment $siteComment) {
        //
    }

    /**
     * Handle the SiteComment "force deleted" event.
     *
     * @param \App\Models\SiteComment $siteComment
     * @return void
     */
    public function forceDeleted(SiteComment $siteComment) {
        //
    }
}
