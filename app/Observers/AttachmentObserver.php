<?php

namespace App\Observers;

use App\Models\Attachment;

class AttachmentObserver {
    /**
     * Handle the Attachment "created" event.
     *
     * @param \App\Models\Attachment $attachment
     * @return void
     */
    public function created(Attachment $attachment) {
        //
    }

    /**
     * Handle the Attachment "updated" event.
     *
     * @param \App\Models\Attachment $attachment
     * @return void
     */
    public function updated(Attachment $attachment) {
        //
    }

    /**
     * Handle the Attachment "deleted" event.
     *
     * @param \App\Models\Attachment $attachment
     * @return void
     */
    public function deleted(Attachment $attachment) {
        //
    }

    /**
     * @param Attachment $attachment
     */
    public function deleting(Attachment $attachment) {

        $path       = public_path("/{$attachment->path}");

        if (file_exists($path))
            unlink($path);
    }

    /**
     * Handle the Attachment "restored" event.
     *
     * @param \App\Models\Attachment $attachment
     * @return void
     */
    public function restored(Attachment $attachment) {
        //
    }

    /**
     * Handle the Attachment "force deleted" event.
     *
     * @param \App\Models\Attachment $attachment
     * @return void
     */
    public function forceDeleted(Attachment $attachment) {
        //
    }
}
