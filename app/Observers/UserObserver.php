<?php

namespace App\Observers;

use App\Mail\User\DeleteUser;
use App\Models\User;
use App\Traits\ParameterOperations;
use Illuminate\Support\Facades\Mail;

class UserObserver {

    use ParameterOperations;

    /**
     * Handle the User "created" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user) {
        //
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function updated(User $user) {
        //
    }

    /**
     * @param User $user
     */
    public function deleting(User $user) {

        $parameterTypes = $user->parameters()->pluck('type')->toArray();

        // dekrement parametrov, parametre, ratingov, komentarov a replies, delete stranok, attachments
        $user->sites()->each(function ($site) use ($user) {

            $site->users()->detach($user->id);

            if ($site->users()->count() < 1) {
                $site->delete();
            }
        });

        // delete user parametrov a decrement
        $this->decrementParameters($user, $parameterTypes);

        $user->parameters()->detach();

        Mail::to($user)->send(new DeleteUser($user));
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function deleted(User $user) {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function restored(User $user) {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function forceDeleted(User $user) {
        //
    }
}
