<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule {

    // https://welcm.uk/blog/adding-google-recaptcha-v3-to-your-laravel-forms

    const URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function passes($attribute, $value) {

        if(config('app.env') != 'production'){
            return true;
        }

        $data       = [
            'secret'   => config('services.recaptcha.secret'),
            'response' => $value,
            'remoteip' => request()->ip()
        ];
        $options    = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context    = stream_context_create($options);
        $result     = file_get_contents(self::URL, false, $context);
        $resultJson = json_decode($result);

//        dd($resultJson);

        return $resultJson->success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message() {
        return 'ReCaptcha Error.';
    }
}
