<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NoticeSiteCollaborators {
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $users, $siteId;

    /**
     * NoticeSiteCollaborators constructor.
     * @param array $users
     * @param int $siteId
     */
    public function __construct(array $users, int $siteId) {
        $this->users  = $users;
        $this->siteId = $siteId;
    }
}
