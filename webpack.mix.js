const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/js/shared.js', 'public/js')
	.js('resources/js/auth.js', 'public/js')
	.js('resources/js/live-preview.js', 'public/js')
	.js('resources/js/post.js', 'public/js')
	.sass('resources/sass/global.scss', 'public/css')
	.sass('resources/sass/auth.scss', 'public/css')
	.sass('resources/sass/shared.scss', 'public/css')
	.sass('resources/sass/live-preview.scss', 'public/css')
	.sass('resources/sass/post.scss', 'public/css')
	.sourceMaps();

if (!mix.inProduction()) {
	mix.webpackConfig({devtool: "inline-source-map"});
}

