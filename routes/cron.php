<?php

use App\Http\Controllers\CronController;
use Illuminate\Support\Facades\Route;

/**
 * CRONS
 */
Route::get('/attachment/delete/6929048399', [CronController::class, 'deleteUnusedAttachments']);
Route::get('/site/delete/0651432315', [CronController::class, 'deleteUnregisteredSites']);
