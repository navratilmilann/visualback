<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web'], function () {
    Route::view('/', 'index')->name('home');
    Route::view('/mobile/live-preview', 'errors.mobile-livepreview')->name('mobileLivepreview');
    Route::view('/support-us', 'support-us')->name('supportUs');
    Route::view('/faq', 'faq')->name('faq');
    Route::view('/terms-and-conditions', 'terms-conditions')->name('termsAndConditions');
    Route::view('/privacy-policy', 'privacy-policy')->name('privacyPolicy');
    Route::view('/about-us', 'about-us')->name('aboutUs');
    Route::view('/cookies', 'cookies')->name('cookies');

    Route::group(['prefix' => 'post', 'as' => 'post.'], function () {
        Route::get('/', [PostController::class, 'index'])->name('index');
        Route::get('/tag/{tag_slug}', [PostController::class, 'index'])->name('index');
        Route::get('/{slug}/{id}', [PostController::class, 'detail'])->name('detail');
    });

    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
        Route::get('/{identifier}', [ProfileController::class, 'detail'])->name('detail');
    });

    Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
        Route::get('/', [ContactController::class, 'index'])->name('index');
        Route::post('/store', [ContactController::class, 'store'])->name('store');
    });

    Route::group(['prefix' => 'project', 'as' => 'site.'], function () {
        Route::get('/', [SiteController::class, 'index'])->name('index');
        Route::get('/demo/app/live-preview', [SiteController::class, 'demoLivepreview'])->name('demoLivepreview');
        Route::get('/{identifier}/{slug}/live-preview', [SiteController::class, 'livePreview'])->name('livePreview');
    });

    /**
     * AUTHENTICATED USER
     */
    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'project', 'as' => 'site.', 'middleware' => ['IsSiteAuthor']], function () {
            Route::get('/{identifier}/edit', [SiteController::class, 'edit'])->name('edit');
            Route::post('/{identifier}/update', [SiteController::class, 'update'])->name('update');
        });

        Route::group(['prefix' => 'profile', 'as' => 'profile.', 'middleware' => ['IsAccountOwner']], function () {
            Route::get('{identifier}/settings/{category?}', [ProfileController::class, 'settings'])->name('settings');
            Route::get('{identifier}/delete', [ProfileController::class, 'delete'])->name('delete');
        });
    });
});
