<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;

/**
 * AUTHENTICATION
 */
// todo: password request

// handling request for verification account from email
Route::middleware('IsUserVerified')->group(function () {
    Route::get('/email/verify/{id}/{hash}', [RegisterController::class, 'verificationAccount'])->name('verification.verify');
    Route::post('/verified-account', [RegisterController::class, 'verifiedAccount']);
});

Route::get('/{token}/reset-password', [LoginController::class, 'showResetPassword'])->name('reset-password');
Route::post('/reset-password', [LoginController::class, 'resetPassword']);
Route::get('/forgot-password', [LoginController::class, 'showForgotPassword'])->name('forgot-password');
Route::post('/forgot-password', [LoginController::class, 'forgotPassword'])->name('password.email');
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/register', [RegisterController::class, 'showRegisterForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

// handling LogIn with GitHub
Route::get('auth/github', [OAuthController::class, 'redirectToGithub'])->name('login.github');
Route::get('auth/github/callback', [OAuthController::class, 'handleGithubCallback']);

// handling LogIn with Googlec
Route::get('auth/google', [OAuthController::class, 'redirectToGoogle'])->name('login.google');
Route::get('auth/google/callback', [OAuthController::class, 'handleGoogleCallback']);

// handling LogIn with Facebook
Route::get('auth/facebook', [OAuthController::class, 'redirectToFacebook'])->name('login.facebook');
Route::get('auth/facebook/callback', [OAuthController::class, 'handleFacebookCallback']);

//  handling LogIn with LinkedIn
Route::get('auth/linkedin', [OAuthController::class, 'redirectToLinkedin'])->name('login.linkedin');
Route::get('auth/linkedin/callback', [OAuthController::class, 'handleLinkedinCallback']);
