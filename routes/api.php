<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ParameterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web'], function () {

    Route::get('/send-verification/{identifier}', [RegisterController::class, 'sendVerificationEmail'])->name('sendVerificationEmail');

    Route::group(['prefix' => 'newsletter', 'as' => 'newsletter.'], function () {
        Route::post('/', [NewsletterController::class, 'storeNewsletter'])->name('store');
    });

    Route::middleware('auth')->group(function () {
        //        Route::get('/{id}/follow', [FollowController::class, 'toggleFollow']);
        Route::post('/{model}/{id}/like', [SiteController::class, 'toggleLike']);
        Route::post('/{type}/{id}/report', [ReportController::class, 'reportModel']);
    });

    Route::prefix('user')->group(function () {
        Route::get('/{identifier}/project', [ProfileController::class, 'userProjects']);
        Route::get('/{user_id?}', [ProfileController::class, 'user']);

        Route::middleware('IsAccountOwner')->group(function () {
//            Route::get('/{id}/followers', [FollowController::class, 'followers']);
//            Route::get('/{id}/followings', [FollowController::class, 'followings']);
            Route::get('/{id}/notifications', [NotificationController::class, 'notifications']);
            Route::get('/{id}/notifications/count', [NotificationController::class, 'notificationCount']);
            Route::post('/{id}/notifications/update', [NotificationController::class, 'update']);
            Route::get('/{id}/notifications/delete', [NotificationController::class, 'delete']);
            Route::get('/{id}/site/count', [ProfileController::class, 'siteCount']);
            Route::post('/{user_id}/update', [ProfileController::class, 'update']);
        });
    });

    Route::prefix('parameter')->group(function () {
        Route::get('/technology', [ParameterController::class, 'getTechnologies']);
        Route::get('/countries', [ParameterController::class, 'getCountries']);
        Route::get('/cities', [ParameterController::class, 'getCities']);
        Route::get('/{type}/{pluck?}', [ParameterController::class, 'getParameter']);
    });

    Route::prefix('post')->group(function () {
        Route::get('/', [PostController::class, 'getPosts']);
        Route::get('/tag/popular', [PostController::class, 'getPopularTags']);
        Route::get('/tag/{tag_slug}', [PostController::class, 'getPosts']);
        Route::get('/{slug}/{id}', [PostController::class, 'getPost']);
        Route::get('/{slug}/{id}/related/{count?}', [PostController::class, 'getRelatedPosts']);
    });

    Route::prefix('project')->group(function () {

        Route::prefix('comment')->group(function () {
            Route::post('/rating/store', [CommentController::class, 'storeCommentRating']);
            Route::post('/store', [CommentController::class, 'storeSiteComment']);

            Route::middleware('IsCommentAuthor')->group(function () {
                Route::post('/{id}/delete', [CommentController::class, 'deleteSiteComment']);
                Route::post('/{id}/update', [CommentController::class, 'updateSiteComment']);
            });
        });

        Route::middleware('IsSiteAuthor')->group(function () {
            Route::post('/{identifier}/authorize', [SiteController::class, 'authorizeSite']);
            Route::post('/{identifier}/{slug}/delete', [SiteController::class, 'deleteSite']);
        });

        Route::middleware('auth')->group(function () {
            Route::get('/{id}/report', [ReportController::class, 'site']);
            Route::post('/store/collaborators', [SiteController::class, 'setCollaborators']);
            Route::post('/store', [SiteController::class, 'storeSite']);
            Route::post('/rating/store', [SiteController::class, 'storeSiteRating']);
        });

        Route::get('/{id}/comment', [CommentController::class, 'getComments']);
        Route::post('/{id}/attachment/store', [CommentController::class, 'storeAttachment']);
        Route::get('/{id}/screenshot/store', [CommentController::class, 'storeScreenshot']);
        Route::get('/{id}/attachment/{attachment_id}', [CommentController::class, 'getAttachment']);
        Route::post('/{identifier}/attachment/{attachment_id}/delete', [CommentController::class, 'deleteAttachment']);

        Route::get('/{limit?}/{offset?}', [SiteController::class, 'getSites']);
    });
});
